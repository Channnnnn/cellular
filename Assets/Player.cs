﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Reproduce = System.Tuple<CellData, SpawnType>;

[System.Serializable]
public class SerializablePlayer
{
	public string Name { get; set; }
	public Player.Type Type { get; set; }
	public SerializableColor32 Color { get; set; }
	public List<Cartesian> PlayerCells { get; set; }
	public float TotalFood { get; set; }
	public SerializablePlayer(Player player) {
		Name = player.NAME;
		Type = player.TYPE;
		Color = new SerializableColor32(player.COLOR);
		PlayerCells = player.PlayerCells.Select(cell => cell.coordination).ToList();
		TotalFood = player.TotalFood;
	}
	public Player Deserialized { get {
			return new Player(this);
	} }
}

[System.Serializable]
public class Player{
	public static Player NoneType { get { return new Player("None", Type.None, Color.clear); } }
	public Player(string name, Type type, Color32 color) {
		NAME = name;
		TYPE = type;
		COLOR = color;
	}
	public Player(SerializablePlayer player) {
		NAME = player.Name;
		TYPE = player.Type;
		COLOR = player.Color.Colorize;
		playerCells = new List<CellData>();
		foreach (Cartesian item in player.PlayerCells) {
			CellData playerCell = MapManager.GetCell(item);
			playerCell.owner = this;
			playerCells.Add(playerCell);
		}
		TotalFood = player.TotalFood;
	}

	public enum Type { None, Human, AI, Minor };
	public string NAME;
	public Type TYPE;
	public Color32 COLOR;
	List<CellData> playerCells = new List<CellData>();
	public List<CellData> PlayerCells { get { return playerCells; } }
	public Vector2 MedianPosition { get {
			Vector2 centroid = new Vector2();
			playerCells.Select(cell => cell.gameObject.transform.position).ForEach(vector => centroid += (Vector2)vector);
			centroid /= playerCells.Count;
			return centroid;
	} }

	public float TotalFood;

	public int CellCount {
		get { return playerCells.Where(cell => cell.HasCell && cell.owner == this).Count(); }
	}

	public void AddPlayerCell(CellData newCell) {
		playerCells.Add(newCell);
		TotalFood += newCell.tile["Food"];
		Debug.Log($"Adding {NAME}'s Cell {newCell.coordination} --- {playerCells.Count} | Food Sum {TotalFood} {newCell.interaction.IsDyingSequenceCompleted} {newCell.stats[Key.Health]}");
		if (TYPE == Type.Human) GameManager._.gameOver = GameManager.IsGameOver;
		}

	public void RemovePlayerCell(CellData diedCell) {
		TotalFood -= diedCell.tile["Food"];
		Debug.Log($"Removing {NAME}'s Cell {diedCell.coordination} --- {playerCells.Count - 1} | Food Sum {TotalFood}");
		if (TYPE == Type.Human) GameManager._.gameOver = GameManager.IsGameOver;
	}

	public void UpdateMovedCellReference(Cartesian originalPosition, CellData modifiedCell) {
		for (int i = 0; i < playerCells.Count; i++) {
			if (playerCells[i].coordination == originalPosition) {
				playerCells[i] = modifiedCell;
			}
			if (playerCells[i].parent && playerCells[i].parent.coordination == originalPosition) {
				playerCells[i].parent = modifiedCell;
			}
		}
	}


	public void DoAction () {
		float randTime = Random.Range(5f, 15f);
		if (GameManager._.PlaySpeed != 0 && Time.time % randTime < 0.01f) {
			int actions = Random.Range(5, (int)(playerCells.Count * 0.5));
			HashSet<int> pickcell = new HashSet<int>();
			for (int i = 0; i < actions; i++) {
				pickcell.Add(Random.Range(0, playerCells.Count));
			}
			foreach (var i in pickcell) {
				TraitRoutine(playerCells[i]);
				ReproductionRoutine(playerCells[i]);
				UpgradeRoutine(playerCells[i]);
			}
		}
	}

	private void TraitRoutine(CellData cell) {
		//if(cell.stats.isSufficient(CellData.Genetics, TraitsReference._this["t1"].Cost)) {
		//	Random.Range(0,2) == 0 ? 
		//}
	}

	private void ReproductionRoutine(CellData cell) {
		SpawnType spawn = SpawnType.Empty;
		float threshold = 4;
		if (cell.upgrades.Contains("u10.1")) { threshold = 6; spawn = SpawnType.Binary; }
		if (cell.upgrades.Contains("u10.2")) { threshold = 4; spawn = SpawnType.Budding; }
		if (spawn == SpawnType.Empty) return;

		if (cell.stats[Key.Size] > threshold) {
			if (cell.stats.IsSufficient(Key.Proteins, cell.REPRODUCTION_PROTEINS_COST)) {
				var bestFood_tiles = cell.FindBestTile("Reproduction");
				if (bestFood_tiles) {
					//Debug.Log($"{cell.coordination} Reproducing to {bestFood_tiles.coordination}");
					if (bestFood_tiles.tile[Key.Food] < 0.85* cell.tile[Key.Food] && !playerCells.Contains(bestFood_tiles)) {
						return;
					}
					//print($"Cell {cell.coordination} F{cell.tile[CellData.Food]} S{cell.stats[CellData.Size]} binary to {bestFood_tiles.coordination} at {GameManager.CycleCount} Cycle");
					cell.Action_Reproduction(new Reproduce(bestFood_tiles, spawn));
				}
			}
		}
	}

	private void UpgradeRoutine(CellData cell) {
		/*if (cell.TOTAL_DAMAGE_BY_TILE > (cell.stats[Key.Health]) / (cell.stats[Key.Lifespan] - cell.Age)) {
			var best_damage = cell.FindBestTile("TileDamage");
			if (best_damage && best_damage.TOTAL_DAMAGE_BY_TILE < cell.TOTAL_DAMAGE_BY_TILE) {
				cell.Action_Move(best_damage);
			}
			if (best_damage && best_damage.TOTAL_DAMAGE_BY_TILE >= cell.TOTAL_DAMAGE_BY_TILE) {
				Extension.RandomEvent(delegate { cell.Action_Move(best_damage); }, 0.2f);
			}
		}*/

		HashSet<string> queueOfUpgrade = new HashSet<string>(cell.queueOfUpgrade);
		string[] reproduction = { "u10.1", "u10.2" };

		if (!cell.upgrades.Overlaps(reproduction)
			&& !queueOfUpgrade.Overlaps(reproduction)
			&& cell.availableUpgrades.Overlaps(reproduction)) {
			int choice = Random.Range(0, 2);
			if (choice == 0) cell.Upgrade_OnIssueProduction("u10.1");
			else cell.Upgrade_OnIssueProduction("u10.2");
		}
	}

	public void ResetPlayerCells() {
		foreach (var cell in playerCells) {
			cell.SpawnNewCell(NoneType, SpawnType.Empty, null);
		}
		playerCells.Clear();
	}
}

public class Multimap
{
	public Dictionary<Cartesian, CellData> exo;
	public Dictionary<Cartesian, CellData> endo;
}
