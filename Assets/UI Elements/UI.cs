﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;

public class UIStatePair
{
	private static Dictionary<UIState, GameObject> dictionary = new Dictionary<UIState, GameObject>();
	public static void SetValue(UIState state, GameObject obj){
		dictionary.Add(state, obj);
	}
	public static GameObject GetValue(UIState state) {
		GameObject _out;
		dictionary.TryGetValue(state, out _out);
		return _out;
	}
}

public class UI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public CameraControl cameraControl;
	public CanvasScaler canvasScaler; 
	public Button menuButton;
	public Button menuClosingButton;
	public Button traitsButton;
	public Button traitsClosingButton;
	[Header("Game speed sprites")]
	public Button pauseButton;
	public Button normalSpeedButton;
	public Button fastSpeedButton;
	public Button fasterSpeedButton;
	public RectTransform GameSpeedPanel;

	[Header("View Panels")]
	public GameObject INGAME_UI_GROUP;
	public GameObject MENU_UI_GROUP;
	public Image welcomeMenu;
	public Image setupPanel;
  public Image optionsPanel;
  public Image menuOverlay;
	public Image cellDataPanel;
	public Image traitsPanel;
	public Image gameOverDialogue;
	public TextMeshProUGUI gameOverDialogueMessage;

	[Header("Traits Screen")]
	public Transform growthRoot;
	public Transform specializationRoot;
	public Transform structureRoot;
	public Transform logisticRoot;
	public Transform purchasedRoot;
	public GameObject traitEntryPrefab;
	public GameObject traitPurchased;
	public Dictionary<string, GameObject> TRAIT_BUTTONS_LIBRARY = new Dictionary<string, GameObject>();
	public Dictionary<string, GameObject> TRAIT_PURCHASED = new Dictionary<string, GameObject>();

	[Header("Tooltips Panels")]
	public Vector2 tooltipOffset;
	public RectTransform pointerTooltipRoot;
	public RectTransform pointerTooltipPanel;
	public TextMeshProUGUI pointerTooltipText;
	[Header("Cell Tooltips Panels")]
	public RectTransform panelTooltip;
	public TextMeshProUGUI panelTooltipText;

	public TextMeshProUGUI announcer;

	ColorBlock defaultButtonScheme;
	ColorBlock activeButtonScheme;

	bool mouseDowned;
	bool mouseLeaved;

	void Awake () {
		UIStatePair.SetValue(UIState.WelcomeScreen, welcomeMenu.gameObject);
		UIStatePair.SetValue(UIState.EscapeMenu, menuOverlay.gameObject);
		UIStatePair.SetValue(UIState.CellScreen, cellDataPanel.gameObject);
		UIStatePair.SetValue(UIState.TraitsScreen, traitsPanel.gameObject);
	}
	void Start () {
		welcomeMenu.transform.parent.localPosition = new Vector3(welcomeMenu.transform.parent.localPosition.x, welcomeMenu.transform.parent.localPosition.y, -450f);
		canvasScaler = GetComponent<CanvasScaler>();

		defaultButtonScheme = pauseButton.colors;
		activeButtonScheme = pauseButton.colors;
		activeButtonScheme.normalColor = activeButtonScheme.highlightedColor = _Color.Alter(activeButtonScheme.pressedColor,0f, 10f, -10f);

		menuButton.image.alphaHitTestMinimumThreshold = 0.75f;
		menuClosingButton.image.alphaHitTestMinimumThreshold = 0.75f;
		menuButton.onClick.AddListener(OnClickMenuButton);
		menuClosingButton.onClick.AddListener(OnClickMenuButton);
		traitsButton.onClick.AddListener(OnClickTraitsScreenButton);
		traitsClosingButton.onClick.AddListener(OnClickTraitsScreenButton);
		pauseButton.onClick.AddListener(OnClickPauseButton);
		normalSpeedButton.onClick.AddListener(OnClickNormalSpeedButton);
		fastSpeedButton.onClick.AddListener(OnClickFastSpeedButton);
		fasterSpeedButton.onClick.AddListener(OnClickFasterSpeedButton);

		GameSpeedButtonHighlight(1);
		MENU_UI_GROUP.SetActive(true);
		welcomeMenu.gameObject.SetActive(true);
		setupPanel.gameObject.SetActive(false);
    optionsPanel.gameObject.SetActive(false);
		INGAME_UI_GROUP.SetActive(false);
		cellDataPanel.gameObject.SetActive(false);
		menuOverlay.gameObject.SetActive(false);
		gameOverDialogue.gameObject.SetActive(false);
		traitsPanel.gameObject.SetActive(false);
	}

	Vector3 welcomVelocity = Vector3.zero;
	void Update() {
		if (welcomeMenu.transform.parent.position.z != 200) {
			welcomeMenu.transform.parent.localPosition = Vector3.SmoothDamp(welcomeMenu.transform.parent.localPosition, new Vector3(0,0,200f), ref welcomVelocity, 2f);
		}
		CheckTooltip();
		//CheckPanelTooltip(); //disabled for now
	}

	public void InstatntiateTraitEntires() {
		var traits = TraitsReference._this.Keys;
		foreach (var id in traits) {
			Trait TRAIT_ENTRY = TraitsReference._this[id];
			GameObject TRAIT_BUTTON_OBJECT = Instantiate(traitEntryPrefab);
			TextMeshProUGUI TRAIT_BUTTON_TEXT = TRAIT_BUTTON_OBJECT.GetComponentInChildren<TextMeshProUGUI>();
			TRAIT_BUTTON_OBJECT.tag = "TraitEntries";
			TRAIT_BUTTON_TEXT.tag = "TraitText";
			TRAIT_BUTTON_TEXT.name = id;
			TRAIT_BUTTON_TEXT.text = $"<style=Genetic>{TRAIT_ENTRY.Cost.FloorS()}<sprite=\"Resources\" index=0></style><pos=20%>{TRAIT_ENTRY.Name}";
			Transform parent = null;
			if (TRAIT_ENTRY.Type == "Growth") {
				parent = growthRoot;
			}
			if (TRAIT_ENTRY.Type == "Structure") {
				parent = structureRoot;
			}
			if (TRAIT_ENTRY.Type == "Specialization") {
				parent = specializationRoot;
			}
			if (TRAIT_ENTRY.Type == "Logistic") {
				parent = logisticRoot;
			}
			TRAIT_BUTTON_OBJECT.transform.SetParent(parent, false);
			TRAIT_BUTTON_OBJECT.GetComponent<Button>().onClick.AddListener(() => OnSelectTrait(id));
			TRAIT_BUTTONS_LIBRARY.Add(id, TRAIT_BUTTON_OBJECT);

			GameObject Trait_Purchased = Instantiate(traitPurchased);
			TextMeshProUGUI Trait_Purchased_Text = Trait_Purchased.GetComponent<TextMeshProUGUI>();
			Trait_Purchased.tag = "TraitPurchased";
			Trait_Purchased.name = id;
			Trait_Purchased_Text.text = TRAIT_ENTRY.Name;
			Trait_Purchased.transform.SetParent(purchasedRoot, false);
			TRAIT_PURCHASED.Add(id, Trait_Purchased);
		}
	}

	public void OnClickTraitsScreenButton() {
		if (!traitsPanel.gameObject.activeSelf) GameManager._.PushUIState(UIState.TraitsScreen);
		else if (traitsPanel.gameObject.activeSelf) { GameManager._.PopUIState(); return; }

		var Selected = GameManager.selected;
		if (traitsPanel.gameObject.activeSelf) {
			print($"{Selected.coordination} [{string.Join(" ", Selected.traits)}] ({string.Join(" ", Selected.availableTraits)}) <{string.Join(" ", Selected.disabledTraits)}>");
			foreach(var trait in TRAIT_BUTTONS_LIBRARY) {
				trait.Value.SetActive(false);
			}
			foreach(var id in Selected.availableTraits) {
				var TRAIT_ENTRY = TraitsReference._this[id];
				var FinalCost = TRAIT_ENTRY.Cost * Selected.stats.Getf("GeneticsDiscount");
				TRAIT_BUTTONS_LIBRARY[id].SetActive(true);
				TRAIT_BUTTONS_LIBRARY[id].GetComponent<Button>().interactable = Selected
					.stats.IsSufficient(Key.Genetics, FinalCost);
				TRAIT_BUTTONS_LIBRARY[id].GetComponentInChildren<TextMeshProUGUI>().text =
					$"<style=Genetic>{FinalCost.Floorf(2)}<sprite=\"Resources\" index=0></style><pos=20%>{TRAIT_ENTRY.Name}";
			}

			var texts = purchasedRoot.GetComponentsInChildren<TextMeshProUGUI>();
			foreach (var item in texts) {
				if (item.name != "Head") { item.gameObject.SetActive(false); }
			}
			foreach (var id in Selected.traits) {
				TRAIT_PURCHASED[id].SetActive(true);
			}
		}
	}

	public void RefreshTraitsScreenButton() {
		foreach (var id in GameManager.selected.traits) {
			TRAIT_PURCHASED[id].SetActive(true);
		}
		RefreshAvailableTraitsButton();
		foreach (var id in GameManager.selected.disabledTraits) {
			TRAIT_BUTTONS_LIBRARY[id].SetActive(false);
		}
	}

	public void RefreshAvailableTraitsButton() {
		var Selected = GameManager.selected;
		foreach (var id in Selected.availableTraits) {
			var FinalCost = TraitsReference._this[id].Cost * Selected.stats.Getf("GeneticsDiscount");
			TRAIT_BUTTONS_LIBRARY[id].SetActive(true);
			TRAIT_BUTTONS_LIBRARY[id].GetComponent<Button>().interactable = Selected.stats.IsSufficient(Key.Genetics, FinalCost);
		}
	}

	void OnSelectTrait(string ID) {
		Trait trait = TraitsReference._this[ID];
		bool complete = GameManager.selected.stats.Deduct(Key.Genetics, trait.Cost);
		if (complete) {
			GameManager.selected.AddTrait(ID);
			TRAIT_BUTTONS_LIBRARY[ID].SetActive(false);


			foreach (var id in trait.Disable) {
				if (id.Contains("t")) TRAIT_BUTTONS_LIBRARY[id].SetActive(false);
			}
			foreach (var id in trait.Unlock) {
				if (id.Contains("t")) TRAIT_BUTTONS_LIBRARY[id].SetActive(true);
			}
			RefreshTraitsScreenButton();
		}
	}

#region Universal Behavoiur
	public void OnPointerDown() {
		mouseDowned = true;
	}

	public void OnPointerUp() {
		mouseDowned = false;
		if (mouseLeaved) cameraControl.SendMessage("UnsetUIBlocking", 0.05f);
	}

	public void OnPointerEnter(PointerEventData e) {
		mouseLeaved = false;
		cameraControl.SendMessage("SetUIBlocking");
	}
	public void OnPointerExit(PointerEventData e) {
		var pointAtCell = GameManager._.pointerData.pointerCurrentRaycast;
		if (pointAtCell.isValid && pointAtCell.gameObject.tag == "Cells") {
			pointAtCell.gameObject.GetComponent<CellInteraction>().highlight.colorGradient = _Color.Gradient(GameManager._.hoverColor);
		}
		mouseLeaved = true;
		if (!mouseDowned) cameraControl.SendMessage("UnsetUIBlocking", 0f);
	}
#endregion

	void OnClickMenuButton() {
		GameManager._.PauseMenuHandler();
	}

#region GAME SPEED CONTROL
	void OnClickPauseButton() {
		GameManager._.SetGameSpeed(0);
	}

	void OnClickNormalSpeedButton() {
		GameManager._.SetGameSpeed(1);
	}

	void OnClickFastSpeedButton() {
		GameManager._.SetGameSpeed(2);
	}

	void OnClickFasterSpeedButton() {
		GameManager._.SetGameSpeed(3);
	}
#endregion

	public void GameSpeedButtonHighlight(int speed) {
		pauseButton.colors = normalSpeedButton.colors = fastSpeedButton.colors = fasterSpeedButton.colors = defaultButtonScheme;
		switch (speed) {
			case 0:
				pauseButton.colors = activeButtonScheme;
				break;
			case 1:
				normalSpeedButton.colors = activeButtonScheme;
				break;
			case 2:
				fastSpeedButton.colors = activeButtonScheme;
				break;
			case 3:
				fasterSpeedButton.colors = activeButtonScheme;
				break;
			default: break;
		}
	}

	public void OnResumeButtonPressed() {
		GameManager._.EscapeKeyHandler();
	}

	public void CheckPanelTooltip() {
		bool isAtCellScreen = GameManager.AtUI(UIState.CellScreen);
		float panelTopPosition = Mathf.Clamp(Input.mousePosition.y + 5f, panelTooltip.sizeDelta.y, Screen.height);
		
		panelTooltip.gameObject.SetActive(isAtCellScreen);
		if (isAtCellScreen) {
			panelTooltip.localPosition = new Vector2(panelTooltip.localPosition.x, panelTopPosition);

		}
	}

	public void CheckTooltip() {
		if (GameManager._.forceHideTooltip_OnMouseExitFlag) {
			HideTooltip();
			GameManager._.forceHideTooltip_OnMouseExitFlag = false;
			return;
		}
		var pointer = GameManager._.pointerData.pointerCurrentRaycast;
		if (pointer.gameObject == null) {
			HideTooltip();
			return;
		}
		EvaluateTooltip(pointer.gameObject);
	}

	public void HideTooltip() {
		pointerTooltipPanel.GetComponent<Image>().enabled = false;
		pointerTooltipText.enabled = false;
		pointerTooltipPanel.gameObject.SetActive(false);
	}

	public void ShowTooltip(string message="") {
		pointerTooltipPanel.gameObject.SetActive(true);
		if (message == "") UpdateTooltipPosition();
		if (message != "") {
			pointerTooltipText.SetText(message);
			UpdateTooltipPosition();
		}
		if (GameManager._.pointerInactiveTime > 0.5) {
			pointerTooltipText.enabled = true;
			pointerTooltipPanel.GetComponent<Image>().enabled = true;
		}
	}

	public void UpdateTooltipPosition() {
		float maxWidth = Screen.width;
		float tooltipWidth = pointerTooltipPanel.sizeDelta.x * GameManager._.CanvasRatio;
		float tooltipHeight = pointerTooltipPanel.sizeDelta.y * GameManager._.CanvasRatio;
		float tooltipBoundX = maxWidth - tooltipWidth;
		float tooltipBoundY = Screen.height;
		float originX = Input.mousePosition.x + tooltipOffset.x;
		float originY = Input.mousePosition.y + tooltipOffset.y;

		if (Input.mousePosition.x > tooltipBoundX) {
			if (Input.mousePosition.y < tooltipHeight) {
				if (Input.mousePosition.x + 5f > tooltipWidth) {
					pointerTooltipRoot.pivot = new Vector2(0, 1);
					pointerTooltipRoot.position = new Vector2(Mathf.Clamp(Input.mousePosition.x, 0, Input.mousePosition.x - (tooltipWidth + 5f)), Mathf.Clamp(originY, tooltipHeight, Screen.height));
				}
			}
		}
		if (Input.mousePosition.x <= tooltipBoundX || Input.mousePosition.y > tooltipHeight) {
			pointerTooltipRoot.pivot = new Vector2(0, 1);
			pointerTooltipRoot.position = new Vector2(Mathf.Clamp(originX, 0, tooltipBoundX), Mathf.Clamp(originY, tooltipHeight, Screen.height));
		}
	}

	public void OnGameOver(string message) {
		gameOverDialogue.gameObject.SetActive(true);
		gameOverDialogueMessage.text = message;
	}

	public void OnKeepSpectating() {
		gameOverDialogue.gameObject.SetActive(false);
	}

	public void OnRestart() {
		gameOverDialogue.gameObject.SetActive(false);
		GameManager._.RestartGame();
	}

	public void OnMainMenuButtonPressed() {
		//SceneManager.LoadSceneAsync("menu");
		gameOverDialogue.gameObject.SetActive(false);
		GameManager._.DestroyGameData();
		GameManager._.OnReturnToWelcomeScreen();
	}

	public void OnQuitButtonPressed() {
		Application.Quit();
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#endif
	}

	private void EvaluateTooltip(GameObject pointAt) {
		CellData cell;
		Upgrade upgrade;
		Trait trait;
		string red = "<#E42>", green = "<#BF2>", gold = "<#FC5>", grey = "<#DCB>";
		string upgradeID;
		switch (pointAt.tag) {
			case "Cells":
				cell = pointAt.GetComponent<CellData>();
				ShowTooltip(
					(cell.HasCell ?
						$"<color={cell.owner.COLOR.ToHex()}>{cell.owner.NAME} ({cell.owner.CellCount} Cells)</color> " +
						$"{$"{(int)cell.stats[Key.Health]} {Key.Health}".Style(Key.Health)}   Age {cell.Age.Floor()} / {(int)cell.LIFESPAN}\n" : "") +
						$"{(int)cell.tile[Key.Food]} Food. ".Style("Growth") +
						$"{(int)cell.TemperatureValue} <sup>o</sup>C. ".Style("Temp") +
						$"{(int)cell.tile[Key.Moisture]} Moisture. ".Style("Moisture") +
						$"{(int)cell.tile[Key.Oxygen]}% Oxygen.\n".Style("Oxygen") +
						(cell.owner.NAME != "None" ?
							$"{cell.CellStatsToRich}" : "") + $"\n{cell.statusModifier}"
					);
				return;
			case "UpgradeEntries":
				cell = GameManager.selected;
				upgradeID = pointAt.name.Split(' ')[0];
				upgrade = UpgradeReference._this[upgradeID];
				ShowTooltip(
					$"<line-height=0.1><color=#6FF><b>{upgrade.Name}</b></color>\n" + $"<align=right>{gold}{(cell.UPGRADE_ENERGY_COST_REMAINING(upgradeID) /cell.ENERGY_OUTPUT).Floorf(1)} cycles</color></align></line-height>" +
					
					(upgrade.Description == "" ? "" :
					$"\n{grey}{upgrade.Description}</color>") +
					
					(upgrade.Costs.Count == 0 && upgrade.Space == 0 ? "" :
					$"\n<b>Require</b>" +
					$"{(upgrade.Space > 0 ? $" : <b>{(cell.stats.IsSufficient(Key.Space, upgrade.Space) ? green:red)}{upgrade.Space} Space</color></b>" : "")}"+
					$"{(upgrade.Costs.Count > 0 ? $"\n{upgrade.CostsToString(AttributeAppearance.Keyword)}" : "")}") +
					
					(upgrade.Effects.Count == 0 ? "" :
					$"\n<b>Upgrade</b>\n" +
					$"{upgrade.RewardsToString(AttributeAppearance.Keyword)}") +

					(upgrade.Unlock.Count == 0 ? "" :
					$"\n<b>Unlocked Upgrade/Trait</b>\n{upgrade.GetNames(upgrade.Unlock, "\n")}") +

					(upgrade.Disable.Count == 0 ? "" :
					$"\n<b>Disabled Upgrade/Trait</b>\n{upgrade.GetNames(upgrade.Disable, "\n")}")
					);
				return;
			case "UpgradeProgress":
				cell = GameManager.selected;
				upgradeID = pointAt.name.Split(' ')[0];
				upgrade = UpgradeReference._this[upgradeID];
				float progress = cell.progressOfUpgrades.Get(pointAt.name);
				float cost = upgrade.Energy * cell.GetCombinedStat(Key.Energy.Discount());
				ShowTooltip(
					$"Upgrading: {upgrade.Name}\n" + 
					$"Progress <style=Energy>{progress.Floor(1)}</style> of <style=Energy>{cost.Floor(1)}</style>\n" +
					$"Complete at <style=Cycle>Cycle {(GameManager.CycleCount + cell.UPGRADE_CYCLE_REMAINING(upgradeID)).Floor()}"
					);
				return;
			case "UpgradeCancel":
				ShowTooltip($"Cancel Upgrade");
				return;
			case "TraitText":
				trait = TraitsReference._this[pointAt.name];
				cell = GameManager.selected;
				var finalCost = trait.Cost * cell.stats.Getf("GeneticsDiscount");
				ShowTooltip(
					$"{trait.Description}\n" +
					$"Cost: {(finalCost).Floorf(2)} Genetic\n" + 
					(!cell.stats.IsSufficient(Key.Genetics, finalCost) ? $"<color=#F33>(need {(finalCost - cell.stats.Getf(Key.Genetics)).Floorf(2)} more)</color>\n" : "") +
					(trait.Unlock.Count > 0 ? $"<b>Unlocked Upgrade/Trait</b>\n{trait.GetNames(trait.Unlock, "\n")}\n" : "") +
					(trait.Disable.Count > 0 ? $"<b>Disabled Upgrade/Trait</b>\n{trait.GetNames(trait.Disable, "\n")}\n" : "")
				);
				return;
			case "TraitPurchased":
				trait = TraitsReference._this[pointAt.name];
				cell = GameManager.selected;
				var _finalCost = trait.Cost * cell.stats.Getf("GeneticsDiscount");
				ShowTooltip(
					$"{trait.Description}\n" +
					$"Cost: {(_finalCost).Floorf(2)} Genetic\n" +
					(trait.Unlock.Count > 0 ? $"<b>Unlocked Upgrade/Trait</b>\n{trait.GetNames(trait.Unlock, "\n")}\n" : "") +
					(trait.Disable.Count > 0 ? $"<b>Disabled Upgrade/Trait</b>\n{trait.GetNames(trait.Disable, "\n")}\n" : "")
				);
				return;
			case "FocusEntries":
				cell = GameManager.selected;
				ShowTooltip($"{cell.focus[pointAt.transform.parent.name].GetEffectString()}");
				return;
			default:
				//print(pointAt.name + " " + pointAt.tag);
				break;
		}
		switch (pointAt.name) {
			case "MenuButton":
				ShowTooltip($"Open game menu");
				return;
			case "MenuClosingButton":
				ShowTooltip($"Close");
				return;
			case "FoodPerCycle":
				ShowTooltip(/*$"{UI.Highlight("Resource", $"{cell.ResourcePoint} Resource Point ({cell.ResourceUsed} Used)")}\n" +
					$"{UI.Highlight((int)cell._Resource_ModifiedBase)} from Base Resource point\n" +
					$"{UI.Highlight((int)cell._Resource_BonusFromFood)} from every {cell.foodToResourceRatio} Food ({(int)cell.cellStats["Food"]} Food)\n" +
					((int)cell._Resource_Penalties != 0 ? $"{UI.Highlight((int)cell._Resource_Penalties)} from every {cell.energyToResourceRatio} Energy\n" : "") +*/
					$"Cell size growth rate");
				return;
			case "ConsumptionPerCycle":
				ShowTooltip($"Resource Used\nMaintain resource usage within resource point to maximize cell's lifespan.");
				return;
			case "ProductionPerCycle":
				ShowTooltip($"Energy\nEnergy output translate to productivity of cell. Improve upgrade speed.");
				return;
			case "ProteinPerCycle":
				ShowTooltip($"Protein per cycle\nRate of protein production in a cell. Use as a resource for upgrade.");
				return;
			case "GeneticPerCycle":
				ShowTooltip($"Genetic Score per cycle\nTraits can be unlocked by spending Genetics score.");
				return;
			case "ProteinResource":
				ShowTooltip($"{(int)GameManager.selected.stats[Key.Proteins]} Protein accumulated");
				return;
			case "Budding":
				cell = GameManager.selected;
				ShowTooltip("<line-height=0.1>Budding Reproduction\n<align=right>(B)</align></line-height>\n" +
					"Spawn an outgrown cell into new individual. Starts with full health" +
					$"<b>Require</b> {cell.REPRODUCTION_PROTEINS_COST.Floor(1)}<sprite=\"Resources\" index=1> " +
					$"{cell.REPRODUCTION_CARBS_COST.Floor(1)}<sprite=\"Resources\" index=2> " +
					$"{cell.REPRODUCTION_LIPIDS_COST.Floor(1)}<sprite=\"Resources\" index=3> ");
				return;
			case "Binary":
				cell = GameManager.selected;
				ShowTooltip("<line-height=0.1>Binary Fission Reproduction\n<align=right>(B)</align></line-height>\n" +
					"Split cell into new individual and transfer half of Resources" +
					"(<style=\"Genetic\">Genetics</style>, <style=\"Protein\">Proteins</style>, <style=\"Carb\">Carbohydrates</style>, <style=\"Lipid\">Lipids</style>) to it\n" +
					$"<b>Require</b> {cell.REPRODUCTION_PROTEINS_COST.Floor(1)}<sprite=\"Resources\" index=1> " +
					$"{cell.REPRODUCTION_CARBS_COST.Floor(1)}<sprite=\"Resources\" index=2> " +
					$"{cell.REPRODUCTION_LIPIDS_COST.Floor(1)}<sprite=\"Resources\" index=3> ");
				return;
			case "Move":
				cell = GameManager.selected;
				ShowTooltip("<line-height=0.1>Move Cell\n<align=right>(M)</align></line-height>\n" +
					"Energy Cost scale with cell size. Smaller cell are being moved faster.");
				return;
			case "TraitsPanelButton":
				ShowTooltip("Open Traits Screen (T)");
				return;
			//case "CellHealth":
			//	var data = GameManager._.selected;
			//	if (data && data.FoodBalance < 0) {
			//		ShowTooltip($"Not enough food");
			//	}
			//	break;
			case "CellLifespan":
				cell = GameManager.selected;
				ShowTooltip($"Expected lifespan {(int)cell.stats[Key.Lifespan]} cycles\n" +
					$"Cell age {(int)cell.Age} cycle\n" +
					$"When cell reaches expected lifespan health will degrade.");
				return;
			case "Unit Name":
				cell = GameManager.selected;
				ShowTooltip($"SUMMARY EFFECT\n" +
					$"{string.Join("\n", cell.focus.currentEffect)}\n" +
					$"TOTAL EFFECT\n" +
					$"{string.Join("\n", (cell.stats + cell.focus.currentEffect).Where(stat => cell.focus.currentEffect.Keys.Contains(stat.Key)))}");
				return;
			default:
				//print("HideTooltip");
				HideTooltip();
				return;
		}
	}
}

public static class UIExtension
{
	public static string Style(this string msg, string style) {
		return $"<style={style}>{msg}</style>";
	}
	public static string Highlight(this string msg, string type) {
		bool MSG = (msg != "");
		string color_code = "";
		switch (type) {
			case "Numeric":
				float val;
				bool valid = float.TryParse(msg, out val);
				if (valid) {
					color_code = (val > 0 ? "#1C0" : "#F41"); break;
				}
				else {
					color_code = "#000"; break;
				}
			case "Resource":
				color_code = "#9C2"; break;
			case "Energy":
				color_code = "#EA4"; break;
			case "Protein":
				color_code = "#E93"; break;
			case "Genetic":
				color_code = "#A5F"; break;
			case "Hazard":
				color_code = "#F00"; break;
			case "Health":
				color_code = "#E75151"; break;
			default:
				color_code = "#000"; break;
		}
		return (MSG ? $"<color={color_code}>{msg}</color>" : color_code);
	}

	public static string Highlight(float val, params float[] threshold) {
		string color_code = "";
		if (threshold.Length == 0) {
			color_code = (val < 0 ? "#F41" : "#1C0");
		}
		return $"<color={color_code}>{val}</color>";
	}
}
