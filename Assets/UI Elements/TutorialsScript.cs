﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class PositionString
{
	public Vector2 anchor;
	public Vector2 pos;
	[TextArea(3, 5)]
	public string message;
}
public class TutorialsScript : MonoBehaviour {
	public RectTransform dialogRoot;
	public RectTransform dialog;
	public TextMeshProUGUI messageText;
	public Toggle disableToggle;

	public int step = 0;
	public PositionString[] messages;
	public bool countNow = false;
	public float timer = 0;

	bool openTrait = false;
	bool completed = false;

	void Start() {
		disableToggle.onValueChanged.AddListener(SetDisableTutorial);
	}

	public void SetDisableTutorial(bool value) {
		dialog.gameObject.SetActive(false);
		PlayerPrefs.SetInt("Enable Tutorial", 0);
	}

	public void SetTutorial(bool value) {
		dialog.gameObject.SetActive(value);
		if (value) {
			openTrait = false; completed = false; step = 0;
		}
		else {
			completed = true; step = 0;
		}
	}

	void Update () {
		dialog.localPosition = messages[step].pos;
		dialog.anchorMin = messages[step].anchor;
		dialog.anchorMax = messages[step].anchor;
		dialog.pivot = messages[step].anchor;
		dialogRoot.pivot = messages[step].anchor;
		dialogRoot.anchorMin = messages[step].anchor;
		dialogRoot.anchorMax = messages[step].anchor;
		messageText.text = messages[step].message;
		AutoAdvancer();
		if (countNow && timer < 1.25f) timer += Time.unscaledDeltaTime;
		if (timer >= 1.25f) {
			timer = 0; countNow = false;
			NextMessage();
		}
	}

	public void NextMessage() {
		step = ++step % messages.Length;
	}

	public void AutoAdvancer() {
		if (step == 0 && Input.GetButtonUp("Back")) {
			countNow = true;
		}
		else if (step == 1 && GameManager.selected) {
			countNow = true;
		}
		else if (step == 3 && Input.GetKeyUp(KeyCode.Space)) {
			countNow = true;
		}
		else if (step == 6 && GameManager.AtUI(UIState.TraitsScreen)) {
			openTrait = true;
			if (openTrait == true && GameManager.AtUI(UIState.CellScreen)) timer = 1.3f;
		}
		else if (step == 7) {
			completed = true;
		}
		else if (step == 0 && completed) {
			SetDisableTutorial(true);
		}
	}
}
