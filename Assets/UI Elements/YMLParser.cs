﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

public class YMLParser : MonoBehaviour {

	// Use this for initialization
	void Start () {
		ReadResourcesFile();
	}

	private void Update() {
		if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyUp(KeyCode.BackQuote)) {
			ReadResourcesFile();
		}
	}

	public void ReadResourcesFile() {
		GameManager._.upgradeHandler.ClearAvailableUpgrade();
		GameManager._.upgradeHandler.ClearTraits();
		ReadUpgradeFile();
		ReadTraitFile();
		ReadModifierFile();
		GameManager._.upgradeHandler.InstantiateUpgradeButtons();
		GameManager._.UImanager.InstatntiateTraitEntires();
		print("Reloaded Resources");
	}

	public void ReadUpgradeFile() {
		using (var reader = File.OpenText(Application.dataPath + "/Resources/upgrades.yml")) {
			var deserializerBuilder = new DeserializerBuilder().WithNamingConvention(new CamelCaseNamingConvention());
			var deserializer = deserializerBuilder.Build();
			var data = deserializer.Deserialize<Dictionary<string, Upgrade>>(reader);
			UpgradeReference._this = data;
		}
	}

	public void ReadTraitFile() {
		using (var reader = File.OpenText(Application.dataPath + "/Resources/traits.yml")) {
			var deserializerBuilder = new DeserializerBuilder().WithNamingConvention(new CamelCaseNamingConvention());
			var deserializer = deserializerBuilder.Build();
			var data = deserializer.Deserialize<Dictionary<string, Trait>>(reader);
			TraitsReference._this = data;
		}
	}

	public void ReadModifierFile() {
		using (var reader = File.OpenText(Application.dataPath + "/Resources/modifiers.yml")) {
			var deserializerBuilder = new DeserializerBuilder().WithNamingConvention(new CamelCaseNamingConvention());
			var deserializer = deserializerBuilder.Build();
			var data = deserializer.Deserialize<Dictionary<string, Modifier>>(reader);
			ModifierReference.This = new Dictionary<string, Modifier>(data);
			foreach (var it in data) {
				var item = it.Value;
				if (ModifierReference.This.ContainsKey(it.Key)) ModifierReference.This[it.Key].Name = it.Key;
				foreach (var secondary in item.Secondaries) {
					ModifierReference.This[secondary.Name] = secondary;
				}
			}
			//Debug.Log(ModifierReference.This.Count +"\n"+ string.Join("---\n", ModifierReference.This));
		}
	}

	public string DictToString(Dictionary<string, float> d) {
		return "[" + string.Join(", ", d.Select(kvp => kvp.Key + ": " + kvp.Value.ToString())) + "]";
	}
	public string ArrToString(string[] s) {
		return "[" + string.Join(", ", s) + "]";
	}
}

public abstract class Record
{
	public string Name { get; set; }
	public string Description { get; set; }
	public HashSet<string> Require { get; set; } = new HashSet<string>();
	public HashSet<string> Unlock { get; set; } = new HashSet<string>();
	public HashSet<string> Disable { get; set; } = new HashSet<string>();
  public Dictionary<string, float> Effects { get; set; } = new Dictionary<string, float>();
  public List<string> Modifiers { get; set; } = new List<string>();

  public string GetNames(HashSet<string> collection, string delim = " ") {
		List<string> result = new List<string>();
		foreach (string id in collection) {
			if (id[0] == 'u') {
				result.Add($"<color=#6FF>{UpgradeReference.GetName(id)}</color>");
			}
			else if (id[0] == 't') {
				result.Add($"<color=#B8F>{TraitsReference.GetName(id)}</color>");
			}
		}
		return string.Join(delim, result);
	}

	public static string GetSuperVariant(string id) {
		return id.Split('.')[0];
	}
}

public class Trait: Record
{
	public string Type { get; set; }
	public float Cost { get; set; }
}

public class TraitsReference
{
	public static Dictionary<string, Trait> _this { get; set; }
	public static string GetName(string id) {
		return _this[id].Name;
	}
	public static string GetNames(HashSet<string> collection, string format = "\n") {
		return string.Join(format, collection.Select(c =>  _this[c].Name));
	}
}

public class Upgrade: Record
{
	public float Energy { get; private set; }
	public float Space { get; set; } = 1;
	public Dictionary<string, float> Costs { get; set; } = new Dictionary<string, float>();
	public List<Focus> Focus { get; set; } = new List<Focus>();

	public string CostsToString(AttributeAppearance option = 0) {
		List<string> s = new List<string>();
		foreach (var item in Costs) {
			s.Add(ToRichText(item, option));
		}
		return "<space=7>" + string.Join("\n<space=7>", s);
	}

	public string RewardsToString(AttributeAppearance option = 0) {
		List<string> s = new List<string>();
		foreach (var item in Effects) {
			//s += item + " ";
			s.Add(ToRichText(item, option, NumericalAppearance.Signed));
		}
		return "<space=7>" + string.Join("\n<space=7>", s);
	}

	public string ToRichText(KeyValuePair<string, float> kvp, AttributeAppearance attribute = 0, NumericalAppearance signed = 0) {
		string key = GetDisplayKeyword(kvp.Key);
		float Value = kvp.Value;
		string value = "";
		bool isMultiplier = kvp.Key.ContainsAny(Key.Multiplier);
		if (signed == NumericalAppearance.Signed)
			if (isMultiplier) value = (Value - 1).ToPercentfS(2);
			else value = Value.FloorfS(2);
		if (signed != NumericalAppearance.Signed)
			if (isMultiplier) value = (Value - 1).ToPercentf(2).ToString();
			else value = Value.Floorf(2).ToString();

		if (key == Key.Energy) {
			return $"{value} Energy".Style(key);
		}
		else if (key == Key.Proteins) {
			return $"{value} Protein".Style(Key.Protein);
		}
		else if (key == Key.Protein) {
			return $"{value} Protein per cycle".Style(Key.Protein);
		}
		else if (key == Key.Carbs) {
			return $"{value} {Key.Carb}".Style("Carb");
		}
		else if (key == Key.Carb) {
			return $"{value} {Key.Carb} per cycle".Style("Carb");
		}
		else if (key == Key.Lipids) {
			return $"{value} {Key.Lipid}".Style(Key.Lipid);
		}
		else if (key == Key.Lipid) {
			return $"{value} {Key.Lipid} per cycle".Style(Key.Lipid);
		}
		else if (key == Key.Waste) {
			return $"{value} {key}".Style(Key.Waste);
		}
		else if (key == Key.MaxHealth) {
			return $"{value} {key}".Style(Key.Health);
		}

		return $"{value} {key}";
	}

	string GetDisplayKeyword(string key) {
		if (key == "ReproductionEnergyCost_GenerationPenalty") return "Extra Reproduction Energy cost per <b>Generation</b>";
		else if (key == "ReproductionEnergyCost_AdjacentPenalty") return "Extra Reproduction Energy cost per <b>Friendly Adjacent cell</b>";
		else if (key == "GrowthRate") return "Growth Rate";
		else if (key == "AgingRate") return "Aging Rate";
		else if (key == "LifespanModifier") return "Lifespan";
		else if (key == "BreachResistance") return "Breaching Resistance";
		else if (key == "EnergyModifier") return "Energy";
		else if (key == "ReproductionProteinsCost_Modifier") return "Reproduction Protein Cost";
		else if (key == "FocusPoint") return "Focus Point";
		else if (key == "MaxHealth") return "Max Health";
		else return key;
	}

}


public class UpgradeReference
{
	public static Dictionary<string, Upgrade> _this { get; set; }

	public static string GetName(string id) {
		return _this[id].Name;
	}
	public static string GetNames(IEnumerable<string> collection, string format = "\n") {
		return string.Join(format, collection.Select(c => _this[c].Name));
	}

	public static float GetEnergyCost(string id) {
		return _this[id].Energy;
	}

	public static float GetSpaceCost(string id) {
		return _this[id].Space;
	}
}

public class ModifierReference
{
	public static Dictionary<string, Modifier> This { get; set; }
}