﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class UpgradeHandler : MonoBehaviour {
	//public RectTransform panelRect;
	//public float panelXPosition;
	public RectTransform InGameRect;
	public float InGameRectX;
	public bool isShowing = false;
	[Header("Lists Root GameObject")]
	public RectTransform _productionQueueTransform;
	public GameObject _upgradeAvailableRoot;
	public GameObject _reproductionRoot;
	public TextMeshProUGUI[] _cellStats;
	public TextMeshProUGUI _cellStatsLifespan;
	public TextMeshProUGUI _cellStatsHealth;
	public TextMeshProUGUI _cellStatsResource;
	public TextMeshProUGUI _cellStatsConsume;
	public TextMeshProUGUI _cellStatsProduction;
	public TextMeshProUGUI _cellStatsProtein;
	public TextMeshProUGUI _cellStatsGenetic;

	public TextMeshProUGUI _cellResourceGENETICS;

	public Toggle moveButton;
	public Toggle buddingButton;
	public Toggle binaryButton;

	[Header("Entries Prefab")]
	public GameObject _productionEntry;
	public Button _upgradeEntry;

	Dictionary<string, Button> UpgradeUIReference = new Dictionary<string, Button>();
	List<GameObject> upgradeQueueUI = new List<GameObject>();
	//RectTransform _productionQueueTransform;

	void Start () {
		//_productionQueueTransform = _productionQueueRoot.GetComponent<RectTransform>();
		//string.Join(" ", TraitsReference._this);

		//panelRect = GetComponent<RectTransform>();
		//panelXPosition = Screen.width;
		//panelRect.position = new Vector2(panelXPosition + panelRect.sizeDelta.x, panelRect.position.y);
		//GameManager._._UImanager.GameSpeedPanel.position = new Vector2(panelXPosition, GameManager._._UImanager.GameSpeedPanel.position.y);

		InGameRect = GameManager._.UImanager.INGAME_UI_GROUP.GetComponent<RectTransform>();
		InGameRect.offsetMax = new Vector2(0, InGameRect.offsetMax.y);

		moveButton.onValueChanged.AddListener(OnMoveToggled);
		buddingButton.onValueChanged.AddListener(OnBuddingToggled);
		binaryButton.onValueChanged.AddListener(OnBinaryToggled);
		//print(panelRect);
	}

	public void InstantiateUpgradeButtons() {
		foreach (var id in UpgradeReference._this.Keys) {
			if (id[0] == 'u') {
				UpgradeUIReference[id] = InstantiateAvailableUpgrade(id);
			}
		}
		print(UpgradeUIReference.Count);
	}

	void Update() {
		//if (isShowing && panelXPosition != Screen.width + panelRect.sizeDelta.x) panelXPosition = Screen.width + panelRect.sizeDelta.x * GameManager._.CanvasRatio;
		//if (!isShowing && panelXPosition != Screen.width) panelXPosition = Screen.width;
		//if (Mathf.Abs(panelRect.position.x - panelXPosition) > 0.25f) {
		//	float newXPosition = Mathf.Lerp(panelRect.position.x, panelXPosition, Time.unscaledDeltaTime * 20f);
		//	panelRect.position = new Vector2(newXPosition, panelRect.position.y);
		//	GameManager._._UImanager.GameSpeedPanel.position = new Vector2(newXPosition - panelRect.sizeDelta.x * GameManager._.CanvasRatio, GameManager._._UImanager.GameSpeedPanel.position.y);
		//}
		if (!isShowing && InGameRectX > -250)  InGameRectX = -250;
		if (isShowing && InGameRect.offsetMax.x < 0) InGameRectX = 0;
		if (Mathf.Abs(InGameRect.offsetMax.x - InGameRectX) > 0.25f) {
			float newXOffset = Mathf.Lerp(InGameRect.offsetMax.x, InGameRectX, Time.unscaledDeltaTime * 20f);
			InGameRect.offsetMax = new Vector2(newXOffset, InGameRect.offsetMax.y);
		}

		if (GameManager._.CurrentUI == UIState.CellScreen || GameManager._.CurrentUI == UIState.TraitsScreen) {
			var SELECTED = GameManager.selected;
			GameManager._.focusHandler.gameObject.SetActive(SELECTED.HasCell);
			_reproductionRoot.SetActive(SELECTED.HasCell);
			_productionQueueTransform.gameObject.SetActive(SELECTED.HasCell);
			_upgradeAvailableRoot.SetActive(SELECTED.HasCell);


			UpdateCellStatus(SELECTED);
			UpdateAvailableUpgrades(SELECTED);

			bool reproductionSufficiency = SELECTED.stats.IsSufficient(Key.Proteins, SELECTED.REPRODUCTION_PROTEINS_COST) &&
			SELECTED.stats.IsSufficient(Key.Carbs, SELECTED.REPRODUCTION_CARBS_COST) &&
			SELECTED.stats.IsSufficient(Key.Lipids, SELECTED.REPRODUCTION_LIPIDS_COST);

			binaryButton.gameObject.SetActive(SELECTED.upgrades.Contains("u10.1"));
			binaryButton.interactable = reproductionSufficiency;
			binaryButton.image.sprite = reproductionSufficiency ? binaryButton.spriteState.highlightedSprite : binaryButton.spriteState.pressedSprite;
			buddingButton.gameObject.SetActive(SELECTED.upgrades.Contains("u10.2"));
			buddingButton.interactable = reproductionSufficiency;
			buddingButton.image.sprite = reproductionSufficiency ? buddingButton.spriteState.highlightedSprite : buddingButton.spriteState.pressedSprite;

			moveButton.gameObject.SetActive(SELECTED.traits.Contains("t10"));
			moveButton.GetComponentInChildren<TextMeshProUGUI>().text = $"Move Cell\n({SELECTED.stats[Key.Size]}<sprite=\"Resources\" index=4>)\n";

			buddingButton.GetComponentInChildren<TextMeshProUGUI>().text = $"Budding ({SELECTED.REPRODUCTION_ENERGY_COST.Floor(1)}<sprite=\"Resources\" index=4>)\n" +
				$"{(SELECTED.stats.IsSufficient(Key.Proteins, SELECTED.REPRODUCTION_PROTEINS_COST) ? "<#FFF>" : "<#A00>")}" +
				$"{SELECTED.REPRODUCTION_PROTEINS_COST.Floor(1)}<sprite=\"Resources\" index=1> " +

				$"{(SELECTED.stats.IsSufficient(Key.Proteins, SELECTED.REPRODUCTION_CARBS_COST) ? "<#FFF>" : "<#A00>")}" +
				$"{SELECTED.REPRODUCTION_CARBS_COST.Floor(1)}<sprite=\"Resources\" index=2> " +

				$"{(SELECTED.stats.IsSufficient(Key.Proteins, SELECTED.REPRODUCTION_LIPIDS_COST) ? "<#FFF>" : "<#A00>")}" +
				$"{SELECTED.REPRODUCTION_LIPIDS_COST.Floor(1)}<sprite=\"Resources\" index=3>";

			binaryButton.GetComponentInChildren<TextMeshProUGUI>().text = $"Binary ({SELECTED.REPRODUCTION_ENERGY_COST.Floor(1)}<sprite=\"Resources\" index=4>)\n" +
				$"{(SELECTED.stats.IsSufficient(Key.Proteins, SELECTED.REPRODUCTION_PROTEINS_COST) ? "<#FFF>" : "<#A00>")}" +
				$"{SELECTED.REPRODUCTION_PROTEINS_COST.Floor(1)}<sprite=\"Resources\" index=1> " +

				$"{(SELECTED.stats.IsSufficient(Key.Proteins, SELECTED.REPRODUCTION_CARBS_COST) ? "<#FFF>" : "<#A00>")}" +
				$"{SELECTED.REPRODUCTION_CARBS_COST.Floor(1)}<sprite=\"Resources\" index=2> " +

				$"{(SELECTED.stats.IsSufficient(Key.Proteins, SELECTED.REPRODUCTION_LIPIDS_COST) ? "<#FFF>" : "<#A00>")}" +
				$"{SELECTED.REPRODUCTION_LIPIDS_COST.Floor(1)}<sprite=\"Resources\" index=3>";
		}
	}

	public void UpdateCellStatus(CellData cell) {
		_cellStatsLifespan.text = $"Lifespan {cell.Age.Floor(1)}/{cell.LIFESPAN.Floor(1)} cycles";
		_cellStatsHealth.text = $"<size=80%><sprite=2></size> {(cell.HEALTH).Floor(1)}/{cell.MAXHEALTH.Floor(1)}";
		_cellStats[0].text = $"<align=center>Size {cell.stats[Key.Size]} | Used {cell.SpaceRemaining}";
		_cellStats[1].text = $"<style=col-l><style=Growth>Growth\n" +
			$"<style=col-c>({cell.GrowthProgressOfCurrentSize.Floor(1)}/{cell.GrowthCostCurrentDifference.Floor(1)})\n" +
			$"<style=col-r>{cell.SizeGrowthRate.FloorS(2)}";
		_cellStats[2].text = $"<style=Waste>Waste ({cell.GetCombinedStat(Key.Waste)}/24)";
		_cellStats[3].text = $"<style=Energy>Energy {cell.ENERGY_OUTPUT.FloorS(2)}<size=72.5%><sprite=3>";
		_cellStats[4].text = $"<style=Genetic><style=col-l>Genetic\n<style=col-c>{cell.GENETIC_OUTPUT.FloorS(2)}\n<style=col-r>{cell.stats[Key.Genetics].Floor()} <sprite=\"Resources\" index=0>";
		_cellStats[5].text = $"<style=Protein><style=col-l>Protein\n<style=col-c>{cell.PROTEIN_OUTPUT.FloorS(2)}\n<style=col-r>{cell.stats[Key.Proteins].Floor()} <sprite=\"Resources\" index=1>";
		_cellStats[6].text = $"<style=Carb><style=col-l>Carbohydrate\n<style=col-c>{cell.CARB_OUTPUT.FloorS(2)}\n<style=col-r>{cell.stats[Key.Carbs].Floor()} <sprite=\"Resources\" index=2>";
		_cellStats[7].text = $"<style=Lipid><style=col-l>Lipid\n<style=col-c>{cell.LIPID_OUTPUT.FloorS(2)}\n<style=col-r>{cell.stats[Key.Lipids].Floor()} <sprite=\"Resources\" index=3>";
		//TODO: cell Lifespan
		//_cellStatsResource.text = $"<sprite=4>\n{(int)(cell.ResourcePoint) - (int)(cell.ResourceUsed)}";
		//_cellStatsProduction.text = $"<sprite=3>\n{cell.ENERGY_OUTPUT.Floorf(2)}";
		//_cellStatsProtein.text = $"<sprite=1>\n+{cell.PROTEIN_OUTPUT.Floorf(2)}";
		//_cellStatsGenetic.text = $"<sprite=0>\n+{cell.GENETIC_OUTPUT.Floorf(2)}";
		//_cellResourcePROTEINS.text = $"<sprite=\"resourcesSheet\" index=2 color=#FFAA33> <color=#E93>{(int)cell.stats[Key.Proteins]}</color>\n" +
		//	$"<color=#84F>{(int)cell.stats[Key.Genetics]} Genetics</color>";
		_cellResourceGENETICS.text = $"Genetic Score <sprite=\"Resources\" index=0> <style=Genetic>{cell.stats[Key.Genetics].Floor()}";
		GameManager._.UImanager.RefreshAvailableTraitsButton();

		bool reproductionSufficiency = cell.stats.IsSufficient(Key.Proteins, cell.REPRODUCTION_PROTEINS_COST) &&
			cell.stats.IsSufficient(Key.Carbs, cell.REPRODUCTION_CARBS_COST) &&
			cell.stats.IsSufficient(Key.Lipids, cell.REPRODUCTION_LIPIDS_COST);

		buddingButton.interactable = reproductionSufficiency;
		binaryButton.interactable = reproductionSufficiency;
	}

	public void OnMoveToggled(bool isOn) {
		GameManager._MovingCell = isOn;
		if (!isOn) {
			Cursor.SetCursor(GameManager._.cameraController.cursors[9], GameManager._.activePoint, CursorMode.Auto);
		}
	}

	public void OnBuddingToggled(bool isOn) {
		if (isOn) { GameManager._.EnterBuddingMode(); }
		else {			GameManager._.ExitReproductionMode(); }
	}

	public void OnBinaryToggled(bool isOn) {
		if (isOn) { GameManager._.EnterBinaryMode(); }
		else {			GameManager._.ExitReproductionMode(); }
	}

	public void RefreshUpgradeAppearance(Button item) {
		string id = item.name;
		Upgrade upgrade = UpgradeReference._this[id];
		TextMeshProUGUI[] texts = item.GetComponentsInChildren<TextMeshProUGUI>();
		float remainingCycle = GameManager.selected.UPGRADE_CYCLE_REMAINING(id);
		bool sufficiency = GameManager.selected.stats.IsSufficientForAll(id);
		item.enabled = sufficiency;
		item.GetComponent<Image>().color = sufficiency ? Color.white : item.colors.disabledColor;
		texts[0].text = $"{remainingCycle.Floor(1)}\nCycles";
		texts[1].text = (sufficiency ? UpgradeReference.GetName(id) : $"<color=#F55>{UpgradeReference.GetName(id)}</color>");
	}

	public void UpdateAvailableUpgrades(CellData cell) {
		var statusModifier = cell.modifiers;
		foreach (var id in cell.availableUpgrades) {
			var item = UpgradeUIReference[id];
			if (statusModifier.ContainsKey("Developing Phase") || statusModifier.ContainsKey("Developer Phase") || statusModifier.ContainsKey("Immature Phase")) {
				item.interactable = false;
				continue;
			}
			else { item.interactable = true; }

			RefreshUpgradeAppearance(item);
		}
	}

	public void UpdateProgress(string key) {
		var upgradeID = key.Split(' ')[0];
		if (upgradeQueueUI.Count != GameManager.selected.queueOfUpgrade.Count) {
			upgradeQueueUI.ForEach(Destroy);
			upgradeQueueUI.Clear();

			GameManager.selected.queueOfUpgrade.ForEach(item => upgradeQueueUI.Add(InstantiateQueuedUpgrade(item)));
		}
		Image progress = upgradeQueueUI[0].GetComponentInChildren<Image>();
		TextMeshProUGUI remainingCycleText = upgradeQueueUI[0].GetComponentsInChildren<TextMeshProUGUI>()[1];
		float progressFraction = 0;
		float remainingCycles = 0;

		if (key.Contains("u")) {
			progressFraction = GameManager.selected.UPGRADE_ENERGY_PROGRESS(upgradeID);
			remainingCycles = GameManager.selected.UPGRADE_CYCLE_REMAINING(upgradeID);
		}
		else if(key.Contains("a0")) {
			progressFraction = GameManager.selected.progressOfUpgrades[key] / GameManager.selected.stats[Key.Size];
			remainingCycles = (GameManager.selected.stats[Key.Size] - GameManager.selected.progressOfUpgrades[key]) / GameManager.selected.ENERGY_OUTPUT ;
		}
		else if (key.Contains("a1")) {
			progressFraction = GameManager.selected.progressOfUpgrades[key] / GameManager.selected.REPRODUCTION_ENERGY_COST;
			remainingCycles = (GameManager.selected.REPRODUCTION_ENERGY_COST - GameManager.selected.progressOfUpgrades[key]) / GameManager.selected.ENERGY_OUTPUT;
		}
		progress.fillAmount = progressFraction;
		remainingCycleText.text = remainingCycles.Floor(2) + "\nCycle";
	}

	public void OnCompleteUpgradeWhileFocus(CellData cell) {
		Destroy(upgradeQueueUI[0]);
		upgradeQueueUI.RemoveAt(0);
		UpdateCellStatus(cell);
	}

	Button InstantiateAvailableUpgrade(string key) {
		Upgrade upgrade =  UpgradeReference._this[key];
		Button b = Instantiate(_upgradeEntry);
		TextMeshProUGUI[] t = b.gameObject.GetComponentsInChildren<TextMeshProUGUI>();
		b.name = key;
		b.onClick.AddListener(() => IssueUpgrade(b));
		//t[0].text = GameManager.selected.UPGRADE_CYCLE_REMAINING(key).Floor();
		t[1].text = upgrade.Name;
		//t[2].text = upgrade.RewardsToString();

		b.transform.SetParent(_upgradeAvailableRoot.transform, false);
		return b;
	}

	public void ClearAvailableUpgrade() {
		var buttons = _upgradeAvailableRoot.GetComponentsInChildren<Button>();
		foreach (var item in buttons) {
			Destroy(item.gameObject);
		}
		UpgradeUIReference.Clear();
	}

	public void ClearTraits() {
		var entries = new List<Button>(4);
		entries.AddRange(GameManager._.UImanager.growthRoot.GetComponentsInChildren<Button>());
		entries.AddRange(GameManager._.UImanager.specializationRoot.GetComponentsInChildren<Button>());
		entries.AddRange(GameManager._.UImanager.structureRoot.GetComponentsInChildren<Button>());
		entries.AddRange(GameManager._.UImanager.logisticRoot.GetComponentsInChildren<Button>());
		var pruchased = GameManager._.UImanager.purchasedRoot.GetComponentsInChildren<TextMeshProUGUI>();
		foreach (var item in entries) {
			Destroy(item.gameObject);
		}
		foreach (var item in pruchased) {
			Destroy(item.gameObject);
		}
		GameManager._.UImanager.TRAIT_BUTTONS_LIBRARY.Clear();
		GameManager._.UImanager.TRAIT_PURCHASED.Clear();
	}

	GameObject InstantiateQueuedUpgrade(string key, CellData target = null) {
		string upgradeID = key.Split(' ')[0];
		Upgrade upgrade = UpgradeReference._this[key.Split(' ')[0]];
		GameObject p = Instantiate(_productionEntry);
		
		Button b = p.GetComponentInChildren<Button>();
		p.name = key;
		b.name = (target != null ? $"{key} {target.coordination}": $"{key}");
		b.onClick.AddListener(() => CancelUpgrade(b));

		Image progress = p.GetComponentInChildren<Image>();
		Image base_image = progress.GetComponentsInChildren<Image>()[1];
		TextMeshProUGUI[] t = p.GetComponentsInChildren<TextMeshProUGUI>();

		float progressFraction = 0;
		float progressRemainingCycle = 0;

		if (key.Contains("u")) {
			progressFraction = GameManager.selected.UPGRADE_ENERGY_PROGRESS(upgradeID);
			progressRemainingCycle = GameManager.selected.UPGRADE_CYCLE_REMAINING(upgradeID);
		}
		else if (key.Contains("a0")) {
			progressFraction = GameManager.selected.progressOfUpgrades[b.name] / GameManager.selected.stats[Key.Size];
			progressRemainingCycle = (GameManager.selected.stats[Key.Size] - GameManager.selected.progressOfUpgrades[b.name]) / GameManager.selected.ENERGY_OUTPUT;
		}
		else if (key.Contains("a1")) {
			progressFraction = GameManager.selected.progressOfUpgrades[b.name] / GameManager.selected.REPRODUCTION_ENERGY_COST;
			progressRemainingCycle = (GameManager.selected.REPRODUCTION_ENERGY_COST - GameManager.selected.progressOfUpgrades[b.name]) / GameManager.selected.ENERGY_OUTPUT;
		}
		progress.fillAmount = progressFraction;
		base_image.name = key;
		base_image.tag = "UpgradeProgress";
		t[0].text = upgrade.Name;
		t[1].text = progressRemainingCycle.Floor(2) + "\nCycle";

		p.transform.SetParent(_productionQueueTransform, false);
		return p;
	}

	public void ShowDataOfSelectedCell(CellData data) { //call When Clicked Cell
		string debug = "";
		//debug += string.Join("\n", data.stats);
		//debug += string.Join("\n", data.statusModifier);
		UpdateCellStatus(data);

		//foreach (var u_complete in completedUpgrade) {
		//TODO: Add Section of upgraded
		//}

		debug += $"Cell ({(int)data.coordination.x}:{(int)data.coordination.y}) Health({data.stats["Health"]}/{data.stats["MaxHealth"]}) Size({data.stats["Space"]}/{data.stats["Size"]})\n" +
			$"<b>Upgrades</b>\n\t{UpgradeReference.GetNames(data.upgrades, "\n\t")}\n" +
			$"<b>Upgrades Available</b>\n\t{UpgradeReference.GetNames(data.availableUpgrades, "\n\t")}\n" +
			$"<b>Upgrades Disabled</b>\n\t{UpgradeReference.GetNames(data.disabledUpgrades, "\n\t")}\n" +
			$"<b>Traits</b>\n\t{TraitsReference.GetNames(data.traits, "\n\t")}\n" +
			$"<b>Traits Available</b>\n\t{TraitsReference.GetNames(data.availableTraits, "\n\t")}\n" +
			$"<b>Traits Disabled</b>\n\t{TraitsReference.GetNames(data.disabledTraits, "\n\t")}\n";

#region REFRESH AVAILABLE UPGRADES
		foreach (var item in UpgradeUIReference.Values) { item.gameObject.SetActive(false); }
		ShowAvailableUpgradeEntries(data);
		#endregion

#region REFRESH UPGRADE QUEUE
		upgradeQueueUI.ForEach(Destroy);
		upgradeQueueUI.Clear();

		data.queueOfUpgrade.ForEach(item => upgradeQueueUI.Add(InstantiateQueuedUpgrade(item)));
		//foreach (var u_process in data.queueOfUpgrade) { upgradeQueueUI.Add(InstantiateQueuedUpgrade(u_process));	}
		//UpgradeQueue_UIResize();
#endregion
		print(debug);
	}

	public void ShowAvailableUpgradeEntries(CellData data) {
		foreach (var id in data.availableUpgrades.Except(data.queueOfUpgrade)) {
			//float remainingCycle = GameManager.selected.UPGRADE_CYCLE_REMAINING(id);
			//bool isSufficient = GameManager.selected.stats.IsSufficientForAll(id);
			UpgradeUIReference[id].gameObject.SetActive(true);
			RefreshUpgradeAppearance(UpgradeUIReference[id]);
		}
		foreach (var id in data.disabledUpgrades) {
			UpgradeUIReference[id].gameObject.SetActive(false);
		}
	}

	public void IssueUpgrade(Button btn) {
		string id = btn.name;
		Upgrade upgrade = UpgradeReference._this[id];
		string debug = "<b>Issue Upgrade </b>" + upgrade.Name + "\n";

		GameManager.selected.Upgrade_OnIssueProduction(id);
		
		upgradeQueueUI.Add(InstantiateQueuedUpgrade(id));
		btn.gameObject.SetActive(false);

		//UpgradeQueue_UIResize();

		foreach (var cost in upgrade.Costs) {
			GameManager.selected.stats.Deduct(cost.Key, cost.Value);
		}
		print(debug);
	}

	public void IssueAction(string action, CellData target) {
		upgradeQueueUI.Add(InstantiateQueuedUpgrade(action, target));
	}

	public void CancelUpgrade(Button btn) {
		GameObject buttonParent = btn.gameObject.transform.parent.gameObject;
		string upgradeId = buttonParent.name;

		Destroy(buttonParent);
		upgradeQueueUI.Remove(buttonParent);
		//UpgradeQueue_UIResize();
		
		GameManager.selected.Upgrade_OnRemoveProduction(btn.name);

		if (upgradeId.Contains("u")) {
			Upgrade upgrade = UpgradeReference._this[upgradeId];
			foreach (var cost in upgrade.Costs) {
				if (cost.Key == "Energy") continue;
				GameManager.selected.stats.Increment(cost.Key, cost.Value);
			}
			UpgradeUIReference[upgradeId].gameObject.SetActive(true);
		}
		else if (upgradeId.Contains("a1")) {
			GameManager.selected.stats.Increment(Key.Proteins, GameManager.selected.REPRODUCTION_PROTEINS_COST);
			GameManager.selected.stats.Increment(Key.Carbs, GameManager.selected.REPRODUCTION_CARBS_COST);
			GameManager.selected.stats.Increment(Key.Lipids, GameManager.selected.REPRODUCTION_LIPIDS_COST);
		}
	}

	//void UpgradeQueue_UIResize() {
	//	if (upgradeQueueUI.Count < 2) _productionQueueTransform.sizeDelta = new Vector2(_productionQueueTransform.sizeDelta.x, 24f);
	//	else _productionQueueTransform.sizeDelta = new Vector2(_productionQueueTransform.sizeDelta.x, 24f * (upgradeQueueUI.Count));
	//}
}


