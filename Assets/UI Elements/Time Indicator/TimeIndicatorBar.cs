﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeIndicatorBar : MonoBehaviour {
	public RingIndicator indicator;

	void Update () {
		indicator.Display(GameManager.CycleCount, GameManager.SubcycleFraction);
	}
}
