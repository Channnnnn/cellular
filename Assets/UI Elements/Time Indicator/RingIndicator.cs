﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RingIndicator : MonoBehaviour {

	public Image radialSprite;
	public TextMeshProUGUI cycleText;
	// Use this for initialization
	void Start () {
		if (!radialSprite) print($"Unassigned indicator {{{name}}}"); 
	}

	public void Display(float integral, float @decimal) {
		radialSprite.fillAmount = @decimal;
		if (cycleText) cycleText.text = integral.Floor();
	}

	public void Display(float value) {
		radialSprite.fillAmount = value == 1 ? value : value - (float)System.Math.Truncate(value);
		if (cycleText && value > 1) cycleText.text = ((int)value).ToString();
	}
}
