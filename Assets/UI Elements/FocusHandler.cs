﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FocusHandler : MonoBehaviour {
	public GameObject slotGroupPrefab;
	public GameObject slotPrefab;

	public Transform unitRootTransform;
	public TextMeshProUGUI unitHeader;
	[Header("Configuration")]
	public int focusCount = 3;
	//public FocusEntries focusEntries = new FocusEntries();
  
	public List<GameObject> focusUIElement = new List<GameObject>();

	public void Refresh(CellData cell) {

		foreach (var item in focusUIElement) { Destroy(item);	}
		focusUIElement.Clear();

		int i = 0;
		foreach (var focus in cell.focus.Values) {
			GameObject slotGroup = Instantiate(slotGroupPrefab);
			TextMeshProUGUI slotGroupName = slotGroup.GetComponentInChildren<TextMeshProUGUI>();
			slotGroup.name = $"{focus.Name}";			
			focusUIElement.Add(slotGroup);

			for (int j = 0; j < focus.Slots.Count; j++) {
				GameObject slot = Instantiate(slotPrefab);
				Toggle slotToggle = slot.GetComponent<Toggle>();
				slotToggle.name = $"{j}";
				slotToggle.isOn = focus.Slots[j];
				slot.transform.SetParent(slotGroup.transform, false);
				slotToggle.onValueChanged.AddListener(delegate { Toggle(slotToggle); });
				//print($"{slotToggle.name} entry: {i} {j}");
			}

			string effect = "";
			if (focus.GetActiveCount > 0) {
				foreach (var e in focus.Effects) {
					effect += $"+{focus.GetEffect(e.Key)} {e.Key}";
				}
			}
			slotGroupName.text = $"{focus.Name} {effect}";
			slotGroup.transform.SetParent(unitRootTransform, false);
			i++;
		}
		
		unitHeader.text = $"Remaining Focus Points: <align=right><style=Energy>{cell.stats[Key.FocusPoint]}";
	}

	public void Toggle(Toggle t) {
		int self;
		int.TryParse(t.name, out self);
		GameManager.selected.focus.Toggle(t.transform.parent.name, self, t.isOn, GameManager.selected);
		RefreshSlot();
	}

	public void RefreshSlot() {
		var cellFocus = GameManager.selected.focus;
		int i = 0;
		foreach (var focus in cellFocus.Values) {
			TextMeshProUGUI focusName = focusUIElement[i].GetComponentInChildren<TextMeshProUGUI>();
			Toggle[] focusSlots = focusUIElement[i].GetComponentsInChildren<Toggle>();
			for (int j = 0; j < focus.Slots.Count; j++) {
				Toggle current = focusSlots[j];
				focusSlots[j].onValueChanged.RemoveAllListeners();
				focusSlots[j].isOn = focus.Slots[j];
				focusSlots[j].onValueChanged.AddListener(delegate { Toggle(current); });
			}
			string effect = "";
			if (focus.GetActiveCount > 0) {
				foreach (var e in focus.Effects) {
					effect += $"{focus.GetEffect(e.Key).FloorS(2)} {e.Key} ";
				}
			}
			//focusName.text = $"{focus.Name} {effect}";
			i++;
		}
		unitHeader.text = $"Remaining Focus Points: <align=right><style=Energy>{GameManager.selected.stats[Key.FocusPoint]}"; ;
	}

}


/*
public class FocusEntries: List<FocusEntry>
{
	public int maxFocus;
	public List<Toggle> activeFocus = new List<Toggle>();

	public FocusEntries() : base() { }
	public FocusEntries(int maxFocus) : base() {
		this.maxFocus = maxFocus;
	}

	public void AddGroup(List<Toggle> t, GameObject rootObject, TextMeshProUGUI rootObjectText, Vector3 bonus) {
		this.Add(new FocusEntry(t, rootObject, rootObjectText, bonus));
	}

	public void ToggleSlot(Toggle t, FocusHandler caller) {
		//string result = $"({GetAllActiveFocus}->";
		if (t.isOn) {
			activeFocus.Add(t);
			if (GetAllActiveFocus > maxFocus) {
				activeFocus[0].isOn = false;
			}
		}
		else {
			activeFocus.Remove(t);
		}
		caller.Refresh();

		//result += $"{GetAllActiveFocus})\n";
		//this.ForEach(entry => { entry.slots.ForEach(slot => result += slot.isOn ? "1 ": "0 "); result += "\t"; });
		//MonoBehaviour.print(result);
	}
	public int GetAllActiveFocus { get {
			return activeFocus.Count;
	} }

	public string GetFocusBonus() {
		string result = "";
		foreach (var group in this) {
			float percent = group.GetActiveSlot() / group.slots.Count;
			if (percent != 0) { result += $"+{percent.Round(2)} "; }

			string bonusText = "";
			foreach (var item in group.bonus) {
				if (percent * item.Value != 0) {
					bonusText += $" +{(percent * item.Value).Round(2)} {item.Key}";
				}
			}
			group.entryName.text = group.entryObject.name + bonusText;
		}
		return result;
	}
}

public class FocusEntry
{
	public GameObject entryObject;
	public TextMeshProUGUI entryName;
	public List<Toggle> slots;
	public Dictionary<string, float> bonus;

	public FocusEntry(List<Toggle> slots, GameObject entryObject, TextMeshProUGUI entryName, Vector3 bonus) {
		this.entryObject = entryObject;
		this.entryName = entryName;
		this.slots = slots;
		this.bonus = new Dictionary<string, float> {
			[CellData.Protein] = bonus.x,
			[CellData.Energy] = bonus.y,
			[CellData.Genetic] = bonus.z
		};
	}

	public float GetActiveSlot() {
		int active = 0;
		slots.ForEach(slot => active += (slot.isOn ? 1 : 0));
		return active;
	}
}*/