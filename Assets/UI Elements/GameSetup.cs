﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using TMPro;
using System;

public partial class GameSetup : MonoBehaviour
{
	public int Min_RADIUS;
	public int Min_WIDTH;
	public int Min_HEIGHT;

	public Button startGameButton;
	[Header("Map Shapes")]
	public Toggle[] mapShapes;
	public TMP_InputField widthConfig;
	public TMP_InputField heightConfig;
	public TMP_InputField radiusConfig;
	[Header("Game & Player")]
	public int MIN_CYCLE_LENGTH = 5;
	public int DEF_CYCLE_LENGTH = 10;
	public TMP_InputField gameLengthConfig;
	public TMP_Dropdown gameLengthEntry;
	public TMP_Dropdown playerColor;
	public Toggle tutorials;

	[Header("Player Color")]
	public Color32[] playerColorLibrary;

	GameObject widthConfigParent;
	GameObject heightConfigParent;
	GameObject radiusConfigParent;
	[SerializeField]
	MapManager.MapType mapType;
	[SerializeField]
	Cartesian mapDimensions;

	// Use this for initialization
	void Start() {
		if (!PlayerPrefs.HasKey("Enable Tutorial")) { PlayerPrefs.SetInt("Enable Tutorial", 1); }
		tutorials.isOn = PlayerPrefs.GetInt("Enable Tutorial") == 1 ? true : false;
		widthConfigParent = widthConfig.transform.parent.gameObject;
		heightConfigParent = heightConfig.transform.parent.gameObject;
		radiusConfigParent = radiusConfig.transform.parent.gameObject;

		widthConfig.text = Min_WIDTH.ToString();
		heightConfig.text = Min_HEIGHT.ToString();
		radiusConfig.text = Min_RADIUS.ToString();

		for (int i = 0; i < mapShapes.Length; i++) {
			mapShapes[i].onValueChanged.AddListener(OnToggleShape);
		}
		widthConfig.onEndEdit.AddListener(OnEditedDimesion);
		heightConfig.onEndEdit.AddListener(OnEditedDimesion);
		radiusConfig.onEndEdit.AddListener(OnEditedDimesion);
		gameLengthConfig.onEndEdit.AddListener(OnEditedGameLengthField);
		gameLengthConfig.onValueChanged.AddListener(OnChangeGameLengthField);
		gameLengthEntry.onValueChanged.AddListener(OnChangeGameLengthEntry);
		tutorials.onValueChanged.AddListener(EnableTutorials);

		playerColor.onValueChanged.AddListener(OnChangedPlayerColor);
		playerColor.GetComponent<Image>().color = playerColorLibrary[0];
		startGameButton.onClick.AddListener(OnConfirmSettings);

		OnToggleShape(true);
		OnEditedDimesion("");
		gameLengthConfig.text = DEF_CYCLE_LENGTH.ToString();

		SettingInitialization();
		GameManager._.tutorials.SetTutorial(tutorials.isOn);
	}

	// Update is called once per frame
	void Update() {

		if (playerColor.IsExpanded) {
			OnPlayerColorExpanded();
		}
	}

	public void EnableTutorials(bool value) {
		PlayerPrefs.SetInt("Enable Tutorial", value == true ? 1 : 0);
		GameManager._.tutorials.SetTutorial(value);
	}

	void OnToggleShape(bool isOn) {
		if (mapShapes[0].isOn) {
			widthConfigParent.SetActive(false);
			heightConfigParent.SetActive(false);
			radiusConfigParent.SetActive(true);
			mapType = MapManager.MapType.Hex;
			OnEditedDimesion("");
		}
		if (mapShapes[1].isOn || mapShapes[2].isOn) {
			widthConfigParent.SetActive(true);
			heightConfigParent.SetActive(true);
			radiusConfigParent.SetActive(false);
			mapType = mapShapes[1].isOn ? MapManager.MapType.Rect : MapManager.MapType.Diamond;
			OnEditedDimesion("");
			//mapDimensions = new Vector2(float.Parse(widthConfig.text), float.Parse(heightConfig.text));
		}
	}

	void OnEditedDimesion(string text) {
		if (mapType == MapManager.MapType.Hex) {
			if (radiusConfig.text.Length < 1 || int.Parse(radiusConfig.text) < Min_RADIUS) {
				radiusConfig.text = Min_RADIUS.ToString();
				return;
			}
			int radius = int.Parse(radiusConfig.text);
			mapDimensions = new Cartesian(radius, radius);
		}
		else {
			if (widthConfig.text.Length < 1 || int.Parse(widthConfig.text) < Min_WIDTH) {
				widthConfig.text = Min_WIDTH.ToString();
				return;
			}
			if (heightConfig.text.Length < 1 || int.Parse(heightConfig.text) < Min_HEIGHT) {
				heightConfig.text = Min_HEIGHT.ToString();
				return;
			}
			mapDimensions = new Cartesian(int.Parse(widthConfig.text), int.Parse(heightConfig.text));
		}
	}
	void OnPlayerColorExpanded() {
		Toggle[] playerColors = playerColor.GetComponentsInChildren<Toggle>();
		for (int i = 0; i < playerColors.Length; i++) {
			Image[] im = playerColors[i].GetComponentsInChildren<Image>();
			im[0].color = playerColorLibrary[i];
		}
		var dropdownRect = playerColor.GetComponentInChildren<ScrollRect>().GetComponent<RectTransform>();
		var entryRect = playerColors[0].GetComponent<RectTransform>();
		float height = 0;
		for (; height < Screen.height / 2; height += entryRect.sizeDelta.y) { }
		dropdownRect.sizeDelta = new Vector2(dropdownRect.sizeDelta.x, Mathf.Clamp(height, height, 160f));
	}

	void OnChangedPlayerColor(int option) {
		playerColor.GetComponent<Image>().color = playerColorLibrary[option];
	}

	void OnEditedGameLengthField(string text) {
		int integer = 0;
		int.TryParse(text, out integer);
		if (integer < MIN_CYCLE_LENGTH) {
			gameLengthConfig.text = "" + MIN_CYCLE_LENGTH;
			return;
		}
	}

	void OnChangeGameLengthField(string text) {
		//if (text.Length < 1) return;
		int length = 0;
		int.TryParse(text, out length);
		if (length >= MIN_CYCLE_LENGTH && length < 10) {
			gameLengthEntry.value = 0;
		}
		else if (length >= 10 && length < 15) {
			gameLengthEntry.value = 1;
		}
		else if (length >= 15) {
			gameLengthEntry.value = 2;
		}
	}

	void OnChangeGameLengthEntry(int option) {
		if (gameLengthEntry.IsExpanded) {
			switch (option) {
				case 0:
					gameLengthConfig.text = "5";
					break;
				case 1:
					gameLengthConfig.text = "10";
					break;
				case 2:
					gameLengthConfig.text = "15";
					break;
				default:
					break;
			}
		}
	}
	/*
	private void OnEnable() {
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	private void OnDisable() {
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	
	void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		if(scene.name == "default2") {
			SceneManager.SetActiveScene(scene);
			GameObject[] roots = SceneManager.GetActiveScene().GetRootGameObjects();
			print($"Find Roots Of new scene {roots.Length}");
			MapManager mapManager = null;
			GameManager gameManager = null;
			foreach (var obj in roots) {
				print(obj.name);
				if (obj.tag == "GameController") {
					mapManager = obj.GetComponent<MapManager>();
					mapManager.mapDimension = mapDimensions;
					mapManager.mapType = mapType;
				}
				if (obj.tag == "MainCamera") {
					gameManager = obj.GetComponent<GameManager>();
					gameManager.lengthOfCycle = float.Parse(gameLengthConfig.text);
					gameManager.playerColor = playerColorLibrary[playerColor.value];
				}
			}
			print("Assigned Values");
			if (mapManager && gameManager) {
				gameManager.LateStart();
				//mapManager.LateStart();
			}
		}
	}
	*/
	void OnConfirmSettings() {
		//GameObject.DontDestroyOnLoad(this.gameObject);
		//GameObject.Find("EventSystem").SetActive(false);
		//GameObject.Find("Main Camera").SetActive(false);
		//gameObject.GetComponent<Canvas>().enabled = false;
		//print("Hide Elements");

		//SceneManager.LoadSceneAsync("default2", LoadSceneMode.Additive);
		float gameLength = float.Parse(gameLengthConfig.text);
		GameManager._.StartGame(gameLength, playerColor.value, mapDimensions, mapType, playerColorLibrary);

	}

	public void OnExitButtonPressed() {
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#endif
		Application.Quit();
	}
}

public partial class GameSetup
{
	public AudioMixer audioMixer;
	public TMP_Dropdown resolutionDropdown;
	Resolution[] resolutions;

	void SettingInitialization() {
		resolutions = Screen.resolutions;
		resolutionDropdown.ClearOptions();

		int currentResolution = 0;
		List<string> res = new List<string>();
		for (int i = 0; i < resolutions.Length; i++) {

			if (resolutions[i].width < 800 || resolutions[i].width < 600) continue;
			if (Screen.currentResolution.width == resolutions[i].width &&
					Screen.currentResolution.height == resolutions[i].height)
				currentResolution = i;
			res.Add($"{resolutions[i].width} x {resolutions[i].height}");
		}
		res.Reverse();
		resolutionDropdown.AddOptions(res);
		resolutionDropdown.value = (resolutions.Length - 1) - currentResolution;
		resolutionDropdown.RefreshShownValue();
	}

	public void SetResolution(int resolutionIndex) {
		Resolution res = resolutions[(resolutions.Length - 1) - resolutionIndex];
		Screen.SetResolution(res.width, res.height, Screen.fullScreen);
	}

	public void SetMasterVolume(float master) {
		audioMixer.SetFloat("Master Volume", LinearToDecibel(master));
	}

	public void SetMusicVolume(float music) {
		audioMixer.SetFloat("BGM Volume", LinearToDecibel(music));
	}

	public void SetSFXVolume(float sfx) {
		audioMixer.SetFloat("SFX Volume", LinearToDecibel(sfx));
	}

	float LinearToDecibel(float linear) {
		return linear != 0 ? 20f * Mathf.Log10(linear) : -144.0f;
	}

	public void SetQuality(int qualityIndex) {
		QualitySettings.SetQualityLevel(qualityIndex);
	}
	public void SetFullscreen(int fullscreenIndex) {
		Screen.fullScreen = fullscreenIndex == 0 ? true : false;
	}
}