﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Cellular
{
	public class HexGrid : MonoBehaviour
	{
		public int width = 6;
		public int height = 6;

		public HexCell cellPrefab;
		public Text cellLabelPrefab;

		HexCell[] cells;
		HexMesh hexMesh;
		Canvas gridCanvas;

		private void Awake()
		{
			gridCanvas = GetComponentInChildren<Canvas>();
			hexMesh = GetComponentInChildren<HexMesh>();
			cells = new HexCell[height * width];
			for (int z = 0, i = 0; z < height; z++)
			{
				for (int x = 0; x < width; x++)
				{
					CreateCell(x, z, i++);
				}
			}
		}
		
		private void Start() {
			hexMesh.Triangulate(cells);
		}

		private void CreateCell(int x, int z, int v)
		{
			Vector3 position = new Vector3(
				(x + z * .5f) * (HexTemplate.innerRadius * 2f),
				0f,
				z * (HexTemplate.outerRadius * 1.5f));

			HexCell cell = cells[v] = Instantiate<HexCell>(cellPrefab);
			cell.transform.SetParent(transform, false);
			cell.transform.localPosition = position;

			Text label = Instantiate<Text>(cellLabelPrefab);
			label.rectTransform.SetParent(gridCanvas.transform, false);
			label.rectTransform.anchoredPosition = new Vector2(position.x, position.z);
			label.text = x.ToString() + "," + z.ToString();
		}
	}
}