﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class SerializableStats
{
	public Dictionary<string, float> Stats { get; set; }
	public SerializableStats(CellStats stats) { Stats = new Dictionary<string, float>(stats); }
	public CellStats Deserialized { get { return new CellStats(Stats); } }
}

[System.Serializable]
public class CellStats : Dictionary<string, float>
{
	public CellStats() : base() { }
	public CellStats(Dictionary<string, float> dictionary) : base(dictionary) { }
	public CellStats(CellStats stat) : base(stat) { }


	/// <summary>
	/// (Apply) Add flat bonus or Multiply percentage bonus
	/// </summary>
	public static CellStats operator +(CellStats original, Dictionary<string, float> adder) {
		CellStats result = new CellStats(original);
		foreach (var item in adder) {
			if (item.Key.ContainsAny(Key.Multiplier)) {
				if (original.ContainsKey(item.Key)) result[item.Key] = original[item.Key] * item.Value;
				else result[item.Key] = 1 * item.Value;
			}
			else {
				if (original.ContainsKey(item.Key)) result[item.Key] = original[item.Key] + item.Value;
				else result[item.Key] = item.Value;
			}
		}
		return result;
	}
	/// <summary>
	/// (Restore) Subtract flat bonus or Divide percentage bonus
	/// </summary>
	public static CellStats operator -(CellStats original, Dictionary<string, float> adder) {
		CellStats result = new CellStats(original);
		foreach (var item in adder) {
			if (item.Key.ContainsAny(Key.Multiplier)) {
				if (original.ContainsKey(item.Key)) result[item.Key] = original[item.Key] / item.Value;
				else result[item.Key] = 1 / item.Value;
			}
			else {
				if (original.ContainsKey(item.Key)) result[item.Key] = original[item.Key] - item.Value;
				else result[item.Key] = -item.Value;
			}
		}
		return result;
	}

	/// <summary>
	/// (Apply) Add flat bonus or Multiply percentage bonus
	/// </summary>
	/// <param name="adder">Collection of amount of changed stats</param>
	/// <param name="pruneUnchanged">Show only affected stats?</param>
	public CellStats Combine(Dictionary<string, float> adder, float multiplier = 1f, bool pruneUnchanged = false) {
		CellStats result = new CellStats();
		foreach (var item in adder) {
			if (ContainsKey(item.Key)) {
				if (item.Key.ContainsAny(Key.Multiplier)) this[item.Key] *= item.Value;
				else if (item.Key == Key.Health) this.HealthChange(item.Value * multiplier);
				else this[item.Key] += item.Value * multiplier;
			}
			else {
				this[item.Key] = item.Value;
			}
			result[item.Key] = this[item.Key];
		}
		return pruneUnchanged ? result : this;
	}

	/// <summary>
	/// (Restore) Subtract flat bonus or Divide percentage bonus
	/// </summary>
	/// <param name="adder">Collection of amount of changed stats</param>
	/// <param name="pruneUnchanged">Show only affected stats?</param>
	public CellStats Uncombine(Dictionary<string, float> adder, bool pruneUnchanged = false) {
		CellStats result = new CellStats();
		foreach (var item in adder) {
			if (ContainsKey(item.Key)) {
				if (item.Key.ContainsAny(Key.Multiplier)) this[item.Key] /= item.Value;
				else this[item.Key] -= item.Value;
			}
			else {
				if (item.Key.ContainsAny(Key.Multiplier)) this[item.Key] = 1 / item.Value;
				else this[item.Key] = -item.Value;
			}
			result[item.Key] = this[item.Key];
		}
		return pruneUnchanged ? result : this;
	}

	public override string ToString() {
		string s = "";
		foreach (var entry in this) {
			s += $"{entry.Value} {entry.Key}\n";
		}
		return s;
	}

	public string ToRichText(bool icon, bool multiline) {
		List<string> s = new List<string>();
		foreach (var entry in this) {
			string key = "";
			string _color = "", color_ = "</color>";
			if (icon) {
				// convert key to corresponding icon
			}
			else {
				key = entry.Key;
			}
			_color = "";
			s.Add($"{_color}{entry.Value} {key}{color_}");
		}
		return string.Join($"{(multiline ? "\n" : " ")}", s);
	}

	public bool IsSufficientForAll(string uid) {
		var upgrade = UpgradeReference._this[uid];
		if (!IsSufficient(Key.Space, upgrade.Space)) return false;
		foreach (var cost in upgrade.Costs) {
			if (!IsSufficient(cost.Key, cost.Value)) return false;
		}
		return true;
	}

	public bool IsSufficient(string stat_key, float cost) {
		if (stat_key == Key.Energy) return true;
		float current_value;
		TryGetValue(stat_key, out current_value);
		if (current_value > 0) {
			if (stat_key == Key.Space) { return current_value + cost <= this[Key.Size]; }
			return (current_value >= cost);
		}
		return false;
	}

	public bool Deduct(string key, float amount) {
		if (IsSufficient(key, amount)) {
			if (key == Key.Space) this[key] += amount;
			else this[key] -= amount;
			return true;
		}
		MonoBehaviour.print($"Stats[{key}] not enough.");
		return false;
	}

	public void Increment(string key, float amount) {
		if (ContainsKey(key)) {
			if (key == Key.Space) this[key] -= amount;
			else this[key] += amount;
		}
		else {
			MonoBehaviour.print($"Stats[{key}] not exist.");
		}
	}

	public void HealthChange(float amount) {
		this[Key.Health] = Mathf.Clamp(this[Key.Health] + amount, 0, this[Key.MaxHealth]);
	}
}

