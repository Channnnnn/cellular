﻿using System.Collections.Generic;
using System.Linq;
using NamedFloat = System.Collections.Generic.Dictionary<string, float>;

[System.Serializable]
public class CellFocus : SortedDictionary<string, Focus>
{
	//public int Point;
	public List<KeyValuePair<string, int>> activeFocus = new List<KeyValuePair<string, int>>();
	public NamedFloat currentEffect = new NamedFloat();

	public CellFocus() { }
	public CellFocus(CellFocus original) {
		foreach (var item in original) {
			Add(item.Key, new Focus(item.Value));
		}
		activeFocus = new List<KeyValuePair<string, int>>();
		foreach (var item in original.activeFocus) {
			activeFocus.Add(new KeyValuePair<string, int>(item.Key, item.Value));
		}
		UpdateAllCurrentEffects();
	}

	public float ActivePoint {
		get {
			return activeFocus.Count;
		}
	}

	public void Toggle(string parent_key, int self_index, bool isOn, CellData cell) {
		string result = $"t{parent_key}{self_index}";

		if (isOn) {
			activeFocus.Add(new KeyValuePair<string, int>(parent_key, self_index));
			this[parent_key].Slots[self_index] = true;

			if (ActivePoint > cell.stats[Key.FocusPoint]) {
				this[activeFocus[0].Key].Slots[activeFocus[0].Value] = false;
				activeFocus.RemoveAt(0);
			}
		}
		else {
			int index = activeFocus.FindIndex(f => f.Key == parent_key && f.Value == self_index);
			activeFocus.RemoveAt(index);
			this[parent_key].Slots[self_index] = false;
			this.ToList().ForEach(f => { f.Value.Slots.ForEach(slot => result += slot ? "1 " : "0 "); result += "\t"; });
			//MonoBehaviour.print(result);
		}
		UpdateAllCurrentEffects();
	}

	/// <summary>
	/// Return combined attribute bonus from every active focus
	/// </summary>
	/// <param name="stat_key">Attribute name</param>
	/// <returns></returns>
	public float GetComputedEffect(string stat_key) {
		bool multiplier_key = stat_key.ContainsAny(Key.Multiplier);
		float totalEffect = 0f;
		if (multiplier_key) totalEffect = 1f;
		HashSet<string> _activeFocus = new HashSet<string>(activeFocus.Select(a => a.Key));
		foreach (var active in _activeFocus) {
			if (multiplier_key) totalEffect *= this[active].GetEffect(stat_key);
			else totalEffect += this[active].GetEffect(stat_key);
		}
		return totalEffect;
	}

	public void UpdateAllCurrentEffects() {
		currentEffect.Clear();
		HashSet<string> unique_keys = new HashSet<string>(activeFocus.Select(f => f.Key));
		foreach (string key in unique_keys) {
			NamedFloat currentComputedEffect = this[key].GetComputedEffects();
			foreach (var attr in currentComputedEffect) {
				currentEffect.AdditiveInsert(attr.Key, attr.Value);
			}
		}
	}
}

[System.Serializable]
public class Focus
{
	public int Slot { get; private set; } = 1;
	public string Name { get; set; }
	public List<bool> Slots { get; set; }
	public NamedFloat Effects { get; set; }

	public Focus() { }
	public Focus(string _name, int slot_count, NamedFloat _effects) {
		Name = _name;
		Slots = new List<bool>(new bool[slot_count]);
		Effects = _effects;
	}
	public Focus(Focus focus) {
		Name = focus.Name;
		Slot = focus.Slot;
		Slots = new List<bool>(new bool[focus.Slot]);
		Effects = focus.Effects;
	}

	public float GetActiveCount {
		get {
			float count = 0;
			foreach (var slot in Slots) { if (slot) count++; }
			return count;
		}
	}
	public float GetMultiplier { get { return GetActiveCount / Slots.Count; } }
	public float GetMultiplierStep { get { return 1 / Slots.Count; } }

	public float GetEffect(string key) {
		if (Effects.ContainsKey(key)) {
			return Effects.Getf(key) * GetMultiplier;
		}
		else return Effects.Getf(key);
	}

	/// <summary>
	/// Return Effects of a focus multiplied by its active percentage OR per slot
	/// </summary>
	/// <param name="perSlot">Get effects per slot only</param>
	/// <returns></returns>
	public NamedFloat GetComputedEffects(bool perSlot = false) {
		float multiplier = perSlot ? GetMultiplierStep : GetMultiplier;
		NamedFloat result = new NamedFloat();
		foreach (var item in Effects) {
			result[item.Key] = item.Value * multiplier;
		}
		return result;
	}

	public string GetEffectString() {
		List<string> result = new List<string>();
		string red = "<#E42>", green = "<#BF2>";
		string color = "";
		foreach (var e in Effects) {
			if (e.Key.ContainsAny(Key.Multiplier)) {
				if (e.Key.ContainsAny(Key.Inverts)) {
					color = e.Value > 1f ? red : e.Value < 1f ? green : "";
					result.Add($"{color}{e.Value.ToPercentfS(2)} {e.Key}</color>");
				}
				else {
					color = e.Value > 1f ? green : e.Value < 1f ? red : "";
					result.Add($"{color}{e.Value.ToPercentfS(2)} {e.Key}</color>");
				}
			}
			else {
				if (e.Key.ContainsAny(Key.Inverts)) {
					color = e.Value > 0f ? red : e.Value < 1f ? green : "";
					result.Add($"{color}{e.Value.FloorfS(2)} {e.Key}</color>");
				}
				else {
					color = e.Value > 0f ? green : e.Value < 1f ? red : "";
					result.Add($"{color}{e.Value.FloorfS(2)} {e.Key}</color>");
				}
			}
		}
		return string.Join("\n", result);
	}
}
