﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using Reproduce = System.Tuple<CellData, SpawnType>;

public class CellInteraction : MonoBehaviour
{
	public CellData cell;
	CameraControl cameraControl;
	//GameManager gameManager;
	[Header("Upgrade Handler")]
	public UpgradeHandler upgradeHandler;
	public FocusHandler focusHandler;

	public SpriteRenderer cellBase;
	//public SpriteRenderer cellBorder;
	public SpriteRenderer cellOverlay;
	public LineRenderer highlight;

	public UnityEngine.UI.Image baseRing;
	public RingIndicator outerRing;
	public RingIndicator innerRing;
	public RingIndicator bodyFillIndicator;
	public UnityEngine.UI.Image bodyFill;

	public bool pendingFocus;
  public bool IsDyingSequenceCompleted;

	public string overriden = "";
	bool Overridden { get { return overriden != ""; } }
	bool Selected { get { return cell == GameManager.selected; } }
	bool Hovering { get { return gameObject == GameManager._.pointerData.pointerEnter; } }

	void Start() {
		cell = gameObject.GetComponent<CellData>();
		cameraControl = GameManager._.cameraController;
		upgradeHandler = GameManager._.upgradeHandler;
		focusHandler = GameManager._.focusHandler;
		//if (!cellBase) cellBase = GetComponent<SpriteRenderer>();
		//if (!cellBorder) cellBorder = GetComponentInChildren<SpriteRenderer>();
		if (!highlight) highlight = GetComponent<LineRenderer>();
		highlight.enabled = true;
		ApplyHighlightColor("Default");
		highlight.widthMultiplier = 0.05f;
		ApplyCellBodyColor(cell.owner.COLOR);

		RingAppearance();
	}

	public void RingAppearance() {
		if (cell.HasCell && !cellBase.gameObject.activeSelf) {
			cellBase.gameObject.SetActive(true);
			outerRing.radialSprite.color = outerRing.radialSprite.color.Alter(0, 0, 0, 255f);
			innerRing.radialSprite.color = innerRing.radialSprite.color.Alter(0, 0, 0, 255f);
		}
		if (!cell.HasCell && cellBase.gameObject.activeSelf) cellBase.gameObject.SetActive(false);
		if (!cell.HasCell) return;

		outerRing.Display(cell.HEALTH.Map01(0f, cell.MAXHEALTH));
		innerRing.Display((cell.LIFESPAN - cell.Age).Map01(0, cell.LIFESPAN));
		//innerRing.Display(cell.stats[Key.SizeGrowthProgress].Map01(cell.stats[Key.PrevSizeGrowthCost], cell.stats[Key.SizeGrowthCost]));
		if (cell.Queue) {
			if (cell.QueueHead.Contains("a0")) bodyFillIndicator.Display(cell.QueueProgress / cell.stats[Key.Size]);
			else if (cell.QueueHead.Contains("a1")) bodyFillIndicator.Display(cell.QueueProgress / cell.REPRODUCTION_ENERGY_COST);
			else bodyFillIndicator.Display(cell.UPGRADE_ENERGY_PROGRESS(cell.QueueHead));
		}
		if (!cell.Queue) bodyFillIndicator.Display(0f);
	}

	public void SizeAppearance() {
		if (cell.HasCell) {
			float currentProgress = cell.stats[Key.SizeGrowthProgress] - cell.stats[Key.PrevSizeGrowthCost];
			float currentGoal = cell.stats[Key.SizeGrowthCost] - cell.stats[Key.PrevSizeGrowthCost];
			float sizeParameter = cell.stats[Key.Size] + (currentProgress / currentGoal);
			float scale = sizeParameter.Map(1, 10, 0.5f, 1);
			cellBase.transform.localScale = new Vector3(scale, scale, scale);
		}
		//cellBorder.transform.localScale = new Vector3(scale, scale, 1f);
	}

	private void Update() {

		if (cellDeathSequenceActive) {
			cell.adjacents.ForEach(adj => cell.interaction.overriden = "");
			CellDeathSequence();
			return;
		}

		RingAppearance();
		SizeAppearance();

		if (Selected) {
			if (GameManager.__CheatSPAWNCELL || GameManager._ReproductionBinary || GameManager._ReproductionBudding || GameManager._MovingCell) {
				cell.adjacents.GetEmpty.ForEach(_cell => {
					if (_cell.interaction.overriden == "") {
						_cell.interaction.overriden = "ADJACENT";
					}
				});
			}
			if (!(GameManager.__CheatSPAWNCELL || GameManager._ReproductionBinary || GameManager._ReproductionBudding || GameManager._MovingCell)) {
				cell.adjacents.ForEach(_cell => {
					if (_cell.interaction.overriden == "ADJACENT") {
						_cell.interaction.overriden = "";
					}
					
				});
			}
		}

		if (!Overridden && !Hovering && !Selected) {
			ApplyHighlightColor("Default");
		}
		if (Overridden) {
			if (overriden == "ADJACENT") {
				//var foodRating = cell.SizeGrowthRate.Map(0.5f, 3f, 0f, 255f);
				//var tileDamage = cell.TOTAL_DAMAGE_BY_TILE.Map(0f, 5f, 0f, 255f);
				//byte red = (byte)(tileDamage/foodRating * 255);
				//byte green = (byte)(foodRating /tileDamage * 255);
				//byte blue = (byte)Mathf.Clamp(255 - ((foodRating - tileDamage)).Abs(), 0, 128);
				//ApplyHighlightColor(_Color.Gradient(new Color32(red, green, blue, 255)));
				ApplyHighlightColor(_Color.Gradient(new Color32(118, 255, 39, 255)));
				HoveringHighlightRenderOrder();
			}
			else if (overriden == "COVET") {
				ApplyHighlightColor(_Color.Gradient(new Color32(222, 180, 39, 255)));
				HoveringHighlightRenderOrder();
			}
		}

		if (pendingFocus) {
			GameManager._.SetSelectObject(this.cell);
			pendingFocus = false;
		}

		cellOverlay.enabled = Global.IsShowingOverlay;
		if (Global.CurrentOverlay == 0) {
			float foodRating = cell.tile[Key.Food].Map01(0f, 20f);
			cellOverlay.color = GameManager._.foodOverlayColor.Evaluate(foodRating);
		}
		else if  (Global.CurrentOverlay == 1) {
			float temperature = cell.TemperatureValue.Map01(-20, 100);
			cellOverlay.color = GameManager._.temperatureOverlayColor.Evaluate(temperature);
		}
		else if (Global.CurrentOverlay == 2) {
			float moistureRating = cell.tile[Key.Moisture].Map01(6f, 20f);
			cellOverlay.color = GameManager._.moistureOverlayColor.Evaluate(moistureRating);
		}
		else if (Global.CurrentOverlay == 3) {
			float oxygenRating = cell.tile[Key.Oxygen].Map01(0f, 21f);
			cellOverlay.color = GameManager._.oxygenOverlayColor.Evaluate(oxygenRating);
		}
	}

	private void OnMouseEnter() {
		ApplyHighlightColor("Hover");
		highlight.widthMultiplier = 0.1f;
		if (cell.HasCell && !Selected && !cellDeathSequenceActive) {
			ApplyCellBodyColor(cell.owner.COLOR, 20f);
		}
	}

	private void OnMouseDown() {
		ApplyHighlightColor("Click");
	}

	private void OnMouseUpAsButton() {
		/*RESET COLOR TO HOVER STATE*/
		ApplyHighlightColor("Hover");
		ApplyCellBodyColor(cell.owner.COLOR);

		/*IF CLICKED CELL, USE SELECTED STATE*/
		if (!cameraControl.dragged && !cameraControl._UIBlock) {

			/*USE CHEAT ADD OR REMOVE CELL*/
			if (GameManager.__CheatSPAWNCELL) {
				if (cell.HasCell) {
					cell.owner.RemovePlayerCell(cell);
					cell.SpawnNewCell(Player.NoneType, SpawnType.Empty);
					ApplyCellBodyColor(Color.clear);
					return;
				}
				else {
					cell.SpawnNewCell(GameManager._.GetPlayer, SpawnType.Primitive);
					ApplyCellBodyColor(cell.owner.COLOR);
					return;
				}
			}

			/*REPRODUCTION - ONLY EMPTY TILES - NOT SELF OR OTHER'S*/
			if (GameManager._ReproductionBinary || GameManager._ReproductionBudding) {
				if (Selected || cell.HasCell || GameManager.selected.ReproduceTarget.Any(c => c.Item1 == cell)) return;

				bool valid = GameManager.selected.adjacents.Contains(cell.coordination);
				if (valid) {
					SpawnType spawnType = GameManager._ReproductionBudding ? SpawnType.Budding : SpawnType.Binary;

					GameManager.selected.Action_Reproduction(new Reproduce(this.cell, spawnType));
					upgradeHandler.IssueAction("a1", cell);

					GameManager._.ExitReproductionMode();
					return;
				}
				
				GameManager._.ExitReproductionMode();
				return;
			}

			if (GameManager._MovingCell) {
				if (Selected || cell.HasCell) return;

				bool valid = GameManager.selected ? GameManager.selected.adjacents.Contains(cell.coordination) : false;
				GameManager._MovingCell = false;
				if (valid) {
					GameManager.selected.Action_Move(cell);
					upgradeHandler.IssueAction("a0", cell);

					GameManager._.ExitMoveCellMode();
					return;
				}
				GameManager._.ExitMoveCellMode();
				return;
			}

			if (cell.HasCell && cell.owner == GameManager._.GetPlayer) {
				ApplyHighlightColor("Selected");
				GameManager._.SetSelectObject(cell);
				upgradeHandler.ShowDataOfSelectedCell(cell);
				focusHandler.Refresh(cell);
			}
		}
	}

	private void OnMouseOver() {
		RaycastResult UItest = GameManager._.pointerData.pointerCurrentRaycast;
		if (UItest.isValid && UItest.gameObject.tag == "UI") {
			ApplyHighlightColor("Default");
		}
		if (cameraControl.dragged) {
			ApplyHighlightColor("Hover");
		}
		if (!Input.GetMouseButton(0) && !cell.HasCell) {
			ApplyHighlightColor("Hover");
		}
	}

	private void OnMouseExit() {
		if (Selected) {	ApplyHighlightColor("Selected"); return; }

		ApplyHighlightColor("Default");
		highlight.widthMultiplier = 0.05f;
		if (cell.HasCell && !cellDeathSequenceActive) { ApplyCellBodyColor(cell.owner.COLOR); }
		GameManager._.forceHideTooltip_OnMouseExitFlag = true;
	}

	public void OnSelectCell() {
		//celldata.progressUpdate.ForEach(x => print(x));
		upgradeHandler.ShowDataOfSelectedCell(cell);
	}	

	public void Unselected() {
		ApplyHighlightColor("Default");
		highlight.widthMultiplier = 0.05f;
		cell.adjacents.ForEach(_cell => {
			if (_cell.interaction.overriden == "ADJACENT") {
				_cell.interaction.overriden = "";
			}
		});
	}

	public void ApplyHighlightColor(Gradient borderColor) {
		highlight.colorGradient = borderColor;
	}

	void ApplyHighlightColor(string colorKey) {
		switch (colorKey) {
			case "Default":
				ApplyHighlightColor(_Color.Gradient(GameManager._.gridColor.Evaluate(GameManager.SubcycleFraction)));
				DefaultHighlightRenderOrder();
				break;
			case "Hover":
				ApplyHighlightColor(_Color.Gradient(GameManager._.hoverColor));
				HoveringHighlightRenderOrder();
				break;
			case "Hide":
				ApplyHighlightColor(_Color.HideGradient); break;
			case "Selected":
				ApplyHighlightColor(_Color.Gradient(GameManager._.selectedColor));
				SelectedHighlightRenderOrder();
				break;
			case "Click":
				ApplyHighlightColor(_Color.Gradient(GameManager._.clickColor)); break;
			default:
				break;
		}
	}

	void DefaultHighlightRenderOrder() {
		highlight.sortingOrder = 1;
	}

	void HoveringHighlightRenderOrder() {
		highlight.sortingOrder = 6;
	}

	void SelectedHighlightRenderOrder() {
		highlight.sortingOrder = 10;
	}

	public void ApplyCellBodyColor(Color baseColor, float deltaBrightness = 0) {
		cellBase.color = baseColor.Alter(0, 0, deltaBrightness);
		bodyFill.color = baseColor.Alter(0, -40, deltaBrightness + 30);
    float baseRingAlpha = baseColor == Color.clear ? 0f : 255f;
    baseRing.color = baseRing.color.Alter(0, 0, 0, baseRingAlpha);
	}
	
	public bool cellDeathSequenceActive = false;
	public void CellDeathSequence() {
		if (cellDeathSequenceActive && GameManager._.PlaySpeed != 0) {
			float FadeRate = -255f * Time.deltaTime;
			baseRing.color = baseRing.color.Alter(0, 0, 0, FadeRate);
			innerRing.radialSprite.color = innerRing.radialSprite.color.Alter(0, 0, 0, FadeRate);
			outerRing.radialSprite.color = outerRing.radialSprite.color.Alter(0, 0, 0, FadeRate);
			bodyFillIndicator.radialSprite.color = bodyFillIndicator.radialSprite.color.Alter(0, 0, 0, FadeRate);
			cellBase.color = cellBase.color.Alter(0, 0, 0, FadeRate);
			bodyFill.color = bodyFill.color.Alter(0, 0, 0, FadeRate);
		}
		if (cellBase.color.a <= 0) {
			IsDyingSequenceCompleted = true;
			cellDeathSequenceActive = false;
		}
	}
}

public static class _Color
{
	public static string ToHex(this Color32 _color) {
		Color32 color = new Color(_color.r, _color.g, _color.b, _color.a);
		//Debug.Log(color.ToString());
		return "#" + ((int)color.r).ToString("X2") + ((int)color.g).ToString("X2") + ((int)color.b).ToString("X2");
	}
	public static Color Alter(this Color c, float H, float S = 0, float V = 0, float A = 0) {
		float cH, cS, cV;
		Color.RGBToHSV(c, out cH, out cS, out cV);
		Color newC = Color.HSVToRGB((cH + H / 255).Clamp(0,1), (cS + S / 255).Clamp(0,1), (cV + V / 255).Clamp(0,1));
		newC.a = (c.a + A / 255).Clamp(0,1);
		return newC;
	}

	public static Gradient Gradient(Color targetColor, float alpha = 1f) {
		Gradient g = new Gradient();
		GradientAlphaKey a1 = new GradientAlphaKey(alpha, 0f);
		GradientColorKey c1 = new GradientColorKey(targetColor, 0f);
		GradientAlphaKey a2 = new GradientAlphaKey(alpha, 1f);
		GradientColorKey c2 = new GradientColorKey(targetColor, 1f);

		GradientAlphaKey[] a = { a1, a2 };
		GradientColorKey[] c = { c1, c2 };
		g.SetKeys(c, a);
		return g;
	}
	public static Gradient HideGradient { get { return Gradient(Color.clear, 0f); } }
}