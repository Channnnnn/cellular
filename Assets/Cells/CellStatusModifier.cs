﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Timer = System.Tuple<float, float>;

[System.Serializable]
public class SerializableStatusModifier
{
	public HashSet<string> StatusModifier { get; set; }
	public Dictionary<string, Timer> TemporaryStatus { get; set; }
	public SerializableStatusModifier(StatusModifier sm) {
		StatusModifier = sm;
		TemporaryStatus = sm.TemporaryStatus;
	}

	public StatusModifier Deserialize {
		get {
			return new StatusModifier(this);
		}
	}
}

public static class StatusModifierExtended
{
	public static void AddStatus(this CellData self, string id) {
		self.statusModifier.Add(id);

		Modifier modifier = ModifierReference.This[id];
		if (modifier.IsTemporary) {
			self.statusModifier.TemporaryStatus[id] = new Timer(GameManager.CycleCount, GameManager.CycleCount + modifier.Duration);
		}
		if (modifier.Apply == Modifier.APPLY.Once) {
			modifier.ApplyEffectsOnce(self);
		}

		if (modifier.Type == Modifier.TYPE.Affector) {
			Debug.Log(modifier);

			List<CellData> affectorAdjacents = null;
			foreach (var mod in modifier.Secondaries) {
				var Mod = ModifierReference.This[mod.Name];
				if (Mod.Type == Modifier.TYPE.Buff) { affectorAdjacents = CellAdjacents.GetFriendlyOf(self); }
				else if (Mod.Type == Modifier.TYPE.Debuff) { affectorAdjacents = CellAdjacents.GetHostileOf(self); }
				if (affectorAdjacents != null) {
					affectorAdjacents.ForEach(cell => {
						cell.statusModifier.Add(Mod.Name);
					});
				}
			}
		}
		//Signal Change
	}

	public static void RemoveStatus(this CellData self, string id) {
		Modifier modifier = ModifierReference.This[id];
		if (self.statusModifier.TemporaryStatus.ContainsKey(id)) self.statusModifier.TemporaryStatus.Remove(id);
		if (modifier.Apply == Modifier.APPLY.Once) {
			modifier.RemoveEffectsOnce(self);
		}
		self.statusModifier.Remove(id);
		//Signal change
	}

	public static void StatusDecay(this CellData self) {
		List<string> expired = new List<string>();
		foreach (var S in self.statusModifier.TemporaryStatus) {
			if (GameManager.CycleCount >= S.Value.Item2) {
				expired.Add(S.Key);
			}
		}
		expired.ForEach(self.RemoveStatus);
	}

	public static void ApplyEffects(this CellData self) {
		foreach (var id in self.statusModifier) {
			Modifier modifier = ModifierReference.This[id];
			if (modifier.Type != Modifier.TYPE.Label) {
				modifier.ApplyEffects(self);
				if (modifier.Type == Modifier.TYPE.Condition) {
					//Check remove condition;
				}
			}
		}
	}
}

[System.Serializable]
public class StatusModifier : HashSet<string>
{
	public Dictionary<string, Timer> TemporaryStatus = new Dictionary<string, Timer>();

	public StatusModifier() { }
	public StatusModifier(StatusModifier original) : base(original) { }
	public StatusModifier(SerializableStatusModifier sm) {
		new HashSet<string>(sm.StatusModifier);
		TemporaryStatus = new Dictionary<string, Timer>(sm.TemporaryStatus);
	}

	public override string ToString() {
		//List<Modifier> result =	new List<Modifier>();
		string red = "<#E42>", green = "<#BF2>", gold = "<#FC5>", grey="<#DCB>";
		string color = "";
		List<string> result = new List<string>();
		if (this.Count > 0) {
			foreach (var name in this) {
				var mod = ModifierReference.This[name];
				if (mod.Type == Modifier.TYPE.Buff) color = green;
				else if (mod.Type == Modifier.TYPE.Debuff) color = red;
				else if (mod.Type == Modifier.TYPE.Affector) color = gold;
				else if (mod.Type == Modifier.TYPE.Label) color = grey;

				result.Add($"{color}{mod.Name}</color> " + $"{mod.Detail} " + 
					(mod.IsTemporary ? $"(for {(TemporaryStatus[name].Item2 - GameManager.CycleCount).Floor(1)} cycles)" : "")
					);
				//result.Add(ModifierReference.This[name]);
			}
		}
		return string.Join("\n", result);
	}
}
/*
[System.Serializable]
public class SerializableModifier
{
    public string Detail { get; set; }
	public string Type { get; set; }
	public float Duration { get; set; }
	public string Apply { get; set; }
	public string Name { get; set; }
	public SerializableStats Effects { get; set; }
	public List<string> SecondaryModifiers { get; set; }
	public SerializableModifier(Modifier modifier) {
		Type = modifier.Type.ToString();
		Duration = modifier.Duration;
		Apply = modifier.Apply.ToString();
		Name = modifier.Name;
		Effects = new SerializableStats(modifier.Effects);
        SecondaryModifiers = modifier.SecondaryModifiers;
	}

	public Modifier Deserialized { get { return new Modifier(this); } }
}
*/
public class Modifier
{
	public enum APPLY { Once, Continuous, Subcycle, Cycle }
	public enum TYPE { Label, Affector, Buff, Debuff, Condition }
	public string Detail { get; set; }
	public TYPE Type { get; set; }
	public float Duration { get; set; } = float.PositiveInfinity;
	public APPLY Apply { get; set; }
	public string Name { get; set; }
	public CellStats Effects { get; set; } = new CellStats();
	public List<Modifier> Secondaries { get; set; } = new List<Modifier>();

	public Modifier() { }
	/*public Modifier(SerializableModifier modifier) {
	Type = (TYPE) System.Enum.Parse(typeof(TYPE), modifier.Type);
	Duration = modifier.Duration;
	Apply = (APPLY) System.Enum.Parse(typeof(APPLY), modifier.Apply);
	Name = modifier.Name;
	Effects = modifier.Effects.Deserialized;
			SecondaryModifiers = modifier.SecondaryModifiers;
}*/
	//public List<Modifier> GetSecondaryModifiers { get { return SecondaryModifiers.Select(key => ModifierReference.This[key]).ToList(); } }
	public bool IsTemporary { get { return !float.IsInfinity(Duration); } }

	public void ApplyEffectsOnce(CellData cell) {
		if (Apply == APPLY.Once) {
			cell.stats.Combine(Effects);
		}
	}

	public void RemoveEffectsOnce(CellData cell) {
		if (Apply == APPLY.Once) {
			cell.stats.Uncombine(Effects);
		}
	}

	public void ApplyEffects(CellData cell) {
		if (Apply == APPLY.Continuous) {
			cell.stats.Combine(Effects, Time.deltaTime);
			//Debug.Log($"{cell.coordination} applied {Effects}");
		}
		else if (Apply == APPLY.Subcycle && GameManager.__SubcycleTick) {
			cell.stats.Combine(Effects);
			//Debug.Log($"{cell.coordination} applied {Effects}");
		}
		else if (Apply == APPLY.Cycle && GameManager.__CycleTick) {
			cell.stats.Combine(Effects);
			//Debug.Log($"{cell.coordination} applied {Effects}");
		}
	}

	public override string ToString() {
		string result = $"\n{Name}: {Type.ToString()}";
		if (Type != TYPE.Label) result += $"\n{Apply.ToString()} for {Duration}";
		if (Effects.Count != 0) result += $"\n{Effects}";
		if (Secondaries.Count != 0) result += $"\n{string.Join("\t\n", Secondaries.Select(s => s.Name))}";
		return result;
	}
}
