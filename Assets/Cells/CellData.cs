﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using NamedFloat = System.Collections.Generic.Dictionary<string, float>;
using Reproduce = System.Tuple<CellData, SpawnType>;

[System.Serializable]
public class SerializableCell
{
	public Cartesian Parent { get; set; }
	public Cartesian Coordination { get; set; }
	public float Age { get; set; }
	public SerializableStats Stats { get; set; }
	public SerializableStats Tile { get; set; }
	public CellFocus Focus { get; set; }
	public SerializableStatusModifier Modifier { get; set; }
	//StatusModifier
	public HashSet<string> Traits { get; set; }
	public HashSet<string> AvailableTraits { get; set; }
	public HashSet<string> DisabledTraits { get; set; }

	public HashSet<string> Upgrades { get; set; }
	public HashSet<string> AvailableUpgrades { get; set; }
	public HashSet<string> DisabledUpgrades { get; set; }

	public SerializableCell(CellData cell) {
		if (cell.parent != null) Parent = cell.parent.coordination;
		else Parent = Cartesian.Null;
		Coordination = cell.coordination;
		Age = cell.Age;
		Stats = new SerializableStats(cell.stats);
		Tile = new SerializableStats(cell.tile);
		Focus = cell.focus;
		Modifier = new SerializableStatusModifier(cell.statusModifier);
		Traits = cell.traits;
		AvailableTraits = cell.availableTraits;
		DisabledTraits = cell.disabledTraits;
		Upgrades = cell.upgrades;
		AvailableUpgrades = cell.availableUpgrades;
		DisabledUpgrades = cell.disabledUpgrades;
	}
	
	public void Deserialize(ref CellData cell) {
		if (Parent != Cartesian.Null) cell.parent = MapManager.GetCell(Parent);
		else cell.parent = null;
		cell.coordination = Coordination;
		cell.Age = Age;
		cell.stats = Stats.Deserialized;
		cell.tile = Tile.Deserialized;
		cell.focus = Focus;
		cell.statusModifier = Modifier.Deserialize;
		cell.traits = Traits;
		cell.availableTraits = AvailableTraits;
		cell.disabledTraits = DisabledTraits;
		cell.upgrades = Upgrades;
		cell.availableUpgrades = AvailableUpgrades;
		cell.disabledUpgrades = DisabledUpgrades;
	}

	public void DeserializeOnlyCell(ref CellData cell) {
		if (Parent != Cartesian.Null) cell.parent = MapManager.GetCell(Parent);
		else cell.parent = null;
		cell.Age = Age;
		cell.stats = Stats.Deserialized;
		cell.focus = Focus;
		cell.statusModifier = Modifier.Deserialize;
		cell.traits = Traits;
		cell.availableTraits = AvailableTraits;
		cell.disabledTraits = DisabledTraits;
		cell.upgrades = Upgrades;
		cell.availableUpgrades = AvailableUpgrades;
		cell.disabledUpgrades = DisabledUpgrades;
	}
}

[System.Serializable]
public partial class CellData : MonoBehaviour
{
	public CellAdjacents adjacents;
	public CellInteraction interaction;
	public Cartesian coordination;

	[System.NonSerialized]
	public Player owner;
	public SerializablePlayer _owner;

	public CellData parent;
	public float Age = 0f;
	public CellStats stats = new CellStats();
	public CellStats tile = new CellStats();
	public CellFocus focus = new CellFocus();
	public NamedFloat modifiers = new NamedFloat();
	public StatusModifier statusModifier;

	public HashSet<string> traits = new HashSet<string>();
	public HashSet<string> availableTraits = new HashSet<string>();
	public HashSet<string> disabledTraits = new HashSet<string>();

	public HashSet<string> upgrades = new HashSet<string>();
	public HashSet<string> availableUpgrades = new HashSet<string>();
	public HashSet<string> disabledUpgrades = new HashSet<string>();

	public NamedFloat progressOfUpgrades = new NamedFloat();
	public List<string> queueOfUpgrade = new List<string>();

	public bool Selected { get { return GameManager.selected == this; } }
	public bool HasCell { get { return owner.TYPE != Player.Type.None; } }
	public CellData MoveTarget = null;
	public List<Reproduce> ReproduceTarget = new List<Reproduce>();

	// Use this for initialization
	void Start() {		
		AddTrait("t0");
		AddAndApplyUpgrade("u0");
	}

	void Update() {
		if (_owner == null) _owner = new SerializablePlayer(owner);
		if (HasCell) {
			//print($"{owner.NAME} Size:{stats[Size]} Rate:{SizeGrowthRate} Cost:{SizeGrowthCost} Growth:{SizeGrowthProgress}");
			CellAging();
			CellSizeGrowth();
			CellHealthCheck();
			this.StatusDecay();
			this.ApplyEffects();

			if (Queue) {
				string id = QueueHead;
				if (Selected) GameManager._.upgradeHandler.UpdateProgress(id);

				QueueProgress += ENERGY_OUTPUT * GameManager.DeltaCycle;

				if (id.Contains("a0")) {
					if (!MoveTarget) MoveTarget = MapManager.GetCell(Cartesian.Parse(id.Split(' ')[1]));
					if (MoveTarget.HasCell || QueueProgress >= stats[Key.Size]) MoveTo(MoveTarget);
				}
				else if (id.Contains("a1")) {
					if (ReproduceTarget[0].Item1.HasCell || QueueProgress >= REPRODUCTION_ENERGY_COST) ReproduceTo(ReproduceTarget[0]);
				}
				if (id[0] == 'u' && QueueProgress >= UPGRADE_ENERGY_COST(id)) { Upgrade_OnCompleteProduction(); }
			}
			
			stats[Key.Proteins] += PROTEIN_OUTPUT * GameManager.DeltaCycle;
			stats[Key.Carbs] += CARB_OUTPUT * GameManager.DeltaCycle;
			stats[Key.Lipids] += LIPID_OUTPUT * GameManager.DeltaCycle;
			stats[Key.Genetics] += GENETIC_OUTPUT * GameManager.DeltaCycle;
			
			StatusModifierDecay();
		}
	}

	public void MoveTo(CellData target) {
		Cartesian from = coordination;
		if (target.HasCell) {
			print($"{coordination} cancels Reproduction to occupied cell{target.coordination}");
		}
		else {
			SerializableCell temp = new SerializableCell(this);
			temp.DeserializeOnlyCell(ref target);
			target.SetOwnerData(owner);
			owner.UpdateMovedCellReference(from, target);
			SetOwnerData(Player.NoneType);

			if (Selected) {
				target.interaction.pendingFocus = true;
			}
		}

		string ID = QueueHead;
		progressOfUpgrades[ID] = 0;
		queueOfUpgrade.RemoveAt(0);
		MoveTarget = null;
		target.interaction.overriden = "";
		if (Selected) {
			GameManager._.upgradeHandler.OnCompleteUpgradeWhileFocus(this);
		}
	}

	public void ReproduceTo(Reproduce target) {
		if (!target.Item1.HasCell) {
			target.Item1.SpawnNewCell(owner, target.Item2, this);
			if (target.Item2 == SpawnType.Budding) target.Item1.AddStatus("Recovering");
		}		
		else {
			print($"{coordination} cancels Reproduction to occupied cell{target.Item1.coordination}");
			stats.Increment(Key.Proteins, REPRODUCTION_PROTEINS_COST);
			stats.Increment(Key.Carbs, REPRODUCTION_CARBS_COST);
			stats.Increment(Key.Lipids, REPRODUCTION_LIPIDS_COST);
		}
		ReproduceTarget.RemoveAt(0);
		target.Item1.interaction.overriden = "";

		string ID = QueueHead;
		progressOfUpgrades[ID] = 0;
		queueOfUpgrade.RemoveAt(0);
		if (Selected) {
			GameManager._.upgradeHandler.OnCompleteUpgradeWhileFocus(this);
		}
	}

	public void Action_Move(CellData target) {
		MoveTarget = target;
		target.interaction.overriden = "COVET";
		Upgrade_OnIssueProduction($"a0 {target.coordination}");
	}
	public void Action_Reproduction(Reproduce target) {
		//stats.Deduct(Key.Proteins, REPRODUCTION_PROTEINS_COST);
		stats.Deduct(Key.Proteins, REPRODUCTION_PROTEINS_COST);
		stats.Deduct(Key.Carbs, REPRODUCTION_CARBS_COST);
		stats.Deduct(Key.Lipids, REPRODUCTION_LIPIDS_COST);
		ReproduceTarget.Add(target);
		target.Item1.interaction.overriden = "COVET";
		Upgrade_OnIssueProduction($"a1 {target.Item1.coordination}");
	}

	void CellSizeGrowth() {
		stats[Key.SizeGrowthProgress] += SizeGrowthRate * GameManager.DeltaCycle;
		if (stats[Key.SizeGrowthProgress] >= stats[Key.SizeGrowthCost]) {
			stats[Key.PrevSizeGrowthCost] = stats[Key.SizeGrowthCost];
			stats[Key.Size] += 1;
			stats[Key.SizeGrowthCost] = GrowthCostOfSize(stats[Key.Size]);
			
			if (traits.Contains("t2")) {
				stats.HealthChange(5);
			}
		}
	}

	void CellAging() {
		Age += GetCombinedStat(Key.AgingRate) * GameManager.DeltaCycle;
		if (Age > LIFESPAN) {
      stats.HealthChange(-(Age - LIFESPAN) * Time.deltaTime);

      //CellDied();
    }
	}

	void CellHealthCheck() {
		// Damage by Temperature
		float damagePerCycle = DAMAGE_BY_TEMPERATURE + DAMAGE_BY_MOISTURE;
		// 
		//
		
		stats.HealthChange(- damagePerCycle * GameManager.DeltaCycle);
		if (stats[Key.Health] < 0.06f) {
			interaction.cellDeathSequenceActive = true;
			//StartCoroutine(interaction.OnCellDeathSequence());
		}
    if (interaction.IsDyingSequenceCompleted)
    {
      CellDied();
    }
	}

	void CellDied() {
    owner.RemovePlayerCell(this);
		SetOwnerData(Player.NoneType);
		_owner = new SerializablePlayer(owner);

		if (GameManager.selected == this) {
			interaction.Unselected();
			GameManager._.CameraLostCellFocus();
			GameManager.selected = null;
		}
		interaction.IsDyingSequenceCompleted = false;
	}

	void StatusModifierDecay() {
		//List<string> toRemove = new List<string>();
		List<string> keys = new List<string>(modifiers.Keys);
		foreach (var status in keys) {
			modifiers[status] -= GameManager.DeltaCycle;
			if (modifiers[status] <= 0) {
				//if (status == "Developing Phase") {
				//cellStats[Proteins] += parent.cellStats[Proteins] / 2;
				//parent.cellStats[Proteins] /= 2;
				//}
				//if (status == "Developer Phase") {
				//	cellStats["ProteinResource"] /= 2;
				//}
				modifiers.Remove(status);
			}
			//if (statusModifier[status] < 0) toRemove.Add(status);
		}
		//foreach (var item in toRemove) {
		//	statusModifier.Remove(item);
		//}
	}

#region CELL UPGRADE ROUTINES
	public void Upgrade_OnIssueProduction(string upgradeId) {
		queueOfUpgrade.Add(upgradeId);
		float progressOfUpgradeId;
		bool isProgressExist = progressOfUpgrades.TryGetValue(upgradeId, out progressOfUpgradeId);
		if (!isProgressExist) {
			progressOfUpgrades.Add(upgradeId, 0f);
		}
		print($"{GameManager.GetTime} Cell{coordination} upgrading: {UpgradeReference._this[upgradeId.Split(' ')[0]].Name}");
	}

	public void Upgrade_OnRemoveProduction(string _command) {
		string[] command = _command.Split(' ');
		if (command[0] == "a1") {
			Cartesian coord = Cartesian.Parse(command[1]);
			int index = ReproduceTarget.FindIndex(r => r.Item1 == MapManager.GetCell(coord));
			ReproduceTarget.RemoveAt(index);
			queueOfUpgrade.Remove(_command);
		}
		else queueOfUpgrade.Remove(command[0]);
	}

	public void Upgrade_OnCompleteProduction() {
		string ID = QueueHead;
		AddAndApplyUpgrade(ID);
		progressOfUpgrades.Remove(ID);
		queueOfUpgrade.RemoveAt(0);
		
		
		if (Selected) {
			GameManager._.upgradeHandler.OnCompleteUpgradeWhileFocus(this);
			GameManager._.upgradeHandler.ShowAvailableUpgradeEntries(this);
		}
	}

	public void AddAndApplyUpgrade(string upgrade_ID) {
		AddUpgradeID(upgrade_ID);
		ApplyUpgrade(upgrade_ID);
	}

	public void AddUpgradeID(string upgrade_ID) {
		var upgradeData = UpgradeReference._this[upgrade_ID];
		upgrades.Add(upgrade_ID);
		availableUpgrades.Remove(upgrade_ID);
		CheckUnlockDisableOf(upgradeData);
	}

	public void ApplyUpgrade(string upgrade_ID) {
		Upgrade upgrade = UpgradeReference._this[upgrade_ID];
		stats.Combine(upgrade.Effects);
		foreach(Focus focus in upgrade.Focus) {
			Focus newFocus = new Focus(focus);
			this.focus[newFocus.Name] = newFocus;
		}
		foreach(string name in upgrade.Modifiers) {
			if (!statusModifier.Contains(name)) this.AddStatus(name);
		}
	}

	public void AddTrait(string id) {
		Trait trait = TraitsReference._this[id];
		traits.Add(id);
		availableTraits.Remove(id);
		CheckUnlockDisableOf(trait);
		foreach (string name in trait.Modifiers) {
			if (!statusModifier.Contains(name)) this.AddStatus(name);
		}
	}

	void CheckUnlockDisableOf(Record record) {
		var unlockUpgrade = record.Unlock.Where(u => u.ContainsAny("u", "a"));
		var unlockTrait = record.Unlock.Where(u => u.Contains("t"));
		var disableUpgrade = record.Disable.Where(u => u.ContainsAny("u", "a"));
		var disableTrait = record.Disable.Where(u => u.Contains("t"));
		availableUpgrades.ExceptWith(disableUpgrade);
		disabledUpgrades.UnionWith(disableUpgrade);
		availableTraits.ExceptWith(disableTrait);
		disabledTraits.UnionWith(disabledTraits);

		HashSet<string> existingUpgradeTrait = new HashSet<string>();
		foreach (var u in upgrades) { existingUpgradeTrait.Add(Record.GetSuperVariant(u)); existingUpgradeTrait.Add(u); }
		foreach (var t in traits) { existingUpgradeTrait.Add(Record.GetSuperVariant(t)); existingUpgradeTrait.Add(t); }

		foreach (var unlock in unlockUpgrade) {
			var upgradeEntry = UpgradeReference._this[unlock];
			if (upgradeEntry.Require.IsSubsetOf(existingUpgradeTrait)) {
				bool isAdded = availableUpgrades.Add(unlock);
				if (isAdded && !progressOfUpgrades.ContainsKey(unlock)) progressOfUpgrades.Add(unlock, 0f);
			}
		}

		foreach (var unlock in unlockTrait) {
			var traitEntry = TraitsReference._this[unlock];
			if (traitEntry.Require.IsSubsetOf(existingUpgradeTrait)) {
				availableTraits.Add(unlock);
			}
		}
	}
#endregion

	public void InitCellData(Cartesian coordination) {
		this.coordination = coordination;
	}

	public void SetOwnerData(Player _owner) {
		//if (_owner != Player.NoneType) _owner.AddPlayerCell(this);
		this.owner = _owner;
		this._owner = new SerializablePlayer(_owner);
		interaction.ApplyCellBodyColor(owner.COLOR);
  }

	public void SpawnNewCell(Player _owner, SpawnType type, CellData _parent = null) {
		interaction.IsDyingSequenceCompleted = false;
		SetOwnerData(_owner);
		interaction.overriden = "";
		Age = 0;

		if (type == SpawnType.Empty) {
			ConfigEmptyTile();
			return;
		}
		
		if (type >= SpawnType.Primitive) {
			ConfigBasicStat();
			_owner.AddPlayerCell(this);
		}

		if (type > SpawnType.Primitive) {
			InheritStats(_parent, type);
		}
	}

	public bool ContainsStatus(params string[] param) {
		if (param[0] == "Reproductions") param = new string[] { "Developing Phase", "Developer Phase", "Immature Phase" };
		foreach (var item in param) {
			if (modifiers.ContainsKey(item)) { return true; }
		}
		return false;
	}

	public CellData FindBestTile(string option) {
		List<CellData> bestTiles = new List<CellData>();
		dynamic adjacents = this.adjacents;
		if (option.ContainsAny("Reproduction", "TileDamage")) {
			adjacents = this.adjacents.GetEmpty;
		}
		foreach (var adj in adjacents) {
			if (bestTiles.Count == 0) bestTiles.Add(adj);
			if (option == Key.Food) {
				if (adj.tile[Key.Food] > bestTiles[0].tile[Key.Food]) { bestTiles.Clear(); bestTiles.Add(adj); }
				if (adj.tile[Key.Food] == bestTiles[0].tile[Key.Food]) bestTiles.Add(adj);
			}
			else if (option == "TileDamage") {
				if (adj.TOTAL_DAMAGE_BY_TILE < bestTiles[0].TOTAL_DAMAGE_BY_TILE) { bestTiles.Clear(); bestTiles.Add(adj); }
				if (adj.TOTAL_DAMAGE_BY_TILE == bestTiles[0].TOTAL_DAMAGE_BY_TILE) bestTiles.Add(adj);
			}
		}

		if (bestTiles.Count > 1) {
			int index = Random.Range(0, bestTiles.Count);
			return bestTiles[index];
		}
		else if (bestTiles.Count == 0) { return null; }
		else {
			return bestTiles[0];
		}
	}
}

public class CellAdjacents: List<CellData>
{
	public CellAdjacents() { }
	public CellAdjacents(CellData[] adj) {
		AddRange(adj);
	}

	public bool Contains(Cartesian coordination) {
		for (int i = 0; i < Count; i++) {
			if (this[i].coordination == coordination) return true;
		}
		return false;
	}
	public List<CellData> GetEmpty { get { return this.Where(cell => !cell.HasCell).ToList(); } }
	public static List<CellData> GetFriendlyOf(CellData self) { return self.adjacents.Where(cell => cell.owner == self.owner).ToList(); }
	public static List<CellData> GetHostileOf(CellData self) { return self.adjacents.Where(cell => cell.owner != self.owner && cell.HasCell).ToList(); }
}