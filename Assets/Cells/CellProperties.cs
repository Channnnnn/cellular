﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public enum SpawnType { Empty, Primitive, Budding, Binary, Sexual, Spore }
public static class KEY
{
	public static string Modifier(this string value, string extra = "") { return value + "Modifier" + extra; }
	public static string Discount(this string value, string extra = "") { return value + "Discount" + extra; }
	public static string Penalty(this string value, string extra = "") { return value + "Penalty" + extra; }

	public static string EnergyCost(this string value, string extra = "") { return value + "EnergyCost" + extra; }
	public static string ProteinsCost(this string value, string extra = "") { return value + "ProteinsCost" + extra; }
	public static string CarbsCost(this string value, string extra = "") { return value + "CarbsCost" + extra; }
	public static string LipidsCost(this string value, string extra = "") { return value + "LipidsCost" + extra; }
	public static string Base(this string value, string extra = "") { return value + "Base" + extra; }
	public static string TraitPenalty(this string value, string extra = "") { return value + "TraitPenalty" + extra; }
	public static string UpgradePenalty(this string value, string extra = "") { return value + "UpgradePenalty" + extra; }
	public static string GenerationPenalty(this string value, string extra = "") { return value + "GenerationPenalty" + extra; }
	public static string AdjacentPenalty(this string value, string extra = "") { return value + "AdjacentPenalty" + extra; }
}
public class Key
{
	/// <summary>
	/// Keyword that their default value should be 1
	/// </summary>
	public static readonly string[] Multiplier = { "Rate", "Modifier", "Resistance", "Discount" };

	public static readonly string Reproduction = "Reproduction";
	public static readonly string
		/*Cell properties*/
		FocusPoint = "FocusPoint",
		ReproductionCount = "ReproductionCount",
		MaxHealth = "MaxHealth", MaxSize = "MaxSize",

		Health = "Health", Lifespan = "Lifespan", Size = "Size", Space = "Space",

		/*Tile properties*/
		Food = "Food", Acidity = "Acidity", Temperature = "Temperature", Radioactivity = "Radioactivity",
		Moisture = "Moisture", Oxygen = "Oxygen",
		/*Cell Output*/
		Energy = "Energy", Waste = "Waste",
		/*Cell Production*/
		Protein = "Protein", Proteins = "Proteins",
		Carb = "Carbohydrate", Carbs = "Carbohydrates",
		Lipid = "Lipid", Lipids = "Lipids",
		Genetic = "Genetic", Genetics = "Genetics",

		GrowthRate = "GrowthRate", AgingRate = "AgingRate",
		FoodToGrowthRatio = "FoodToGrowthRatio", SizeGrowthProgress = "SizeGrowthProgress",
		SizeGrowthCost = "SizeGrowthCost", PrevSizeGrowthCost = "PrevSizeGrowthCost",

		ColdThreshold = "ColdThreshold", HeatThreshold = "HeatThreshold",
		DryThreshold = "DryThreshold", WetThreshold = "WetThreshold",
		AcidicThreshold = "AcidicThreshold", AlkalineThreshold = "AlkalineThreshold",
		RadioThreshold = "RadioThreshold",
		AnaeroThreshold = "AnaeroThreshold", AeroThreshold = "AeroThreshold"
		;
	public static readonly string[] NonInheritable = { SizeGrowthProgress, SizeGrowthCost, PrevSizeGrowthCost, Size, Space, Health, Proteins, Carbs, Lipids, Genetics };
	public static readonly string[] BinaryInheritable = { Size, Proteins, Carbs, Lipids, Genetics };
	public static readonly string[] Resources = { Proteins, Carbs, Lipids, Genetics };
	public static readonly string[] Inverts = { AgingRate, Waste, ColdThreshold, DryThreshold, AcidicThreshold, AnaeroThreshold, "Cost", "Penalty" };
}

public partial class CellData
{
	[NonSerialized] public string _ = "_";
	public string CellStatsToRich {
		get {
			string s = "";
			s += $"Size {(int)stats[Key.Size]} (Growth {GrowthProgressOfCurrentSize.Floor(1)}/{GrowthCostCurrentDifference}) ";
			s += $"{(int)stats[Key.Energy]} {Key.Energy}\n".Style(Key.Energy);
			s += $"{(int)stats[Key.Genetics]}<sprite=\"Resources\" index=0>  ".Style(Key.Genetic);
			s += $"{(int)stats[Key.Proteins]}<sprite=\"Resources\" index=1>  ".Style(Key.Protein);
			s += $"{(int)stats[Key.Carbs]}<sprite=\"Resources\" index=2>  ".Style("Carb");
			s += $"{(int)stats[Key.Lipids]}<sprite=\"Resources\" index=3>".Style(Key.Lipid);
			return s;
		}
	}

	/*
	 * 
	 ******************************************** QUEUE ACCESSOR ********************************************
	 ********************************************************************************************************
	 */
	public bool Queue { get { return queueOfUpgrade.Count > 0; } }
	/// <summary>
	/// Upgrade ID of first element in Queue
	/// </summary>
	public string QueueHead { get { return Queue ? queueOfUpgrade[0] : null; } }
	/// <summary>
	/// Progress of first element in queue in Energy unit
	/// </summary>
	public float QueueProgress {
		get { if (Queue) return progressOfUpgrades[QueueHead]; return float.NaN; }
		set { if (Queue) progressOfUpgrades[QueueHead] = value; }
	}

	/*
	 * 
	 ******************************************** TILES RELATED ACCESSOR ********************************************
	 ****************************************************************************************************************
	 */
	public float TemperatureValue {
		get {
			return tile[Key.Temperature] + (GameManager._.GlobalTemperatureModifier * GameManager._.GlobalTemperatureMultiplier);
		}
	}

	/*
	 * 
	 ******************************************** FINAL STATS RELATED ACCESSOR ********************************************
	 **********************************************************************************************************************
	 */
	public float WASTE_OUTPUT {
		get {
			float Flat = GetCombinedStat(Key.Waste);
			float Modifier = GetCombinedStat(Key.Waste.Modifier());
			return Flat * Modifier;
		}
	}
	public float ENERGY_OUTPUT {
		get {
			float Flat = GetCombinedStat(Key.Energy);
			float Modifier = GetCombinedStat(Key.Energy.Modifier());
			return Flat * Modifier;
		}
	}

	public float PROTEIN_OUTPUT {
		get {
			float Flat = GetCombinedStat(Key.Protein);
			float Modifier = GetCombinedStat(Key.Protein.Modifier());
			return Flat * Modifier;
		}
	}

	public float CARB_OUTPUT {
		get {
			float Flat = GetCombinedStat(Key.Carb);
			float Modifier = GetCombinedStat(Key.Carb.Modifier());
			return Flat * Modifier;
		}
	}

	public float LIPID_OUTPUT {
		get {
			float Flat = GetCombinedStat(Key.Lipid);
			float Modifier = GetCombinedStat(Key.Lipid.Modifier());
			return Flat * Modifier;
		}
	}

	public float GENETIC_OUTPUT {
		get {
			float Flat = GetCombinedStat(Key.Genetic);
			float Modifier = GetCombinedStat(Key.Genetic.Modifier());
			return Flat * Modifier;
		}
	}

	public float LIFESPAN {
		get {
			float Flat = GetCombinedStat(Key.Lifespan);
			float Modifier = GetCombinedStat(Key.Lifespan.Modifier());
			return Flat * Modifier;
		}
	}

	public float HEALTH {
		get {
			float Flat = GetCombinedStat(Key.Health);
			float Modifier = GetCombinedStat(Key.Health.Modifier());
			return Flat * Modifier;
		}
	}

	public float MAXHEALTH {
		get {
			float Flat = GetCombinedStat(Key.MaxHealth);
			float Modifier = GetCombinedStat(Key.MaxHealth.Modifier());
			return Flat * Modifier;
		}
	}

	public float TOTAL_DAMAGE_BY_TILE { get {
			return DAMAGE_BY_TEMPERATURE + DAMAGE_BY_MOISTURE + DAMAGE_BY_OXYGEN;
		} }
	public float DAMAGE_BY_TEMPERATURE { get {
			float difference = tile.Getf(Key.Temperature).RangeDifference(stats.Getf("ColdThreshold"), stats.Getf("HeatThreshold"));
			float damage = difference / (stats.Getf("HeatThreshold") - stats.Getf("ColdThreshold"));
			if (difference > 0) {
				return damage / GetCombinedStat("HeatResistance");
			}
			else if (difference < 0) {
				return damage.Abs() / GetCombinedStat("ColdResistance");
			}
			else return 0f;
	} }

	public float DAMAGE_BY_MOISTURE { get {
			float difference = tile.Getf(Key.Moisture).RangeDifference(stats.Getf("DryThreshold"), stats.Getf("WetThreshold"));
			float damage = difference / (stats.Getf("WetThreshold") - stats.Getf("DryThreshold"));
			if (difference > 0) {
				return damage / GetCombinedStat("WetResistance");
			}
			else if (difference < 0) {
				return damage.Abs() / GetCombinedStat("DryResistance");
			}
			else return 0f;
	} }

	public float DAMAGE_BY_OXYGEN { get {
			float difference = tile.Getf(Key.Oxygen).RangeDifference(stats.Getf("AnaeroThreshold"), stats.Getf("AeroThreshold"));
			float damage = difference / (stats.Getf("AeroThreshold") - stats.Getf("AnaeroThreshold"));
			if (difference > 0) {
				return damage / GetCombinedStat("AnaeroResistance");
			}
			else if (difference < 0) {
				return damage.Abs() / GetCombinedStat("AeroResistance");
			}
			else return 0f;
	} }

	public float DAMAGE_BY_STARVATION {
		get {
			var friendlies = CellAdjacents.GetFriendlyOf(this);
			var hostiles = CellAdjacents.GetHostileOf(this);

			float friendlies_food = friendlies.Select(f => f.tile.Getf(Key.Food)).Sum() / friendlies.Count;

			float hostiles_size = hostiles.Select(h => h.stats.Getf(Key.Size)).Sum();

			float friendly_pressure = friendlies_food + stats.Getf(Key.Size);
			float hostile_pressure = hostiles_size + hostiles.Count;

			float difference = friendly_pressure - hostile_pressure;
			if (difference >= 0) {
				return 0f;
			}
			else if (difference < 0) {
				return difference.Abs() * GetCombinedStat("StarveResistance");
			}
			else return 0f;
		}
	}
	/*
	 * 
	 ******************************************** FINAL UPGRADE COST RELATED ACCESSOR ********************************************
	 *****************************************************************************************************************************
	 */
	public float UPGRADE_ENERGY_COST(string id) {
		float Cost = UpgradeReference.GetEnergyCost(id);
		float Discount = GetCombinedStat(Key.Energy.Discount());
		return Cost * Discount;
	}

	public float UPGRADE_ENERGY_PROGRESS(string id) {
		return progressOfUpgrades.Get(id) / UPGRADE_ENERGY_COST(id);
	}

	public float UPGRADE_ENERGY_COST_REMAINING(string id) {
		return UPGRADE_ENERGY_COST(id) - progressOfUpgrades.Get(id);
	}

	public float UPGRADE_CYCLE_REMAINING(string id) {
		return UPGRADE_ENERGY_COST_REMAINING(id) / ENERGY_OUTPUT;
	}

	/*
	 * 
	 ******************************************** FINAL REPRODUCTION COST RELATED ACCESSOR ********************************************
	 **********************************************************************************************************************************
	 */
	public float REPRODUCTION_ENERGY_COST {
		get {
			float Base = GetCombinedStat(Key.Reproduction.EnergyCost(_).Base());
			return (Base +
				ReproductionEnergyCost_TraitPenalty +
				ReproductionEnergyCost_UpgradePenalty +
				ReproductionEnergyCost_GenerationPenalty) * ReproductionEnergyCost_Modifier;
		}
	}
	public float REPRODUCTION_PROTEINS_COST { get {
			float BaseCost = GetCombinedStat(Key.Reproduction.ProteinsCost(_.Base()));
			float Modifier = GetCombinedStat(Key.Reproduction.ProteinsCost(_.Modifier()));

			return (BaseCost + ReproductionProteinsCost_UpgradePenalty) * Modifier;
		} }
	public float REPRODUCTION_CARBS_COST { get {
			float BaseCost = GetCombinedStat(Key.Reproduction.CarbsCost(_.Base()));
			float Modifier = GetCombinedStat(Key.Reproduction.CarbsCost(_.Modifier()));

			return (BaseCost + ReproductionCarbsCost_UpgradePenalty) * Modifier;
		} }
	public float REPRODUCTION_LIPIDS_COST { get {
			float BaseCost = GetCombinedStat(Key.Reproduction.LipidsCost(_.Base()));
			float Modifier = GetCombinedStat(Key.Reproduction.LipidsCost(_.Modifier()));

			return (BaseCost + ReproductionLipidsCost_UpgradePenalty) * Modifier;
		} }
	/*
	 * 
	 ******************************************** INTERMEDIATE STATS RELATED ACCESSOR ********************************************
	 ***************************************************************************************************************************** 
	 */
	public float GetCombinedStat(string key) {
		float Stat = stats.Getf(key);
		float Effect = focus.GetComputedEffect(key);
		if (key.ContainsAny(Key.Multiplier)) return Stat * Effect;
		return Stat + Effect;
	}
	public float SizeGrowthRate { get {
			return tile[Key.Food] / GetCombinedStat(Key.FoodToGrowthRatio) * GetCombinedStat(Key.GrowthRate);
		} }

	public float GrowthCostOfSize(float nth) {
		return nth.Floorf() * (nth.Floorf() + 3) / 2;
	}

	public float GrowthProgressOfCurrentSize { get{
			return stats[Key.SizeGrowthProgress] - stats[Key.PrevSizeGrowthCost];
	} }

	public float GrowthCostCurrentDifference { get {
			return stats[Key.SizeGrowthCost] - stats[Key.PrevSizeGrowthCost];
	} }

	public float SpaceRemaining { get {
			float space = 0;
			foreach (var id in upgrades) {
				space += UpgradeReference._this[id].Space;
			}
			stats[Key.Space] = space;
			return space;
		} }

	public float GetSpaceBalance() {
		return stats[Key.Size] - SpaceRemaining;
	}

	/*================================== REPRODUCITON RELATED (ENERGY) ==================================*/
	public float ReproductionEnergyCost_Modifier { get {
			float PerAdjacentPenalty = stats[Key.Reproduction.EnergyCost(_).AdjacentPenalty()];
			return ReproductionEnergyCost_ReducitonTrait * (1f + (PerAdjacentPenalty * GetAdjacentFriendly().Count));
	}	}
	public float ReproductionEnergyCost_ReducitonTrait { get {
			return 1f + GetCombinedStat("Reproduction Energy Cost") /*sum of Energy Reduction from traits*/;
	}	}
	public float ReproductionEnergyCost_TraitPenalty { get {
			return stats[Key.Reproduction.EnergyCost(_.TraitPenalty())] * traits.Count;
	}	}
	public float ReproductionEnergyCost_UpgradePenalty { get {
			return stats[Key.Reproduction.EnergyCost(_.UpgradePenalty())] * upgrades.Count;
	}	}
	public float ReproductionEnergyCost_GenerationPenalty {	get {
			return stats[Key.Reproduction.EnergyCost(_.GenerationPenalty())] * stats[Key.ReproductionCount];
	}	}

	/*================================== REPRODUCITON RELATED (PROTEIN) ==================================*/
	public float ReproductionProteinsCost_UpgradePenalty { get {
			float proteinsSum = 0;
			foreach (var id in upgrades) {
				proteinsSum += UpgradeReference._this[id].Costs.Get(Key.Proteins);
			}
			return 1f * proteinsSum; //multipler depends on mode of reproduce
	} }

	/*================================== REPRODUCITON RELATED (CARBOHYDRATE) ==================================*/
	public float ReproductionCarbsCost_UpgradePenalty { get {
			float carbsSum = 0;
			foreach (var id in upgrades) {
				carbsSum += UpgradeReference._this[id].Costs.Get(Key.Carbs);
			}
			return 1f * carbsSum; //multipler depends on mode of reproduce
	}	}

	/*================================== REPRODUCITON RELATED (LIPID) ==================================*/
	public float ReproductionLipidsCost_UpgradePenalty { get {
			float lipidsSum = 0;
			foreach (var id in upgrades) {
				lipidsSum += UpgradeReference._this[id].Costs.Get(Key.Lipids);
			}
			return 1f * lipidsSum; //multipler depends on mode of reproduce
	} }

	public List<CellData> GetAdjacentFriendly() {
		return CellAdjacents.GetFriendlyOf(this);
	}
	public List<CellData> GetAdjacentHostile() {
		return CellAdjacents.GetHostileOf(this);
	}
}

public partial class CellData
{
	void ConfigEmptyTile() {
		stats[Key.Health] = 0f;
		stats[Key.Lifespan] = 0f;
		stats[Key.Size] = 0f;
		stats[Key.Energy] = 0f;
		stats[Key.Protein] = 0f;
		stats[Key.Genetic] = 0f;

		stats[Key.Proteins] = 0f;
		stats[Key.Genetics] = 0f;
	}
	void InheritStats(CellData _parent, SpawnType type) {
		parent = _parent;
		focus = new CellFocus(_parent.focus);
		statusModifier = new StatusModifier(_parent.statusModifier);
		upgrades = new HashSet<string>(_parent.upgrades);
		availableUpgrades = new HashSet<string>(_parent.availableUpgrades);
		disabledUpgrades = new HashSet<string>(_parent.disabledUpgrades);
		traits = new HashSet<string>(_parent.traits);
		availableTraits = new HashSet<string>(_parent.availableTraits);
		disabledTraits = new HashSet<string>(_parent.disabledTraits);

		focus.activeFocus.Clear();
		foreach (var f in _parent.focus.activeFocus) {
			focus.Toggle(f.Key, f.Value, true, this);
		}

		foreach (var stat in _parent.stats) {
			if (!Key.NonInheritable.Contains(stat.Key)) stats[stat.Key] = stat.Value;
		}

		if (type == SpawnType.Budding) {
			stats[Key.Size] = 1f;
			stats[Key.Health] = _parent.stats[Key.MaxHealth];
			Key.Resources.ForEach(key => stats[key] = 0f);
		}
		if (type == SpawnType.Binary) {
			stats[Key.Health] = _parent.stats[Key.MaxHealth];
			Key.BinaryInheritable.ForEach(key => {
				float halved = _parent.stats[key] / 2f;
				stats[key] = halved;
				_parent.stats[key] = halved;
			});
      _parent.stats[Key.PrevSizeGrowthCost] = GrowthCostOfSize(stats[Key.Size] - 1);
      _parent.stats[Key.SizeGrowthCost] = GrowthCostOfSize(stats[Key.Size]);
      _parent.stats[Key.SizeGrowthProgress] = _parent.stats[Key.PrevSizeGrowthCost];
    }

		stats[Key.ReproductionCount] = _parent.stats[Key.ReproductionCount] + 1;
		stats[Key.Space] = _parent.stats[Key.Space];
		stats[Key.PrevSizeGrowthCost] = GrowthCostOfSize(stats[Key.Size] - 1);
		stats[Key.SizeGrowthCost] = GrowthCostOfSize(stats[Key.Size]);
		stats[Key.SizeGrowthProgress] = stats[Key.PrevSizeGrowthCost];
	}
	void ConfigBasicStat() {
		//
		//TILE RESISTANCE PARAMETER
		//
		stats["HeatResistance"] = 1f; stats["ColdResistance"] = 1f;
		stats["DryResistance"] = 1f; stats["WetResistance"] = 1f;
		stats["AcidicResistance"] = 1f; stats["AlkalineResistance"] = 1f;
		stats["AnaeroResistance"] = 10f; stats["AeroResistance"] = 14f;
		stats["RadioResistance"] = 1f;
		stats["StarveResistance"] = 1f;
		//
		//CELL RESISTANCE PARAMETER
		//
		stats["BreachResistance"] = 1f;
		stats["InhibitionResistance"] = 1f;
		//
		//OPTIMAL TILE PARAMETER
		//
		stats["ColdThreshold"] = 20f; stats["HeatThreshold"] = 30f;
		stats["DryThreshold"] = 8f; stats["WetThreshold"] = 12f;
		stats["AcidicThreshold"] = 6f; stats["AlkalineThreshold"] = 8f;
		stats["RadioThreshold"] = 3f;
		stats["AnaeroThreshold"] = 10f; stats["AeroThreshold"] = 14f;
		//
		//CELL RATE PARAMETER
		//
		stats[Key.GrowthRate] = 1f;
		stats[Key.AgingRate] = 1f;
		stats[Key.FoodToGrowthRatio] = 10f;
		//
		//CELL STATUS PARAMETER
		//
		stats[Key.Lifespan] = 100f; stats[Key.Lifespan.Modifier()] = 1f;
		stats[Key.Health] = 100f; stats[Key.MaxHealth] = 100f;
		stats[Key.Size] = 1f; stats[Key.MaxSize] = 10f;
		stats[Key.Space] = 0f;
		stats[Key.ReproductionCount] = 0f;
		stats[Key.Energy] = 1f; stats[Key.Energy.Modifier()] = 1f; stats[Key.Energy.Discount()] = 1f;
		stats[Key.Waste] = 1f;
		//
		//CELL RESOURCES PARAMETER
		//
		stats[Key.Protein] = 1f; stats[Key.Proteins] = 0f;
		stats[Key.Carb] = 1f; stats[Key.Carbs] = 0f;
		stats[Key.Lipid] = 1f; stats[Key.Lipids] = 0f;
		stats[Key.Genetic] = 1f; stats[Key.Genetics] = 0f; stats[Key.Genetics.Discount()] = 1f;
		//
		//CELL PROGRESSION
		//
		stats[Key.SizeGrowthProgress] = 0f;
		stats[Key.SizeGrowthCost] = stats[Key.Size] + 1f;
		stats[Key.PrevSizeGrowthCost] = 0f;

		//
		//REPRODUCTION PARAMETERS
		//
		stats[Key.Reproduction.EnergyCost(_.Base())] = 2f;
		stats[Key.Reproduction.EnergyCost(_.TraitPenalty())] = 1f;
		stats[Key.Reproduction.EnergyCost(_.UpgradePenalty())] = 0.5f;
		stats[Key.Reproduction.EnergyCost(_.GenerationPenalty())] = 0f; //Binary 2
		stats[Key.Reproduction.EnergyCost(_.AdjacentPenalty())] = 0f; //Binary 0.1 
		/* ReproductionEnergyCost_Modifier Accessor */
		/* ReproductionEnergyCost_ReductionTrait Accessor */

		stats[Key.Reproduction.ProteinsCost(_.Base())] = 5f;
		stats[Key.Reproduction.ProteinsCost(_.Modifier())] = 1f;
		/* ReproductionProteinsCost_UpgradePenalty Accessor */

		stats[Key.Reproduction.CarbsCost(_.Base())] = 2.5f;
		stats[Key.Reproduction.CarbsCost(_.Modifier())] = 1f;
		/* ReproductionCarbsCost_UpgradePenalty Accessor */

		stats[Key.Reproduction.LipidsCost(_.Base())] = 2.5f;
		stats[Key.Reproduction.LipidsCost(_.Modifier())] = 1f;
		/* ReproductionLipidsCost_UpgradePenalty Accessor */

		stats[Key.FocusPoint] = 1;
	}
}