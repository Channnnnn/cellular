﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
[InitializeOnLoad]
public class StopPlayingOnRecompile
{
	static StopPlayingOnRecompile() {
		EditorApplication.update -= StopPlayingIfRecompiling;
		EditorApplication.update += StopPlayingIfRecompiling;
	}
	static void StopPlayingIfRecompiling() {
		if (EditorApplication.isCompiling & EditorApplication.isPlaying) {
			EditorApplication.isPlaying = false;
		}
	}
}
#endif

[System.Serializable]
public struct Cartesian
{
	public int x, y;
	public Cartesian(int _x = 0, int _y = 0) { x = _x; y = _y; }
	public static Cartesian Parse(string cartesianString) {
		var txt = cartesianString.Split(new char[]{ ',', '(', ')' }, System.StringSplitOptions.RemoveEmptyEntries);
		if (txt.Length != 2) { return Null; }
		int x, y;
		int.TryParse(txt[0], out x);
		int.TryParse(txt[1], out y);
		return new Cartesian(x, y);
	}
	public static Cartesian Null { get { return new Cartesian(int.MaxValue, int.MaxValue); } }
	public override string ToString() {
		return $"({x},{y})";
	}
	public override bool Equals(object obj) {	return x == ((Cartesian)obj).x && y == ((Cartesian)obj).y; }
	public override int GetHashCode() {
		var hashCode = 1502939027;
		hashCode = hashCode * -1521134295 + base.GetHashCode();
		hashCode = hashCode * -1521134295 + x.GetHashCode();
		hashCode = hashCode * -1521134295 + y.GetHashCode();
		return hashCode;
	}
	public static bool operator == (Cartesian A, Cartesian B) { return A.x == B.x && A.y == B.y; }
	public static bool operator !=(Cartesian A, Cartesian B) { return A.x == B.x && A.y == B.y; }
}

public class MapManager : MonoBehaviour
{
	public CellData cellPrefab;
	public Text labelPrefab;
	public Color32 labelBaseColor;
	Canvas labelCanvas;
	Transform mapRoot;
	public Cartesian mapDimension;
	public MapType mapType;
	public Dictionary<Cartesian, CellData> CellsData { get; set; } = new Dictionary<Cartesian, CellData>();
	List<Text> cellLabel = new List<Text>();
	public enum MapType
	{
		Diamond,Hex,Rect
	}
	
	void Start() {
		//player1Color = GameManager._.playerColor;
		//if (labelCanvas == null) labelCanvas = gameObject.GetComponentInChildren<Canvas>();
		//mapGrid = GameObject.FindGameObjectWithTag("GridRoot").transform;

		//CreateMap(mapType);
		//Random.InitState(0);
		//RandomPlayerCellSpawn();
		//LateStart();
	}

	void ClearTiles() {
		var labels = labelCanvas.GetComponentsInChildren<Text>();
		var mapTiles = mapRoot.GetComponentsInChildren<Transform>();
		foreach (Transform tile in mapTiles) {
			if (tile.GetInstanceID() != mapRoot.GetInstanceID()) {
				GameObject.Destroy(tile.gameObject);
			}
		}
		foreach (Text text in labels) {
			Destroy(text.gameObject);
		}
	}

	public void LateStart(List<Player> players_ingame) {
		if (labelCanvas == null) labelCanvas = gameObject.GetComponentInChildren<Canvas>();
		mapRoot = GameObject.FindGameObjectWithTag("GridRoot").transform;

		CellsData.Clear();
		ClearTiles();

		CreateMap(mapType);
		//Random.InitState(0);
		SeedTileProperties(CellsData.Values.ToList());

		foreach(Player player in players_ingame) {
			RandomPlayerCellSpawn(CellsData.Values.ToList(), player);
		}
	}

	public void RestartGame(List<Player> players_ingame) {
		foreach (Player player in players_ingame) {
			RandomPlayerCellSpawn(CellsData.Values.ToList(), player);
		}
	}

	public void RestoreMap(Dictionary<Cartesian, SerializableCell> cellsData) {
		ClearTiles();
		foreach(var tile in cellsData) {
			CreateCell(tile.Value);
		}
		foreach(var cell in CellsData) {
			cell.Value.adjacents = new CellAdjacents(GetAdjacentCellData(cell.Value.coordination));
		}
	}

	// Update is called once per frame
	void Update() {
		if (labelCanvas) {
			labelCanvas.gameObject.SetActive(Global.IsShowingDebug);
		}
	}

	void CreateCell(int x, int y) {
		float height = 1.49f;
		float width = 1.29f;
		Vector3 position = new Vector3((x + y*0.5f) * 2f * width, (-y) * 1.5f * height, 0);
		CellData cell = Instantiate<CellData>(cellPrefab);
		cell.transform.SetParent(mapRoot, false);
		cell.transform.localPosition = position;
		cell.name = "Grid" + x + ":" + y;
		cell.InitCellData(new Cartesian(x, y));
		cell.SpawnNewCell(Player.NoneType, SpawnType.Empty);
		CellsData[cell.coordination] = cell;

		CreateLabel(x, y, position);
	}

	void CreateCell(SerializableCell tile) {
		float height = 1.49f;
		float width = 1.29f;
		int x = tile.Coordination.x, y = tile.Coordination.y;
		Vector3 position = new Vector3((x + y * 0.5f) * 2f * width, (-y) * 1.5f * height, 0);
		CellData cell = Instantiate<CellData>(cellPrefab);
		cell.transform.SetParent(mapRoot, false);
		cell.transform.localPosition = position;
		cell.name = "Grid" + x + ":" + y;
		tile.Deserialize(ref cell);
		cell.SetOwnerData(Player.NoneType);
		cell.interaction = cell.GetComponent<CellInteraction>();
		cell.interaction.cell = cell;

		GameManager._.mapManager.CellsData[cell.coordination] = cell;

		CreateLabel(x, y, position);
	}

	void CreateLabel(int x, int y, Vector3 position) {
		Text label = Instantiate(labelPrefab);
		label.rectTransform.SetParent(labelCanvas.transform, false);
		label.rectTransform.anchoredPosition = new Vector2(position.x, position.y);
		label.text = x + ":" + y;
		label.name = "Label " + x + ":" + y;
		label.color = labelBaseColor;
		cellLabel.Add(label);
	}

	public void RandomPlayerCellSpawn(List<CellData> cellData, Player player) {
		int randomLocation;
		int limiter = 0;
		do {
			randomLocation = Random.Range(0, cellData.Count);
			limiter++;
		} while ((cellData[randomLocation].HasCell 
					|| cellData[randomLocation].tile[Key.Temperature].RangeDifference(20,30).Abs() > 15
					|| cellData[randomLocation].tile[Key.Oxygen].RangeDifference(10, 14).Abs() > 6
					|| cellData[randomLocation].tile[Key.Moisture].RangeDifference(8, 12).Abs() > 6) && limiter < 10);
		cellData[randomLocation].SpawnNewCell(player, SpawnType.Primitive);

		if (player.TYPE == Player.Type.Human) GameManager._.cameraController.SetFocus(cellData[randomLocation].transform.position);
		print($"SPAWN {player.NAME} at {cellData[randomLocation].coordination}");
	}

	void CreateMap(MapType t) {
		if (t == MapType.Diamond) {
			CreateMapParalell(mapDimension.x, mapDimension.y);
			GameManager._.cameraController.HorizontalPanRange = new Vector2(0, 2.5f * (mapDimension.x + (mapDimension.y - 1) / 2));
			GameManager._.cameraController.VerticalPanRange = new Vector2(-2f * mapDimension.y, 0);
		}
		if (t == MapType.Hex) {
			CreateMapHex(mapDimension.x);
			GameManager._.cameraController.HorizontalPanRange = new Vector2(-2.5f * mapDimension.x, 2.5f * mapDimension.x);
			GameManager._.cameraController.VerticalPanRange = new Vector2(-2.5f * mapDimension.x, 2.5f * mapDimension.x);
		}
		if (t == MapType.Rect) {
			CreateMapRect(mapDimension.x, mapDimension.y);
			GameManager._.cameraController.HorizontalPanRange = new Vector2(0, 2.5f * mapDimension.x);
			GameManager._.cameraController.VerticalPanRange = new Vector2(-2f * mapDimension.y, 0);
		}
		//Find Adjacents
		foreach (var cell in CellsData) {
			//print(string.Join(" ", GetAdjacentCellData(cell.Value.coordination).Count()));
			cell.Value.adjacents = new CellAdjacents(GetAdjacentCellData(cell.Value.coordination));
			cell.Value.statusModifier = new StatusModifier();
		}
	}

	void CreateMapHex(int radius) {
		for (int q = -radius; q <= radius; q++) {
			int r1 = Mathf.Max(-radius, -q - radius);
			int r2 = Mathf.Min(radius, -q + radius);
			for (int r = r1; r <= r2; r++) {
				CreateCell(q, r);
			}
		}
	}

	void CreateMapParalell(int w, int h) {
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				CreateCell(x, y);
			}
		}
	}

	void	CreateMapRect(int w, int h) {
		for (int y = 0; y < h; y++) {
			int y_offset = y >> 1;
			for (int x = -y_offset; x < w - y_offset; x++) {
				CreateCell(x, y);
			}
		}
	}

	public void ReseedTileProperties() {
		SeedTileProperties(CellsData.Values.ToList());
	}

	void SeedTileProperties(List<CellData> cellData) {
		GameManager game = GameManager._;
		float food_mean = game.foodParameters.x,
			food_var = game.foodParameters.y,
			food_radius_mean = game.foodParameters.z,
			food_radius_var = game.foodParameters.w,
			food_source_count = cellData.Count * game.foodDensity;
		float temperature_mean = game.temperatureParameters.x,
			temperature_var = game.temperatureParameters.y,
			temperature_range_mean = game.temperatureParameters.z,
			temperature_range_var = game.temperatureParameters.w,
			temperature_point = cellData.Count * game.temperatureVariance;
		float moisture_mean = game.moistureParameters.x,
			moisture_var = game.moistureParameters.y,
			moisture_range_mean = game.moistureParameters.z,
			moisture_range_var = game.moistureParameters.w,
			moisture_count = cellData.Count * game.moistureDensity;
		float oxygen_mean = game.oxygenParameter.x,
			oxygen_var = game.oxygenParameter.y,
			oxygen_range_mean = game.oxygenParameter.z,
			oxygen_range_var = game.oxygenParameter.w,
			oxygen_count = cellData.Count * game.oxygenDensity;

		List<TileInfluenceData> food_source = new List<TileInfluenceData>();
		List<TileInfluenceData> temperature = new List<TileInfluenceData>();
		List<TileInfluenceData> moisture = new List<TileInfluenceData>();
		List<TileInfluenceData> oxygen_source = new List<TileInfluenceData>();

		for (int i = 0; i < food_source_count; i++) {
			int randomedLocation = Random.Range(0, cellData.Count);
			TileInfluenceData randomed = new TileInfluenceData {
				coordination = cellData[randomedLocation].coordination,
				Value = Random.Range(food_mean - food_var, food_mean + food_var),
				Radius = Random.Range(food_radius_mean - food_radius_var, food_radius_mean + food_radius_var)
			};
			food_source.Add(randomed);
		}
		for (int i = 0; i < temperature_point; i++) {
			int randomedLocation = Random.Range(0, cellData.Count);
			TileInfluenceData randomed = new TileInfluenceData {
				coordination = cellData[randomedLocation].coordination,
				Value = Random.Range(temperature_mean - temperature_var, temperature_mean + temperature_var),
				Radius = Random.Range(temperature_range_mean - temperature_range_var, temperature_range_mean + temperature_range_var)
			};
			temperature.Add(randomed);
		}
		for (int i = 0; i < moisture_count; i++) {
			int randomedLocation = Random.Range(0, cellData.Count);
			TileInfluenceData randomed = new TileInfluenceData {
				coordination = cellData[randomedLocation].coordination,
				Value = Random.Range(moisture_mean - moisture_var, moisture_mean + moisture_var),
				Radius = Random.Range(moisture_range_mean - moisture_range_var, moisture_range_mean + moisture_range_var)
			};
			moisture.Add(randomed);
		}
		for (int i = 0; i < oxygen_count; i++) {
			int randomedLocation = Random.Range(0, cellData.Count);
			TileInfluenceData randomed = new TileInfluenceData {
				coordination = cellData[randomedLocation].coordination,
				Value = Random.Range(oxygen_mean - oxygen_var, oxygen_mean + oxygen_var),
				Radius = Random.Range(oxygen_range_mean - oxygen_range_var, oxygen_range_mean + oxygen_range_var)
			};
			oxygen_source.Add(randomed);
		}

		foreach (var cell in cellData) {
			List<Vector2> food_influences = new List<Vector2>();
			List<Vector2> temperature_influences = new List<Vector2>();
			List<Vector2> moisture_influences = new List<Vector2>();
			List<Vector2> oxygen_influences = new List<Vector2>();
			foreach (var source in food_source) {
				float distance_from_source = GetDistance(cell.coordination, source.coordination);
				food_influences.Add(new Vector2(source.Influence(distance_from_source), source.Value));
			}
			foreach (var source in temperature) {
				float distance_from_source = GetDistance(cell.coordination, source.coordination);
				temperature_influences.Add(new Vector2(source.Influence(distance_from_source), source.Value));
			}
			foreach (var source in moisture) {
				float distance_from_source = GetDistance(cell.coordination, source.coordination);
				moisture_influences.Add(new Vector2(source.Influence(distance_from_source), source.Value));
			}
			foreach (var source in oxygen_source) {
				float distance_from_source = GetDistance(cell.coordination, source.coordination);
				oxygen_influences.Add(new Vector2(source.Influence(distance_from_source), source.Value));
			}

			cell.tile[Key.Food] = 0f;
			cell.tile[Key.Temperature] = 0f;
			cell.tile[Key.Moisture] = 0f;
			cell.tile[Key.Oxygen] = 0f;
			foreach (var inf in food_influences) {
				cell.tile[Key.Food] += inf.x * inf.y;
			}
			foreach (var inf in temperature_influences) {
				cell.tile[Key.Temperature] += inf.x * inf.y;
			}
			foreach (var inf in moisture_influences) {
				cell.tile[Key.Moisture] += inf.x * inf.y;
			}
			foreach (var inf in oxygen_influences) {
				cell.tile[Key.Oxygen] += inf.x * inf.y;
			}
		}
	}

	public static Vector3 AxialToCube(Cartesian coord) {
		return new Vector3(coord.x, coord.y, -(coord.x + coord.y));
	}

	public static CellData GetCell(Cartesian coordination) {
		CellData result = null;
		GameManager._.mapManager.CellsData.TryGetValue(coordination, out result);
		return result;
	}

	public static Cartesian Offset(Cartesian origin, int offset_x, int offset_y) {
		return new Cartesian(origin.x + offset_x, origin.y + offset_y);
	}

	public static float GetDistance(Cartesian origin, Cartesian target) {
		Vector3 _origin = AxialToCube(origin);
		Vector3 _target = AxialToCube(target);
		return (Mathf.Abs(_origin.x - _target.x) + Mathf.Abs(_origin.y - _target.y) + Mathf.Abs(_origin.z - _target.z)) / 2;
	}

	public static Cartesian[] GetAdjacentCells(Cartesian coordination) {
		Cartesian[] adjacents = {
			Offset(coordination, 1, -1),
			Offset(coordination, 1, 0),
			Offset(coordination, 0, 1),
			Offset(coordination, -1, 1),
			Offset(coordination, -1, 0),
			Offset(coordination, 0, -1),
		};
		return adjacents;
	}

	public CellData[] GetAdjacentCellData(Cartesian coordination) {
		List<CellData> cellData = new List<CellData>();
		CellData temp;
		if (CellsData.TryGetValue(Offset(coordination, 1, -1), out temp))
			cellData.Add(temp);
		if (CellsData.TryGetValue(Offset(coordination, 1, 0), out temp))
			cellData.Add(temp);
		if (CellsData.TryGetValue(Offset(coordination, 0, 1), out temp))
			cellData.Add(temp);
		if (CellsData.TryGetValue(Offset(coordination, -1, 1), out temp))
			cellData.Add(temp);
		if (CellsData.TryGetValue(Offset(coordination, -1, 0), out temp))
			cellData.Add(temp);
		if (CellsData.TryGetValue(Offset(coordination, 0, -1), out temp))
			cellData.Add(temp);
		return cellData.ToArray();
	}
	/*
	public static CellData[] GetAdjacentCellsData(Vector2 coordination) {
		var cellsData = FindObjectOfType<MapManager>().cellsData;
		List<CellData> cellData = new List<CellData>();
		CellData temp;
		if (cellsData.TryGetValue(Offset(coordination, 1, -1), out temp))
			cellData.Add(temp);
		if (cellsData.TryGetValue(Offset(coordination, 1, 0), out temp))
			cellData.Add(temp);
		if (cellsData.TryGetValue(Offset(coordination, 0, 1), out temp))
			cellData.Add(temp);
		if (cellsData.TryGetValue(Offset(coordination, -1, 1), out temp))
			cellData.Add(temp);
		if (cellsData.TryGetValue(Offset(coordination, -1, 0), out temp))
			cellData.Add(temp);
		if (cellsData.TryGetValue(Offset(coordination, 0, -1), out temp))
			cellData.Add(temp);
		return cellData.ToArray();
	}*/

	public static float BlendInfluence(params float[] influ) {
		float screen_blending = 1;
		foreach (float inf in influ) {
			screen_blending *= (1f - inf);
		}
		return 1 - screen_blending;
	}
}

public class TileInfluenceData
{
	public Cartesian coordination;
	public float Value;
	public float Radius;
	public float STD {
		get { return Radius / 3; }
	}
	public float Influence(float distance) {
		return Mathf.Exp(- Mathf.Pow(distance/STD, 2) / 2) / (STD * Mathf.Sqrt(2 * Mathf.PI));
	}
}