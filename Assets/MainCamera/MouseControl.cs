﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public partial class GameManager : MonoBehaviour
{
	public RectTransform canvas;
	public float CanvasRatio { get { return Screen.width / canvas.sizeDelta.x; } }
	public Vector2 activePoint = new Vector2(2,2);
	public float pointerInactiveTime = 0;
	public static CellData selected = null;
	bool cursorChanged = false;
	[SerializeField]
	CellData SELECTED = selected;

	[Header("Game element Outline color")]
	public Color hoverColor;
	public Color clickColor;
	public Color selectedColor;

	[Header("Mouse Internal State")]
	public bool forceHideTooltip_OnMouseExitFlag;

	/// <summary>
	/// Called during GameManager Update() function
	/// </summary>
	void MouseSignalMonitor() {
		SELECTED = selected;
		UpdateAppearance();
		if (pointerData.delta != Vector2.zero) pointerInactiveTime = 0;
		if (pointerData.delta == Vector2.zero) pointerInactiveTime += Time.unscaledDeltaTime;

		if (Input.GetMouseButtonUp(0)) {
			CheckMouseSelecting();
		}
	}

	void UpdateAppearance() {
		bool Green = _ReproductionBinary || _ReproductionBudding || _MovingCell;

		if (!cursorChanged && (cameraController.IsMouseAtBoundariesID != 0 || Green)) { 
			cursorChanged = true;
		}
		else if (cursorChanged && !(cameraController.IsMouseAtBoundariesID != 0 || Green)) {
			Cursor.SetCursor(cameraController.cursors[9], activePoint, CursorMode.Auto);
			cursorChanged = false;
		}
		if (!cursorChanged) return;
		if (Green || HIGHLIGHT_ADJACENTS) {
			Cursor.SetCursor(cameraController.cursors[10], activePoint, CursorMode.Auto); return;
		}
		if (CurrentUI == UIState.CellScreen && selected &&
			selected.ContainsStatus("Developing Phase", "Developer Phase", "Immature Phase") &&
			pointerData.pointerEnter && pointerData.pointerEnter.tag == "UpgradeEntries") {
			Cursor.SetCursor(cameraController.cursors[11], activePoint, CursorMode.Auto); return;
		}
		
		else {
			var index = cameraController.IsMouseAtBoundariesID;
			switch ((int)index) {
				case 1:
					Cursor.SetCursor(cameraController.cursors[0], cameraController.hotspots[0], CursorMode.Auto);
					break;
				case 2:
					Cursor.SetCursor(cameraController.cursors[1], cameraController.hotspots[1], CursorMode.Auto);
					break;
				case 4:
					Cursor.SetCursor(cameraController.cursors[2], cameraController.hotspots[2], CursorMode.Auto);
					break;
				case 8:
					Cursor.SetCursor(cameraController.cursors[3], cameraController.hotspots[3], CursorMode.Auto);
					break;
				case 3:
					Cursor.SetCursor(cameraController.cursors[4], cameraController.hotspots[4], CursorMode.Auto);
					break;
				case 6:
					Cursor.SetCursor(cameraController.cursors[5], cameraController.hotspots[5], CursorMode.Auto);
					break;
				case 12:
					Cursor.SetCursor(cameraController.cursors[6], cameraController.hotspots[6], CursorMode.Auto);
					break;
				case 9:
					Cursor.SetCursor(cameraController.cursors[7], cameraController.hotspots[7], CursorMode.Auto);
					break;
				case 0:
					break;
				default:
					break;
			}
		}
	}

	//public void ClickHandler(CellData target) {
	//	SetSelectObject(target);
	//	//print("Clicked " + selected.name);
	//}

	public void CheckMouseSelecting() {
		var mouseHit = pointerData.pointerCurrentRaycast;
		var hitCell = (mouseHit.isValid && mouseHit.gameObject.tag == "Cells" ? mouseHit.gameObject.GetComponent<CellData>().HasCell : false);

		//Select from CellInteraction.cs
		//Unselect here
		if (!cameraController.dragged && !cameraController._UIBlock && !hitCell && selected) {
			if (_ReproductionBinary || _ReproductionBudding || _MovingCell) { return; }
			selected.SendMessage("Unselected");
			CameraLostCellFocus();
			selected = null;
		}

		if (!selected) {
			ExitReproductionMode();
		}
	}

	public void SetSelectObject(CellData cellData) {
		if (selected && selected != cellData) {
			selected.SendMessage("Unselected");
			if (!cellData.HasCell) CameraLostCellFocus();
		}
		if (!selected) { cameraController.SetUnfocusCamera(); }
		selected = cellData;
		CameraHasCellFocus();
	}

	void CameraHasCellFocus() {
		cameraController.SetFocus(selected.transform.position);
		if (cameraController.zoom > -32) {
			if (GameManager._.CurrentUI == UIState.PlayingScreen) {
				cameraController.SetZoom(7.5f);
			}
			SendMessage("PushUIState", UIState.CellScreen);
		}
	}

	public void CameraLostCellFocus() {
		cameraController.RestoreUnfocusCamera();
		SendMessage("PopUIState");
	}

	/// <summary>
	/// Handler for GameManager.cs
	/// </summary>
	void UnselectCurrentCell() {
		if (selected) {
			selected.SendMessage("Unselected");
			if (selected.HasCell) {
				CameraLostCellFocus();
			}
			selected = null;
		}
	}

	private void OnGUI() {
		var canvas = GameObject.FindGameObjectWithTag("UI Root").GetComponent<RectTransform>();
		var pointAt = GameManager._.pointerData.pointerCurrentRaycast.gameObject;
		var pointAtCell = pointAt ? pointAt.GetComponent<CellData>() : null;
		GUI.contentColor = Color.black;
		if (Global.IsShowingDebug) {
			Vector3 mpos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.gameObject.transform.position.z);
			GUI.Label(new Rect(68f, 0f, 400f, 60f), $"m pos: {Input.mousePosition} Inactive for: {pointerInactiveTime:0.00}");
			GUI.Label(new Rect(68f, 20f, 200f, 60f), "w pos: " + Camera.main.ScreenToWorldPoint(mpos));
			if (selected) {
				GUI.Label(new Rect(68f, 40f, 400f, 60f), "Selected Position: " + selected.coordination);
				GUI.Label(new Rect(68f, 60f, 400f, 60f), "Selected Adjacents: " + string.Join(" ", selected.adjacents.Select(c => c.coordination)));
				var adjHeight = 60*(selected.adjacents.Count + 1);
				string adjData = string.Join("\n", selected.adjacents.Select(adj => $"{adj.coordination}{adj.owner.NAME} {adj.interaction.overriden}"));
				GUI.Label(new Rect(68f, 80f, 400f, adjHeight), $"Adjacent Data\n" + adjData);
				var queueHeight = 60 * (selected.queueOfUpgrade.Count);
				string qData = string.Join("\n", selected.queueOfUpgrade.Select(q => $"{q} {selected.progressOfUpgrades[q]}"));
				GUI.Label(new Rect(68f, 80f + adjHeight, 400f, queueHeight), "<b>Queue Data</b>\n" + qData);
			}
			GUI.Label(new Rect(400f, 0f, 400f, 60f), $"Canvas: {canvas.sizeDelta} Screen: {Screen.width}, {Screen.height} Scale: {Screen.width/canvas.sizeDelta.x} vs {canvas.gameObject.GetComponent<UnityEngine.UI.CanvasScaler>().scaleFactor}");
			GUI.Label(new Rect(400f, 20f, 400f, 60f), "Camera Position: " + transform.position);
			GUI.Label(new Rect(400f, 40f, 400f, 60f), pointAt == null ? "NONE": pointAt.name + " " + pointAt.tag);
			if (pointAtCell) {
				GUI.Label(new Rect(400f, 60f, 400f, 60f), "Selected Adjacents: " + string.Join(" ", pointAtCell.adjacents.Select(c => c.coordination)));
			}
		}
	}
}
