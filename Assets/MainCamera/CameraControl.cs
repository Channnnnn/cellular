﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraControl : MonoBehaviour
{
	[Header("Options")]
	public bool enableZoomWhenFocus = true;
	public bool enableCenterItemsWhenFocus = true;

	public float lerpSpeed = 5f;
	public Vector2 zoomRange;
	public Vector2 ZoomRange { get { return zoomRange; } set { zoomRange = value; } }
	public Vector2 HorizontalPanRange;
	public Vector2 VerticalPanRange;

	[Header("Screen Scroll")]
	public Texture2D[] cursors;
	public Vector2[] hotspots;
	public int boundSize = 15;
	public float dragThreshold;

	[Header("Relative Speed to Zoom")]
	public Vector2 dragPanMultiplier;
	public Vector2 boundPanMultiplier;
	public Vector2 keyPanMultiplier;
	public Vector2 zoomStepMultiplier;

	public Vector3 unfocusCamera;
	Vector2 center;
	[SerializeField]
	public float zoom;
	[SerializeField]
	public bool dragged = false;
	public bool _UIBlock = false;
	
	// Use this for initialization
	void Start() {
		zoom = transform.position.z;
		dragPanMultiplier = new Vector2(20f, 0f);
		boundPanMultiplier = new Vector2(1.5f, 0f);
		zoomStepMultiplier = new Vector2(1f, 0f);
		//keyPanMultiplier = new Vector2(1f, 0f);
		center = new Vector2(transform.position.x, transform.position.y);
		unfocusCamera = transform.position;
	}

	// Update is called once per frame
	void Update() {
		dragPanMultiplier.y = dragPanMultiplier.x / -zoom;
		boundPanMultiplier.y = boundPanMultiplier.x * -zoom;
		zoomStepMultiplier.y = zoomStepMultiplier.x * -zoom;
		keyPanMultiplier.y = keyPanMultiplier.x * -zoom;
		if (_UIBlock) {
			return;
		}
		else {
			if (Input.GetButton("Back")) {
				dragged = dragged || (Mathf.Abs(Input.GetAxisRaw("Mouse X")) > dragThreshold) || (Mathf.Abs(Input.GetAxisRaw("Mouse Y")) > dragThreshold);
			}
			if (dragged && Input.GetButtonUp("Back")) {
        DoAfter(0.1f, delegate { dragged = false; });
			}
			if (!dragged && Input.GetButtonUp("Back") && !GameManager.AtUI(UIState.CellScreen)) {
				Vector3 clickPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, -transform.position.z);
				center = Camera.main.ScreenToWorldPoint(clickPosition);
			}
			if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) {
				center.x += Input.GetAxisRaw("Horizontal") * keyPanMultiplier.y * Time.unscaledDeltaTime;
				center.y += Input.GetAxisRaw("Vertical") * keyPanMultiplier.y * Time.unscaledDeltaTime;
			}
			if (Input.GetAxis("Mouse ScrollWheel") != 0 && !IsMouseLeftScreen) {
				zoom += Input.GetAxis("Mouse ScrollWheel") * zoomStepMultiplier.y;
				zoom = Mathf.Clamp(zoom, zoomRange.y, zoomRange.x);
			}
			if (Input.GetAxis("Key Zoom") != 0) {
				zoom += Input.GetAxisRaw("Key Zoom") * zoomStepMultiplier.y * Time.unscaledDeltaTime * 3f;
				zoom = Mathf.Clamp(zoom, zoomRange.y, zoomRange.x);
			}
			//UpdateCursorAppearance();
			CheckScreenScroll();
			center.x = Mathf.Clamp(center.x, HorizontalPanRange.x, HorizontalPanRange.y);
			center.y = Mathf.Clamp(center.y, VerticalPanRange.x, VerticalPanRange.y);
		}
	}

	void LateUpdate() {
		if (dragged) {
			center.x -= Input.GetAxisRaw("Mouse X") / dragPanMultiplier.y;
			center.y -= Input.GetAxisRaw("Mouse Y") / dragPanMultiplier.y;
		}
		var adjustZ = Mathf.Lerp(transform.position.z, zoom, Time.unscaledDeltaTime * lerpSpeed);
		var adjustX = Mathf.Lerp(transform.position.x, center.x, Time.unscaledDeltaTime * lerpSpeed);
		var adjustY = Mathf.Lerp(transform.position.y, center.y, Time.unscaledDeltaTime * lerpSpeed);
		transform.position = new Vector3(adjustX, adjustY, adjustZ);
	}

	public Vector4 IsMouseAtBoundaries {
		get {
			Vector4 bound = new Vector4(0, 0, 0, 0);
			if (Input.mousePosition.x < boundSize && Input.mousePosition.x >= 0) bound.w = 1;
			if (Input.mousePosition.x > Screen.width - boundSize && Input.mousePosition.x <= Screen.width) bound.y = 1;
			if (Input.mousePosition.y < boundSize && Input.mousePosition.y >= 0) bound.z = 1;
			if (Input.mousePosition.y > Screen.height - boundSize && Input.mousePosition.y <= Screen.height) bound.x = 1;
			return bound;
		}
	}

	public float IsMouseAtBoundariesID {
		get { return IsMouseAtBoundaries.x * 1 + IsMouseAtBoundaries.y * 2 + IsMouseAtBoundaries.z * 4 + IsMouseAtBoundaries.w * 8; }
	}

	bool IsMouseLeftScreen {
		get {
			Vector4 bound = new Vector4(0, 0, 0, 0);
			if (Input.mousePosition.x < 0) bound.w = 1;
			if (Input.mousePosition.x > Screen.width) bound.y = 1;
			if (Input.mousePosition.y < 0) bound.z = 1;
			if (Input.mousePosition.y > Screen.height) bound.x = 1;
			return bound != Vector4.zero;
		}
	}

	void CheckScreenScroll() {
		if (IsMouseAtBoundaries != Vector4.zero) {
			if (IsMouseAtBoundaries.x == 1) {
				center.y += boundPanMultiplier.y * Time.unscaledDeltaTime;
			}
			if (IsMouseAtBoundaries.y == 1) {
				center.x += boundPanMultiplier.y * Time.unscaledDeltaTime;
			}
			if (IsMouseAtBoundaries.z == 1) {
				center.y -= boundPanMultiplier.y * Time.unscaledDeltaTime;
			}
			if (IsMouseAtBoundaries.w == 1) {
				center.x -= boundPanMultiplier.y * Time.unscaledDeltaTime;
			}
		}
	}

	public void SetFocus(Vector2 position) {
		if (enableCenterItemsWhenFocus) {
			center = position;
		}
	}
	public void SetZoom(float value) {
		if (enableZoomWhenFocus) {
			if (value > 0) value = -value;
			zoom = value;
		}
	}
	public void SetUnfocusCamera() {
		if (enableCenterItemsWhenFocus) {
			unfocusCamera.x = center.x;
			unfocusCamera.y = center.y;
		}
		if (enableZoomWhenFocus) {
			unfocusCamera.z = zoom;
		}
	}
	public void RestoreUnfocusCamera() {
		if (enableZoomWhenFocus) {
			zoom = unfocusCamera.z;
		}
		//center.x = unfocusCamera.x;
		//center.y = unfocusCamera.y;
	}

	public void SetUIBlocking() {
		_UIBlock = true;
	}

	public void UnsetUIBlocking(float seconds) {
		if (seconds == 0) {
			_UIBlock = false;
			return;
		}
    DoAfter(seconds, delegate { _UIBlock = false; });
	}

  public void DoAfter(float seconds, System.Action action) {
    StartCoroutine(Delay(seconds, action));
  }

  IEnumerator Delay(float second, System.Action action) {
    yield return new WaitForSecondsRealtime(second);
    action();
  }

	private void OnGUI() {
		GUI.contentColor = Color.black;
		if (Global.IsShowingDebug) {
			GUI.Label(new Rect(68f, 60f, 200f, 60f), "dragged: " + dragged);
			GUI.Label(new Rect(68f, 70f, 200f, 60f), "blocked: " + _UIBlock);
		}
	}
}
