﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public enum AttributeAppearance { Keyword, Symbol }
public enum NumericalAppearance { Unsigned, Signed }
public enum UIState
{
	WelcomeScreen, PlayingScreen, EscapeMenu, CellScreen, TraitsScreen
}
public partial class GameManager : MonoBehaviour
{
#region Singleton Initialisation
	private static GameManager _instance = null;
	public static GameManager _ {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<GameManager>() as GameManager;
			}
			return _instance;
		}
	}
	private void OnApplicationQuit() {
		_instance = null;
	}
	#endregion

	public AudioSource menuBGM;
	public AudioSource gameBGM;
	public AudioClip[] BGM;

	public enum GameSpeed	{	Pause, Normal, Double, Triple	}
	public static bool __CheatSPAWNCELL = false;
	public static bool _ReproductionBudding = false;
	public static bool _ReproductionBinary = false;
	public static bool _MovingCell = false;
	public static bool HIGHLIGHT_ADJACENTS = false;
	public bool BUDDING_MODE;
	public bool BINARY_MODE;
	public bool MOVING_CELL;
	public bool gameOver;

	[Header("Settings")]
	public Gradient ambientLight;
	public Gradient gridColor;
	public Vector3 timeScale = new Vector3(1f, 3f, 6f);

	public float foodDensity;
	public Vector4 foodParameters;
	public float temperatureVariance;
	public Vector4 temperatureParameters;
	public float moistureDensity;
	public Vector4 moistureParameters;
	public float oxygenDensity;
	public Vector4 oxygenParameter;
	public Gradient foodOverlayColor;
	public Gradient temperatureOverlayColor;
	public Gradient moistureOverlayColor;
	public Gradient oxygenOverlayColor;
	[Header("Game Time")]
	[SerializeField]
	float currentGameTime;

	public static float lengthOfCycle = 5f;
	public static float StartTime;
	public static float CurrentElapsedTime { get { return Time.time - StartTime; } }
	public static float CycleCount {	get {	return CurrentElapsedTime / lengthOfCycle; }	}
	public static float SubcycleCount { get { return CurrentElapsedTime % lengthOfCycle; } }
	public static float SubcycleFraction { get { return (CurrentElapsedTime % lengthOfCycle) / lengthOfCycle; }}
	public static float DeltaCycle { get { return Time.deltaTime / lengthOfCycle; } }

	public static string GetTime { get { return $"[{(int)CycleCount}:{(int)SubcycleCount}'{(int)lengthOfCycle}]"; } }

	//OutlineEffect outlineEffect;
	[Header("Internal State")]
	public UI UImanager;
	public CameraControl cameraController;
	public PointerEventData pointerData;
	public MapManager mapManager;
	public UpgradeHandler upgradeHandler;
	public FocusHandler focusHandler;
	public TraitsReference TRAITS_REFERENCE;
	public TutorialsScript tutorials;

	public float GlobalTemperatureModifier = 0f;
	public float start_time = 0f;
	public float GlobalTemperatureMultiplier = 0f;
	public float end_time = 0f;

	[SerializeField]
	GameSpeed[] playSpeed = { GameSpeed.Normal, GameSpeed.Pause };
	public GameSpeed PlaySpeed { get { return playSpeed[0]; } }

	public Stack<UIState> displayUIStack = new Stack<UIState>();
	public UIState CurrentUI {  get { return displayUIStack.Peek(); } }
	public static bool AtUI(UIState name) { return _.CurrentUI == name; }
	public UIState[] UIStateList = { };
	[SerializeField]
	public List<Player> Players { get; set; } = new List<Player>();

	public Player GetPlayer { get { return Players[0].TYPE == Player.Type.Human ? Players[0] : Players.FirstOrDefault(p => p.TYPE == Player.Type.Human); } }
	public static bool IsGameOver { get { return _.GetPlayer.CellCount <= 0; } }
	public static bool IsSpectating = false;

	void Start() {
		//if (!_UImanager) _UImanager = GameObject.FindGameObjectWithTag("UI Root").GetComponent<UI>();
		//cameraController = Camera.main.GetComponent<CameraControl>();
		//PlaySpeed = GameSpeed.Normal;
		//PushUIState(UIState.PlayingScreen);
		//pointerData = ExtendedStandaloneInputModule.GetPointerEventData();
		LateStart();
	}

	public void LateStart() {
		//if (!_UImanager) _UImanager = GameObject.FindGameObjectWithTag("UI Root").GetComponent<UI>();
		//if (!mapManager) mapManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<MapManager>();
		cameraController = Camera.main.GetComponent<CameraControl>();
		pointerData = ExtendedStandaloneInputModule.GetPointerEventData();
		//_UImanager.cellDataPanel.gameObject.SetActive(false);

		PushUIState(UIState.WelcomeScreen);
		UImanager.announcer.text = "";
		//mapManager.LateStart();
	}

	public void StartGame(float gameLength, int _playerColor, Cartesian mapDimensions, MapManager.MapType mapType, Color32[] colors) {
		print("STARTED GAME");
		Players.Clear();
		Players.Add(new Player("HUMAN", Player.Type.Human, colors[_playerColor]));

		for (int i = 0; i < (int)(mapDimensions.x * (float)mapDimensions.y * 0.3); i++) {
			int c = Random.Range(0, colors.Length);
			byte h = (byte)Random.Range(-128, 128);
			byte s = (byte)Random.Range(-64, 64);
			byte v = (byte)Random.Range(-32, 32);
			Players.Add(new Player("AI PLAYER " + i, Player.Type.AI, ((Color)colors[c]).Alter(h,s,v)));
		}

		lengthOfCycle = gameLength;
		mapManager.mapDimension = mapDimensions;
		mapManager.mapType = mapType;

		DestroyGameData();
		mapManager.LateStart(Players);
		PushUIState(UIState.PlayingScreen);

		UImanager.setupPanel.gameObject.SetActive(false);
		UImanager.INGAME_UI_GROUP.SetActive(true);
		UImanager.MENU_UI_GROUP.SetActive(false);
	}

	public void RestartGame() {
		DestroyGameData();
		mapManager.RestartGame(Players);
	}

	public void DestroyGameData() {
		StartTime = Time.time;
		gameOver = false;
		IsSpectating = false;

		foreach (Player player in Players) {
			player.ResetPlayerCells();
		}
		SetGameSpeed(1);
	}

	public static void SaveGame() {
		//string Date = $"{System.DateTime.Today.Year}-{System.DateTime.Today.Month}-{System.DateTime.Today.Day}";
		//string Time = $"{System.DateTime.Now.TimeOfDay : t}";
		//string Dir = Application.persistentDataPath + $"/Saved/Save-{Date}-{Time}.sav";
		string Dir = Application.persistentDataPath + "/Saved/";
		string Save = "save1.sav";
		BinaryFormatter bf = new BinaryFormatter();
		if (!Directory.Exists(Dir)) Directory.CreateDirectory(Dir);
		FileStream stream = new FileStream(Dir+Save, FileMode.Create);

		GameSaveInfo save = new GameSaveInfo(_);
		bf.Serialize(stream, save);
		stream.Close();
		print("Saved at " + Dir + Save);
	}
	public static void LoadGame() {
		string Dir = Application.persistentDataPath + "/Saved/save1.sav";
		if (File.Exists(Dir)) {
			BinaryFormatter bf = new BinaryFormatter();
			using (FileStream stream = new FileStream(Dir, FileMode.Open)) {
				GameSaveInfo save = bf.Deserialize(stream) as GameSaveInfo;
				save.Unpack();
			}			
		}
		else {
			print("File doesn't exist!");
		}
	}

	int lastSubcycle = 0, lastCycle = 0;
	public static bool __SubcycleTick { get; private set; }
	public static bool __CycleTick { get; private set; }
	void UpdateTick() {
		__SubcycleTick = lastSubcycle != (int)SubcycleCount;
		__CycleTick = lastCycle != (int)CycleCount;
		lastSubcycle += ((int)SubcycleCount - lastSubcycle);
		lastCycle += ((int)CycleCount - lastCycle);

		//if (__SubcycleTick) print($"End of subcycle {lastSubcycle}");
		//if (__CycleTick) print($"End of cycle {lastCycle}");

		//if (lastSubcycle != (int)SubcycleCount) { print($"End of subcycle {lastSubcycle} {__SubcycleTick}"); lastSubcycle = (int)SubcycleCount; __SubcycleTick = true; }
		//else if (lastSubcycle == (int)SubcycleCount) { __SubcycleTick = false; }

		//if (lastCycle != (int)CycleCount) { print($"End of cycle {lastCycle} {__CycleTick}"); lastCycle = (int)CycleCount; __CycleTick = true; }
		//else if (lastCycle == (int)CycleCount) { __CycleTick = false; }
	}

	float menuVelocity = 0.0f;
	float gameVelocity = 0.0f;
	float menuVolume;
	float gameVolume;
	// Update is called once per frame
	void Update() {
		if (CurrentUI == UIState.WelcomeScreen) {
			if (!menuBGM.isPlaying) menuBGM.PlayDelayed(1f);
			menuVolume = 0.6f;
			gameVolume = 0f;
			if (gameBGM.isPlaying && gameBGM.volume == 0f) gameBGM.Stop();
		}
		else if (CurrentUI >= UIState.PlayingScreen) {
			if (!gameBGM.isPlaying) gameBGM.PlayDelayed(1f);
			menuVolume = -1f;
			gameVolume = 0.5f;
			if (menuBGM.isPlaying && menuBGM.volume == 0f) menuBGM.Stop();
		}

		menuBGM.volume = Mathf.SmoothDamp(menuBGM.volume, menuVolume, ref menuVelocity, 3f, 0.5f, Time.unscaledDeltaTime);
		gameBGM.volume = Mathf.SmoothDamp(gameBGM.volume, gameVolume, ref gameVelocity, 1f, 0.5f, Time.unscaledDeltaTime);

		UpdateTick();

		BUDDING_MODE = _ReproductionBudding;
		BINARY_MODE = _ReproductionBinary;
		MOVING_CELL = _MovingCell;
		if (CurrentUI >= UIState.PlayingScreen) {
			Players.Where(p => p.CellCount == 0).ForEach(player =>
			{
				if (CycleCount % 50 < 0.25f && player.TYPE != Player.Type.Human) {
					var respawnplayer = Players.Where(p => p.CellCount == 0).Count();
					var cells = Players.Where(p => p.CellCount > 0).Sum(p => p.CellCount);
					if (Random.Range(0, 1) < (respawnplayer / cells)) mapManager.RandomPlayerCellSpawn(mapManager.CellsData.Values.ToList(), player);
				}
			});

			float rand = Random.Range(0f, 1f);
			if (GlobalTemperatureModifier == 0 && ((int)CycleCount + 1) % 3f == 0 && rand > 0.8) {
				int randd = Random.Range(0, 3);
				float[] temperatures = { -5, 5, -2 };
				GlobalTemperatureModifier = _Random.Normal(temperatures[randd], 3);
				start_time = CycleCount;
				end_time = CycleCount + Random.Range(5f, 7f);
				UImanager.announcer.text = $"Climate Event: Temperature changed by {GlobalTemperatureModifier.Floorf(2)}<sup>o</sup>C " +
					$"until around cycle {(int)end_time}.";
			}
			if (CycleCount > start_time && CycleCount < end_time) {
				float range = end_time - start_time;
				float progress = CycleCount - start_time;
				float value = progress / range < 0.5 ? (progress / range) * 2f : 1f - (((progress / range) - 0.5f) * 2f);
				GlobalTemperatureMultiplier = value;
			}
			if (GlobalTemperatureModifier != 0 && CycleCount > end_time) {
				GlobalTemperatureModifier = 0;
				UImanager.announcer.text = $"Climate Event ended";
			}
			if (CycleCount > end_time + 0.25f) UImanager.announcer.text = "";

			currentGameTime = CurrentElapsedTime /*+ Last saved time*/;
			/** Implemented in MouseControl.cs **/
			MouseSignalMonitor();

			if (Input.GetKeyDown(KeyCode.F2)) {
				Global.ToggleDebug();
			}
#if UNITY_EDITOR
			if (Input.GetKeyDown(KeyCode.F3)) {
				mapManager.ReseedTileProperties();
			}
#endif
			if (Input.GetKeyDown(KeyCode.C)) {
				cameraController.SetFocus(GetPlayer.MedianPosition);
			}

			if (Input.GetKeyDown(KeyCode.F5)) {
				if (Global.IsShowingOverlay && Global.CurrentOverlay == 0) Global.SetOverlay(false);
				else if (!Global.IsShowingOverlay) Global.SetOverlay(true);
				Global.SetOverlay(0);
			}
			if (Input.GetKeyDown(KeyCode.F6)) {
				if (Global.IsShowingOverlay && Global.CurrentOverlay == 1) Global.SetOverlay(false);
				else if (!Global.IsShowingOverlay) Global.SetOverlay(true);
				Global.SetOverlay(1);
			}
			if (Input.GetKeyDown(KeyCode.F7)) {
				if (Global.IsShowingOverlay && Global.CurrentOverlay == 2) Global.SetOverlay(false);
				else if (!Global.IsShowingOverlay) Global.SetOverlay(true);
				Global.SetOverlay(2);
			}
			if (Input.GetKeyDown(KeyCode.F8)) {
				if (Global.IsShowingOverlay && Global.CurrentOverlay == 3) Global.SetOverlay(false);
				else if (!Global.IsShowingOverlay) Global.SetOverlay(true);
				Global.SetOverlay(3);
			}
			if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftShift) && Input.GetKeyUp(KeyCode.Z)) {
				__CheatSPAWNCELL = !__CheatSPAWNCELL;
			}
			if (Input.GetKeyDown(KeyCode.O)) {
				SaveGame();
			}
			if (Input.GetKeyDown(KeyCode.L)) {
				LoadGame();
			}
			if (Input.GetKeyDown(KeyCode.M)) {
				upgradeHandler.OnMoveToggled(!_MovingCell);
			}
			if (Input.GetKeyDown(KeyCode.B)) {
				if (upgradeHandler.buddingButton.gameObject.activeSelf) {
					upgradeHandler.OnBuddingToggled(!_ReproductionBudding);
				}
				else if (upgradeHandler.binaryButton.gameObject.activeSelf) {
					upgradeHandler.OnBinaryToggled(!_ReproductionBinary);
				}
			}
			if (Input.GetKeyDown(KeyCode.T)) {
				UImanager.OnClickTraitsScreenButton();
			}
#if UNITY_EDITOR
			if (Input.GetKeyDown(KeyCode.Keypad1) && selected) {
				if (!selected.statusModifier.Contains("Energy Enhancer")) selected.AddStatus("Energy Enhancer");
				else selected.RemoveStatus("Energy Enhancer");
			}
			if (Input.GetKeyDown(KeyCode.Keypad2) && selected) {
				if (!selected.statusModifier.Contains("Breacher I")) selected.AddStatus("Breacher I");
				else selected.RemoveStatus("Breacher I");
			}
			if (Input.GetKeyDown(KeyCode.Keypad3) && selected) {
				if (!selected.statusModifier.Contains("Recovering")) selected.AddStatus("Recovering");
				else selected.RemoveStatus("Recovering");
			}
#endif
			if (!UImanager.menuOverlay.gameObject.activeSelf) {
				if (Input.GetKeyDown(KeyCode.Alpha1)) {
					SetGameSpeed(1);
				}
				if (Input.GetKeyDown(KeyCode.Alpha2)) {
					SetGameSpeed(2);
				}
				if (Input.GetKeyDown(KeyCode.Alpha3)) {
					SetGameSpeed(3);
				}
				if (Input.GetKeyDown(KeyCode.Space)) {
					SetGameSpeed(0);
				}
			}
			if (Input.GetButtonDown("Cancel")) {
				EscapeKeyHandler();
			}

			//outlineEffect.lineColor0 = outlineEffect.lineColor1 = outlineEffect.lineColor2 = gridColor.Evaluate(SubcycleFraction);
			Camera.main.backgroundColor = ambientLight.Evaluate(SubcycleFraction);

			Players.ForEach(player => { if (player.TYPE == Player.Type.AI) player.DoAction(); });

			if (IsGameOver && !IsSpectating && !UImanager.gameOverDialogue.gameObject.activeSelf) {
				StartCoroutine(GameEndingSequence("NO_CELL_LEFT", 1f));
			}
		}
	}

	IEnumerator GameEndingSequence(string type, float delayBeforeExecute = 0f) {
		yield return new WaitForSecondsRealtime(delayBeforeExecute);
		print("GAME OVER");
		if (IsGameOver && !IsSpectating && type == "NO_CELL_LEFT") {
			EscapeMenuPause(true);
			UImanager.OnGameOver("All of your cell has died.");
			IsSpectating = true;
		}
	}

	//public void EnterReproductionMode(string mode, bool isOn) {
	//	if (mode == "Budding") {
	//		if (isOn) {
	//			_ReproductionBudding = true;
	//			_ReproductionBinary = false;
	//		}
	//		else {
	//			ExitReproductionMode();
	//		}
	//	}
	//	if (mode == "Binary") {
	//		if (isOn) {
	//			_ReproductionBudding = false;
	//			_ReproductionBinary = true;
	//		}
	//		else {
	//			ExitReproductionMode();
	//		}
	//	}
	//}

	public void EnterBuddingMode() {
		_ReproductionBudding = true;
		_ReproductionBinary = false;
	}
	public void EnterBinaryMode() {
		_ReproductionBudding = false;
		_ReproductionBinary = true;
	}

	public void ExitReproductionMode() {
		if (CurrentUI == UIState.CellScreen) {
			/**/
			upgradeHandler.buddingButton.isOn = false;
			/**/
			upgradeHandler.binaryButton.isOn = false;
		}
		_ReproductionBudding = false;
		_ReproductionBinary = false;
		Cursor.SetCursor(cameraController.cursors[9], activePoint, CursorMode.Auto);
	}

	public void ExitMoveCellMode() {
		upgradeHandler.moveButton.isOn = false;
		Cursor.SetCursor(cameraController.cursors[9], activePoint, CursorMode.Auto);
	}

	public void EscapeKeyHandler() {
		if (CurrentUI == UIState.TraitsScreen) {
			UImanager.OnClickTraitsScreenButton();
			return;
		}
		PauseMenuHandler();
	}

	public void PauseMenuHandler() {
		bool isEscapeMenuActive = (CurrentUI == UIState.EscapeMenu);
		EscapeMenuPause(!isEscapeMenuActive);
		if (!isEscapeMenuActive) { PushUIState(UIState.EscapeMenu); }
		else { PopUIState(); }
	}

	public int SetGameSpeed(int speed) {
		if (speed <= 0 || PlaySpeed == GameSpeed.Pause) TogglePlayState(speed < 0 ? true : false);
		if (speed > 0) { playSpeed[0] = (GameSpeed)speed; }

		if (PlaySpeed == GameSpeed.Normal) {
			print("1x Speed");
			Time.timeScale = timeScale.x;
		}
		else if (PlaySpeed == GameSpeed.Double) {
			print("2x Speed");
			Time.timeScale = timeScale.y;
		}
		else if (PlaySpeed == GameSpeed.Triple) {
			print("3x Speed");
			Time.timeScale = timeScale.z;
		}
		else if (PlaySpeed == GameSpeed.Pause) {
			print("Paused");
			Time.timeScale = 0;
		}
		UImanager.GameSpeedButtonHighlight((int)playSpeed[0]);
		return (int)PlaySpeed;
	}

	void TogglePlayState(bool enforcePause = false) {
		if (enforcePause && PlaySpeed == GameSpeed.Pause) {
			return;
		}
		GameSpeed p = playSpeed[0];
		playSpeed[0] = playSpeed[1];
		playSpeed[1] = p;
	}

	void EscapeMenuPause(bool value) {
		if (value) {
			print("Escape Menu Opened");
			Time.timeScale = 0;
		}
		else {
			print("Escape Menu Closed");
			switch (PlaySpeed) {
				case GameSpeed.Normal:
					Time.timeScale = timeScale.x;
					break;
				case GameSpeed.Double:
					Time.timeScale = timeScale.y;
					break;
				case GameSpeed.Triple:
					Time.timeScale = timeScale.z;
					break;
				default: break;
			}
		}
	}

	public void PopUIStateUntilLast() {
		while(CurrentUI != UIState.PlayingScreen && displayUIStack.Count > 0) {
			PopUIState();
		}
	}
	public void PushUIState(UIState state) {
		if (displayUIStack.Count > 0 && CurrentUI == state) return;

		print("Currently At:" + state.ToString());
		displayUIStack.Push(state);
		UIStateList = displayUIStack.ToArray();

		if (state != UIState.PlayingScreen) {
			if (state == UIState.CellScreen) { upgradeHandler.isShowing = false; }
			UIStatePair.GetValue(state).SetActive(true);
		}
	}
	public void PopUIState() {
		if (CurrentUI == UIState.WelcomeScreen) return;
		UIState state = displayUIStack.Pop();
		UIStateList = displayUIStack.ToArray();
		if (state != UIState.PlayingScreen) {
			if (state == UIState.CellScreen) { upgradeHandler.isShowing = true; return; }
			UIStatePair.GetValue(state).SetActive(false);
		}
	}
	public void PurgeUIState() {
		while(displayUIStack.Count > 1) {
			PopUIState();
		}
		UIStateList = displayUIStack.ToArray();
		UImanager.INGAME_UI_GROUP.SetActive(false);
		UImanager.MENU_UI_GROUP.SetActive(true);
	}

	public void OnReturnToWelcomeScreen() {
		PurgeUIState();
		SetGameSpeed(0);
		UImanager.welcomeMenu.gameObject.SetActive(true);
	}
}

public static class Global
{
	public static bool IsShowingDebug { get; private set; } = false;
	public static bool IsShowingOverlay { get; private set; } = true;
	public static int CurrentOverlay { get; private set; } = 0;
	public static void ToggleDebug() {
		IsShowingDebug = !IsShowingDebug;
	}
	public static void SetOverlay(bool value) {
		IsShowingOverlay = value;
	}
	public static void SetOverlay(int id) {
		CurrentOverlay = id;
	}
}

public static class _Random
{
	public static float Normal(float mean = 0, float variance = 1) {
		float u1 = 1f - UnityEngine.Random.Range(0f, 1f);
		float u2 = 1f - UnityEngine.Random.Range(0f, 1f);
		float NormalDistb = Mathf.Sqrt(-2f * Mathf.Log(u1)) * Mathf.Sin(2f * Mathf.PI * u2);
		return mean + variance * NormalDistb;
	}
}

public static class Extension
{
	public static void ForEach<T>(this IEnumerable<T> set, System.Action<T> action) {	foreach(var item in set) { action(item); } }

	public static float Abs(this float value) {
		return Mathf.Abs(value);
	}

	public static float RangeDifference(this float value, float lower_inclusive, float upper_inclusive) {
		if (value > upper_inclusive) return value - upper_inclusive;
		else if (value < lower_inclusive) return value - lower_inclusive;
		else return 0f;
	}

	public static float Clamp(this float value, float min, float max) {
		return Mathf.Clamp(value, min, max);
	} 
	public static float Map(this float value, float current_min, float current_max, float target_min, float target_max) {
		return Mathf.Clamp((value - current_min) / (current_max - current_min) * (target_max - target_min) + target_min, target_min, target_max);
	}
	public static float Map01(this float value, float current_min, float current_max) {
		return Mathf.Clamp01((value - current_min) / (current_max - current_min));
	}

	public static string Floor(this float value, int points = 0) {
		return value.Floorf(points).ToString($"F{points}");
	}
	/// <summary>
	/// Return signed string of number
	/// </summary>
	/// <param name="value"></param>
	/// <param name="points">Decimal points of a number</param>
	/// <returns></returns>
	public static string FloorS(this float value, int points = 0) {
		return (value >= 0 ? "+" : "") + value.Floor(points);
	}
	public static string FloorfS(this float value, int points = 0) {
		return (value >= 0 ? "+" : "") + value.Floorf(points);
	}
	public static float Floorf(this float value, int points = 0) {
		float decimals = Mathf.Pow(10, points);
		return (Mathf.Floor(value * decimals) / decimals);
	}

	public static string ToPercent(this float value, int points = 0) {
		return (value * 100f).Floor(points) + "%";
	}
	public static string ToPercentS(this float value, int points = 0) {
		return (value >= 0 ? "+" : "") + (value * 100f).Floor(points) + "%";
	}
	public static string ToPercentfS(this float value, int points = 0) {
		return (value >= 0 ? "+" : "") + (value * 100f).Floorf(points) + "%";
	}
	public static float ToPercentf(this float value, int points = 0) {
		return (value * 100f).Floorf(points);
	}

	public static TValue Get<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key) {
		TValue value;
		bool result = dict.TryGetValue(key, out value);
		if (result) return value;
		else return value;
	}

	public static float AdditiveInsert(this Dictionary<string, float> dict, string key, float value) {
		if (dict.ContainsKey(key)) { dict[key] += value; }
		else { dict[key] = value; }
		return dict[key];
	}

	public static float Getf(this Dictionary<string, float> dict, string key) {
		float value;
		bool result = dict.TryGetValue(key, out value);
		if (result) return value;
		else {
			if (key.ContainsAny(Key.Multiplier)) return 1f;
			return value;
		}
	}

	public static bool ContainsAny(this string value, params string[] substrings) {
		return substrings.Any(value.Contains);
	}
	public static bool ContainsAny<T>(this T value, params T[] collection) {
		return collection.Any(c => value.Equals(c));
	}
}

[System.Serializable]
public class GameSaveInfo
{
	public bool GameOver { get; set; }
	public List<SerializablePlayer> Players { get; set; }
	public MapManager.MapType MapType { get; set; }
	public Cartesian MapDimensions { get; set; }
	public Dictionary<Cartesian, SerializableCell> MapTiles { get; set; } = new Dictionary<Cartesian, SerializableCell>();
	public float CycleLength { get; set; }
	public float ElapsedSeconds { get; set; }
	public SerializableVector3 CameraPosition { get; set; }
	public SerializableVector3 CameraHorizontalPanRange { get; set; }
	public SerializableVector3 CameraVerticalPanRange { get; set; }
	public GameSaveInfo(GameManager gameManager) {
		GameOver = gameManager.gameOver;
		Players = new List<SerializablePlayer>();
		foreach (Player p in gameManager.Players) {
			Players.Add(new SerializablePlayer(p));
		}
		MapType = gameManager.mapManager.mapType;
		MapDimensions = gameManager.mapManager.mapDimension;
		foreach(var cell in gameManager.mapManager.CellsData) {
			MapTiles[cell.Key] = new SerializableCell(cell.Value);
		}
		CycleLength = GameManager.lengthOfCycle;
		ElapsedSeconds = GameManager.CurrentElapsedTime;
		CameraPosition = new SerializableVector3(gameManager.cameraController.transform.position);
		CameraHorizontalPanRange = new SerializableVector3(gameManager.cameraController.HorizontalPanRange);
		CameraVerticalPanRange = new SerializableVector3(gameManager.cameraController.VerticalPanRange);
	}

	public void Unpack() {
		GameManager._.gameOver = GameOver;
		GameManager._.mapManager.CellsData = new Dictionary<Cartesian, CellData>();
		GameManager._.mapManager.RestoreMap(MapTiles);
		GameManager._.mapManager.mapType = MapType;
		GameManager._.mapManager.mapDimension = MapDimensions;
		
		GameManager.lengthOfCycle = CycleLength;
		GameManager.StartTime = Time.time - ElapsedSeconds;
		GameManager._.cameraController.transform.position = CameraPosition.Vectorize;
		GameManager._.cameraController.HorizontalPanRange = CameraHorizontalPanRange.Vectorize;
		GameManager._.cameraController.VerticalPanRange = CameraVerticalPanRange.Vectorize;
		
		List<Player> players = new List<Player>();
		foreach (var p in Players) {
			players.Add(p.Deserialized);
		}
		GameManager._.Players = players;
		GameManager._.PopUIStateUntilLast();
		GameManager._.SetGameSpeed(1);
	}
}

[System.Serializable]
public class SerializableColor32
{
	public byte R;
	public byte G;
	public byte B;
	public byte A;
	public SerializableColor32(Color color) {
		Color32 color32 = color;
		R = color32.r;
		G = color32.g;
		B = color32.b;
		A = color32.a;
	}
	public SerializableColor32(Color32 color32) {
		R = color32.r;
		G = color32.g;
		B = color32.b;
		A = color32.a;
	}
	public SerializableColor32(byte r, byte g, byte b, byte a) {
		R = r;
		G = g;
		B = b;
		A = a;
	}

	public Color32 Colorize { get {
		return new Color32(R, G, B, A);
	}	}
}

[System.Serializable]
public class SerializableVector3
{
	public float x;
	public float y;
	public float z;
	public SerializableVector3(Vector3 vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
	}

	public Vector3 Vectorize { get {
			return new Vector3(x, y, z);
	}	}
}